<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vans', function (Blueprint $table) {
            $table->increments('id');
            $table->string('van_number_plate', 120)->nullable();
            $table->string('van_reg_no', 120)->nullable();
            $table->date('van_reg_no_issue_date');
            $table->date('van_reg_no_expired_date');
            $table->string('van_tax_token', 120)->nullable();
            $table->date('van_tax_token_issue_date');
            $table->date('van_tax_token_expired_date');
            $table->string('van_fitness_no', 120)->nullable();
            $table->date('van_fitness_no_issue_date');
            $table->date('van_fitness_no_expired_date');
            $table->string('van_insurance_no', 120)->nullable();
            $table->date('van_insurance_no_issue_date');
            $table->date('van_insurance_no_expired_date');
            $table->string('van_road_permit_no', 120)->nullable();
            $table->date('van_road_permit_no_issue_date');
            $table->date('van_road_permit_no_expired_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vans');
    }
}
