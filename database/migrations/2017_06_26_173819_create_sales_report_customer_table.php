<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesReportCustomerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * SR means Sales Report
     * SRC means Sales Report Customer
     */
    public function up()
    {
        Schema::create('sales_report_customer', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sales_report_id')->unsigned();
            $table->integer('salesman_id')->unsigned();
            $table->integer('customer_id')->unsigned();
            $table->integer('area_id')->unsigned();
            $table->integer('local_area_id')->unsigned();
            $table->integer('cash_memo')->unsigned();
            $table->decimal('total_amount', 15,5);
            $table->string('comments')->nullable();
             $table->date('salesreport_created_at');
            $table->timestamps();



            $table->foreign('area_id')
                  ->references('id')
                  ->on('areas');


            $table->foreign('local_area_id')
                  ->references('id')
                  ->on('local_areas');


            $table->foreign('customer_id')
                  ->references('id')
                  ->on('customers');


            $table->foreign('sales_report_id')
                  ->references('id')
                  ->on('sales_reports');
                  

            $table->foreign('salesman_id')
                  ->references('id')
                  ->on('salesmen');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_report_customer');
    }
}
