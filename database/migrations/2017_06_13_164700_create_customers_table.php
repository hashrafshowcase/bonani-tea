<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('customer_name', 191);
            $table->string('customer_shop_name', 191)->nullable();
            $table->string('customer_phone_number', 30)->nullable()->unique();
            $table->string('customer_details')->nullable();
            $table->integer('area_id')->unsigned();
            $table->integer('local_area_id')->unsigned();
            $table->timestamps();

            $table->foreign('area_id')
                  ->references('id')
                  ->on('areas');

            $table->foreign('local_area_id')
                  ->references('id')
                  ->on('local_areas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
