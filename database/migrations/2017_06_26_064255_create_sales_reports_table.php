<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_reports', function (Blueprint $table) {
            $table->increments('id');
            $table->date('sales_date');
            $table->integer('salesman_id')->unsigned();
            $table->integer('authorized_id')->unsigned();
            $table->integer('area_id')->unsigned();
            $table->integer('van_id')->unsigned();
            $table->double('total_kg', 15, 5);
            $table->double('total_amount', 15, 5);
            $table->date('salesreport_created_at');
            $table->enum('status', ['paid', 'due']);
            $table->timestamps();

            $table->foreign('salesman_id')
                  ->references('id')
                  ->on('salesmen');

            $table->foreign('authorized_id')
                  ->references('id')
                  ->on('users');

            $table->foreign('area_id')
                  ->references('id')
                  ->on('areas');

            $table->foreign('van_id')
                  ->references('id')
                  ->on('vans');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_reports');
    }
}
