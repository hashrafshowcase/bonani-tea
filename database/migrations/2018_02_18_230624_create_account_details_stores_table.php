<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountDetailsStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('account_details_stores', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('account_transaction_id')->unsigned();
            $table->date('store_date');
            $table->integer('account_asset_type_id')->unsigned();
            $table->integer('account_type_id');
            $table->enum('ledger_type', ['general', 'subsidiary']);
            $table->enum('type', ['debit', 'credit']);
            $table->integer('against_type_id');
            $table->string('against_ledger_type');
            $table->double('value', 15, 2);
            $table->timestamps();

            $table->foreign('account_asset_type_id')
                  ->references('id')
                  ->on('account_assets');

            $table->foreign('account_transaction_id')
                  ->references('id')
                  ->on('account_transactions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('account_details_stores');
    }
}
