<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountSubsidiaryLedgersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('account_subsidiary_ledgers', function (Blueprint $table) {
            $table->increments('subsidiary_ledger_id');
            $table->integer('account_asset_type_id')->unsigned();
            $table->integer('account_general_ledger_id')->unsigned();
            $table->string('subsidiary_ledger_name');
            $table->string('ref_no');
            $table->double('opening_balance', 15, 2);
            $table->text('note')->nullable();
            $table->timestamps();

            $table->foreign('account_asset_type_id')
                   ->references('id')
                   ->on('account_assets');
                   
             $table->foreign('account_general_ledger_id')
                   ->references('general_ledger_id')
                   ->on('account_general_ledgers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('account_subsidiary_ledgers');
    }
}
