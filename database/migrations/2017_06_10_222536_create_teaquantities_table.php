<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeaquantitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teaquantities', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tea_id')->unsigned();
            $table->string('name',80);
            $table->string('short_name', 50);
            $table->double('kg', 10, 5);
            $table->float('amount',5,2);
            $table->timestamps();

            $table->foreign('tea_id')
                  ->references('id')
                  ->onDelete('cascade')
                  ->onUpdate('cascade')
                  ->on('teas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teaquantities');
    }
}
