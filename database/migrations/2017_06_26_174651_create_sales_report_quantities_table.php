<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesReportQuantitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * SR means Sales Report
     */
    public function up()
    {
        Schema::create('sales_report_quantities', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sales_report_id')->unsigned();
            $table->integer('s_rcustomer_id')->unsigned();
            $table->integer('teaquantity_id')->unsigned();
            $table->integer('quantity');
            $table->decimal('amount', 15,5);
            $table->text('note')->nullable();
            $table->string('note_value')->nullable();
            $table->date('salesreport_created_at');
            $table->timestamps();


            $table->foreign('sales_report_id')
                  ->references('id')
                  ->on('sales_reports');

            $table->foreign('s_rcustomer_id')
                  ->references('id')
                  ->on('sales_report_customer');

            $table->foreign('teaquantity_id')
                  ->references('id')
                  ->on('teaquantities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_report_quantities');
    }
}
