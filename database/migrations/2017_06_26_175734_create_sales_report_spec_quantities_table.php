<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesReportSpecQuantitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_report_spec_quantities', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sales_report_id')->unsigned();
            $table->integer('teaquantity_id')->unsigned();
            $table->integer('area_id')->unsigned();
            $table->integer('total_quantity');
            $table->date('salesreport_created_at');
            $table->timestamps();


            $table->foreign('area_id')
                  ->references('id')
                  ->on('areas');

            $table->foreign('sales_report_id')
                  ->references('id')
                  ->on('sales_reports');

            $table->foreign('teaquantity_id')
                  ->references('id')
                  ->on('teaquantities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_report_spec_quantities');
    }
}
