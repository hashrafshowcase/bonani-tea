<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stocks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('stock_indices_id')->unsigned();
            $table->date('curent_date');
            $table->decimal('stocks_past_deposit', 15, 2)->nullable();
            $table->decimal('stocks_new_deposit', 15, 2)->nullable();
            $table->string('stocks_new_deposit_cash_memo')->nullable();
            $table->decimal('stocks_sum_deposit', 15, 2)->nullable();
            $table->decimal('stocks_quantity_sold', 15, 2)->nullable();
            $table->string('stocks_quantity_sold_cash_memo')->nullable();
            $table->decimal('stocks_balance', 15, 2);
            $table->timestamps();

            $table->foreign('stock_indices_id')
                  ->references('id')
                  ->on('stock_indices');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stocks');
    }
}
