<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnloadAssetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('unload_assets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('broker_id')->unsigned();
            $table->date('submit_date')->nullable();
            $table->date('auction_date')->nullable();
            $table->date('prompt_date')->nullable();
            $table->integer('total_packs');
            $table->double('total_kg', 15, 2);
            $table->double('total_amount', 15, 2);
            $table->timestamps();

            $table->foreign('broker_id')
                  ->references('id')
                  ->on('brokers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('unload_assets');
    }
}
