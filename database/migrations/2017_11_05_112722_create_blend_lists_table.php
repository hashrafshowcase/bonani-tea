<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlendListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blend_lists', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('blend_id')->unsigned();
            $table->integer('tea_id')->unsigned();
            $table->integer('tea_grade_id')->unsigned();
            $table->integer('unload_asset_product_list_id')->unsigned();
            $table->double('packs', 15, 2);
            $table->timestamps();

            $table->foreign('unload_asset_product_list_id')
                  ->references('id')
                  ->on('unload_asset_product_lists');

            $table->foreign('blend_id')
                  ->references('id')
                  ->on('blends');

            $table->foreign('tea_id')
                  ->references('id')
                  ->on('teas');

            $table->foreign('tea_grade_id')
                  ->references('id')
                  ->on('tea_grades');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blend_lists');
    }
}
