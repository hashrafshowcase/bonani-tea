<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->increments('id');
            $table->string('employee_name');
            $table->string('designation');
            $table->date('recruit_date');
            $table->string('contact_no');
	        $table->string('nid');
	        $table->decimal('base_salary', 10, 2);
	        $table->string('birth_cer_url');
	        $table->string('profile_image_url');
            $table->string('email')->nullable();
            $table->string('father_name')->nullable();
            $table->string('mother_name')->nullable();
            $table->date('date_of_birth')->nullable();
            $table->enum('sex', ['male', 'female'])->nullable();
            $table->string('religion')->nullable();
            $table->string('nationality')->nullable();
            $table->string('blood_group')->nullable();
            $table->string('present_house')->nullable();
            $table->string('present_village')->nullable();
            $table->string('present_post_office')->nullable();
            $table->string('present_district')->nullable();
            $table->string('present_contact_no')->nullable();
	        $table->string('present_email')->nullable();
	        $table->string('permanent_house')->nullable();
	        $table->string('permanent_village')->nullable();
	        $table->string('permanent_post_office')->nullable();
	        $table->string('permanent_district')->nullable();
	        $table->string('permanent_contact_no')->nullable();
	        $table->string('permanent_email')->nullable();
	        $table->string('emergency_name')->nullable();
	        $table->string('emergency_relation')->nullable();
	        $table->string('emergency_address')->nullable();
	        $table->string('emergency_contact_no')->nullable();
	        $table->string('emergency_email')->nullable();
	        $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
