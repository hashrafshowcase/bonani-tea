<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockIndicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_indices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('stock_category_id')->unsigned();
            $table->integer('teaquantities_id')->unsigned()->nullable();
            $table->string('stock_indices_name');
            $table->string('stock_indices_month');
            $table->timestamps();

            $table->foreign('stock_category_id')
                  ->references('id')
                  ->onDelete('cascade')
                  ->onUpdate('cascade')
                  ->on('stock_categories');

            $table->foreign('teaquantities_id')
                  ->references('id')
                  ->onDelete('cascade')
                  ->onUpdate('cascade')
                  ->on('teaquantities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock_indices');
    }
}
