<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnloadAssetProductListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('unload_asset_product_lists', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('unload_asset_id')->unsigned();
            $table->integer('lot_no');
            $table->integer('garden_id')->unsigned();
            $table->string('invoice_no')->nullable();
            $table->integer('tea_grade_id')->unsigned();
            $table->integer('packs');
            $table->integer('available_packs');
            $table->double('net_each', 5, 2);
            $table->enum('sample', ['N', 'Y']);
            $table->enum('status', ['N', 'Y'])->default('Y');
            $table->enum('payment', ['paid', 'due']);
            $table->double('total_kg', 15, 2);
            $table->double('per_price', 15, 2);
            $table->double('amount', 15, 2);
            $table->timestamps();

            $table->foreign('garden_id')
                  ->references('id')
                  ->on('gardens');

            $table->foreign('tea_grade_id')
                  ->references('id')
                  ->on('tea_grades');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('unload_asset_product_lists');
    }
}
