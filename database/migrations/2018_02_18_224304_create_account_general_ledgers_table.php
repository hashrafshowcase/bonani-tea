<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountGeneralLedgersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('account_general_ledgers', function (Blueprint $table) {
            $table->increments('general_ledger_id');
            $table->integer('account_asset_type_id')->unsigned();
            $table->string('general_ledger_name');
            $table->string('ref_no');
            $table->double('opening_balance', 15, 2);
            $table->text('note')->nullable();
            $table->enum('status', ['open', 'close'])->default('open');
            $table->timestamps();

            $table->foreign('account_asset_type_id')
                   ->references('id')
                   ->on('account_assets');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('account_general_ledgers');
    }
}
