<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Models\Tea;
use App\Models\Teaquantity;
use App\Models\Role;
use App\Models\Area;
use App\Models\LocalArea;
use App\Models\Customer;
use App\Models\UserRole;
use App\Models\Van;
use App\Models\Garden;
use App\Models\SalesReport;
use App\Models\SRcustomer;
use App\Models\SRQuantities;
use App\Models\SRSpcQuantities;
use App\Models\Salesman;
use App\Models\TeaGrade;
use App\Models\Brokers;
use App\Models\StockCategory;
use App\Models\StockIndex;
use App\Models\Stocks;
use App\Models\Vat;
use App\Models\UnloadAsset;
use App\Models\UnloadAssetProductList;
use App\Models\Blend;
use App\Models\BlendList;
use App\Models\HomeRent;
use App\Models\DailySpent;
use App\Models\AccountHistory;
use App\Models\AssetsType;
use App\Models\AssetTypeList;
use App\Models\AccountAssets;
use App\Models\AccountGeneralLedger;
use App\Models\AccountTransaction;
use App\Models\AccountDetailsStore;

class DatabaseSeeder extends Seeder
{
    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
        $user1 = User::create([
                'name' => 'Rafsan Jani',
                'email' => 'rafsan@gmail.com',
                'password' => bcrypt('secret')
            ]);

        $user2 = User::create([
                'name' => 'Rafsan Jani',
                'email' => 'rafsan1@gmail.com',
                'password' => bcrypt('secret')
            ]);

        $user3 = User::create([
                'name' => 'Hb Shifat',
                'email' => 'hbshifat30@gmail.com',
                'password' => bcrypt('secret')
            ]);

        $tea2 = Tea::create([
                'name'  => 'Banani Fresh Tea',
                'slug'  => 'banani-fresh-tea'
        ]);

        $tea1 = Tea::create([
                'name' => 'Banani Special Tea',
                'slug' => 'banani-special-tea',
            ]);
        $tea3 = Tea::create([
                'name'  => 'Banani Hotel Dust',
                'slug'  => 'banani-hotel-dust'
        ]);

        $quantity1 = Teaquantity::create([
                'tea_id' => $tea2->id,
                'name' => 'FRS 500g',
                'kg' => 0.500,
                'short_name' => 'FRS 500g',
                'amount' => 160,
            ]);

        $quantity2 = Teaquantity::create([
                'tea_id' => $tea2->id,
                'name' => 'FRS 250g',
                'kg' => 0.250,
                'short_name' => 'FRS 250g',
                'amount' => 80,
            ]);

        $quantity3 = Teaquantity::create([
                'tea_id' => $tea2->id,
                'name' => 'FRS 100g',
                'kg' => 0.100,
                'short_name' => 'FRS 100g',
                'amount' => 30,
            ]);

        $quantity4 = Teaquantity::create([
                'tea_id' => $tea2->id,
                'name' => 'FRS 50g',
                'kg' => 0.050,
                'short_name' => 'FRS 50g',
                'amount' => 15,
            ]);

        $quantity5 = Teaquantity::create([
                'tea_id' => $tea1->id,
                'name' => 'SP 1000g',
                'kg' => 1,
                'short_name' => 'SP 1000g',
                'amount' => 300,
            ]);

        $quantity6 = Teaquantity::create([
                'tea_id' => $tea1->id,
                'name' => 'SP 500g',
                'kg' => 0.5,
                'short_name' => 'SP 500g',
                'amount' => 150,
            ]);
        $quantity7 = Teaquantity::create([
                'tea_id' => $tea1->id,
                'name' => 'SP 250g',
                'kg' => 0.250,
                'short_name' => 'SP 250g',
                'amount' => 75,
            ]);
        $quantity8 = Teaquantity::create([
                'tea_id' => $tea1->id,
                'name' => 'SP 100g',
                'kg' => 0.100,
                'short_name' => 'SP 100g',
                'amount' => 38,
            ]);
        $quantity9 = Teaquantity::create([
                'tea_id' => $tea1->id,
                'name' => 'SP 50g',
                'kg' => 0.050,
                'short_name' => 'SP 50g',
                'amount' => 19,
            ]);
        $quantity10 = Teaquantity::create([
                'tea_id' => $tea3->id,
                'name' => 'HD 1000g',
                'kg' => 1,
                'short_name' => 'HD 1000g',
                'amount' => 300,
            ]);

        $quantity11 = Teaquantity::create([
                'tea_id' => $tea3->id,
                'name' => 'HD 500g',
                'kg' => 0.5,
                'short_name' => 'HD 500g',
                'amount' => 150,
            ]);

        // $roles1 = Role::create([
        //         'title' => 'Owner'
        //     ]);


        // $roles1 = Role::create([
        //         'title' => 'Owner'
        //     ]);

        // $roles2 = Role::create([
        //         'title' => 'Admin'
        //     ]);

        // $roles3 = Role::create([
        //         'title' => 'Accountant'
        //     ]);

        // $roles4 = Role::create([
        //         'title' => 'Salesman'
        //     ]);

        // $roles5 = Role::create([
        //         'title' => 'Stock Keeper'
        //     ]);

        // $owner = UserRole::create([
        //         'user_id' => 1,
        //         'role_id' => 1
        //     ]);

        // $owner2 = UserRole::create([
        //         'user_id' => 1,
        //         'role_id' => 2
        //     ]);

        // $owner3 = UserRole::create([
        //         'user_id' => 1,
        //         'role_id' => 3
        //     ]);

        // $owner4 = UserRole::create([
        //         'user_id' => 2,
        //         'role_id' => 4
        //     ]);

        // $area1 = Area::create([

        //         'area_name' => 'Kamal Bazar',
        //         'area_slug' => 'kamal-bazar'

        //     ]);

        // $area2 = Area::create([

        //         'area_name' => 'Rikabi Bazar',
        //         'area_slug' => 'rikabi-bazar'

        //     ]);

        // $localarea1 = LocalArea::create([

        //         'area_id' => 1,
        //         'local_area_name' => 'Medical Road',
        //         'local_area_name_slug' => 'medical-road'
        //     ]);


        // $localarea12 = LocalArea::create([

        //         'area_id' => 2,
        //         'local_area_name' => 'Chowhatta',
        //         'local_area_name_slug' => 'Chowhatta'
        //     ]);


        // $localarea13 = LocalArea::create([

        //         'area_id' => 2,
        //         'local_area_name' => 'Lamabazar',
        //         'local_area_name_slug' => 'Lamabazar'
        //     ]);

        // $localarea14 = LocalArea::create([

        //         'area_id' => 1,
        //         'local_area_name' => 'Nobab Road',
        //         'local_area_name_slug' => 'nobab-road'
        //     ]);

        // $customer1 = Customer::create([

        //         'customer_name' => 'SMA Shuvo',
        //         'customer_shop_name' => 'Banani Tea and trading Co.',
        //         'customer_phone_number' => 01710000000,
        //         'area_id' => 2,
        //         'local_area_id' => 1
        //     ]);

        // $van1 = Van::create([
        //         'van_number_plate' => 122192
        //     ]);

        // $van1 = Van::create([
        //         'van_number_plate' => 12218386392
        //     ]);

        // $garden1 = Garden::create([
        //         'garden_name' => 'Rafsan Garden',
        //         'garden_address' => 'Medical Road, Sylhet-3100'
        // ]);

        // $garden12 = Garden::create([
        //         'garden_name' => 'Akhyar Garden',
        //         'garden_address' => 'Mejortilla, Sylhet-3100'
        // ]);

        // $salesman = Salesman::create([
        //         'user_id' => $user1->id,
        //         'salesman_phone_number' => '8801717408297'
        //     ]);


        // $SalesReport = SalesReport::create([
        //         'salesman_id' => 2,
        //         'authorized_id' => 1,
        //         'area_id' => 1,
        //         'van_id' => 1,
        //         'total_kg' => 1111.1111,
        //         'total_amount' => 121121.11,
        //         'status' => 'paid',
        //         'salesreport_created_at' => '2017-08-12'
        //     ]);

        // $salesCustomer1 = SRcustomer::create([
        //         'sales_report_id' => $SalesReport->id,
        //         'salesman_id' => 1,
        //         'customer_id' =>  1,
        //         'area_id' => 1,
        //         'local_area_id' => 1,
        //         'cash_memo' => 2232,
        //         'total_amount' => 12.121,
        //         'comments' => 'Something Here.',
        //         'salesreport_created_at' => '2017-08-12'
        //     ]);

        // $salesCustomer2 = SRcustomer::create([
        //         'sales_report_id' => $SalesReport->id,
        //         'salesman_id' => 1,
        //         'customer_id' =>  1,
        //         'area_id' => 1,
        //         'local_area_id' => 2,
        //         'cash_memo' => 22321,
        //         'total_amount' => 232333.2233,
        //         'comments' => 'Something Here.',
        //         'salesreport_created_at' => '2017-08-12'
        //     ]);

        // $SalesQuantities = SRQuantities::create([
        //         'sales_report_id' => $SalesReport->id,
        //         's_rcustomer_id' => $salesCustomer1->id,
        //         'teaquantity_id' => 1,
        //         'quantity' => 2,
        //         'amount' => 10,
        //         'salesreport_created_at' => '2017-08-12'
        //     ]);

        // $SalesSpecQuan = SRSpcQuantities::create([
        //         'sales_report_id' => $SalesReport->id,
        //         'teaquantity_id' => 1,
        //         'area_id' => 1,
        //         'total_quantity' => 10,
        //         'salesreport_created_at' => '2017-08-12'
        //     ]);

        // $teaGrade1 = TeaGrade::create([
        //         'grade_name' => 'ABC1',
        //         'total_kg' => 0.00,
        //         'total_packs' => 0.00
        //     ]);

        // $teaGrade1 = TeaGrade::create([
        //         'grade_name' => 'ABC2',
        //         'total_kg' => 0.00,
        //         'total_packs' => 0.00
        //     ]);

        // $teaGrade1 = TeaGrade::create([
        //         'grade_name' => 'ABC3',
        //         'total_kg' => 0.00,
        //         'total_packs' => 0.00
        //     ]);

        // $teaGrade1 = TeaGrade::create([
        //         'grade_name' => 'ABA1',
        //         'total_kg' => 0.00,
        //         'total_packs' => 0.00
        //     ]);

        // $teaGrade1 = TeaGrade::create([
        //         'grade_name' => 'BCD1',
        //         'total_kg' => 0.00,
        //         'total_packs' => 0.00
        //     ]);

        // $brokers1 = Brokers::create([
        //         'broker_name' => 'Pipilika Brokers Ltd.',
        //         'broker_address' => 'Hawapara,sylhet'
        //     ]);

        // $brokers1 = Brokers::create([
        //         'broker_name' => 'Google Brokers Ltd.',
        //         'broker_address' => 'Nyc,sylhet'
        //     ]);
        //     
        $StockCategory1 = StockCategory::create([
            'stock_name' => 'Finished Goods'
        ]);

        $StockCategory2 = StockCategory::create([
            'stock_name' => 'Show Room'
        ]);

        // $StockCategory1 = StockCategory::create([
        //         'stock_name' => 'Raw Material Stock'
        //     ]);

        // $StockCategory2 = StockCategory::create([
        //         'stock_name' => 'Poythene Stock'
        //     ]);

        // $StockCategory3 = StockCategory::create([
        //         'stock_name' => 'Dust Stock'
        //     ]);

        $StockIndex1 = StockIndex::create([
                'stock_category_id' => $StockCategory1->id,
                'teaquantities_id' => $quantity1->id,
                'stock_indices_name' => $quantity1->name . '  - FD',
                'stock_indices_month' => 'all'
            ]);
        $StockIndex1 = StockIndex::create([
                'stock_category_id' => $StockCategory1->id,
                'teaquantities_id' => $quantity2->id,
                'stock_indices_name' => $quantity2->name . '  - FD',
                'stock_indices_month' => 'all'
            ]);
        $StockIndex1 = StockIndex::create([
                'stock_category_id' => $StockCategory1->id,
                'teaquantities_id' => $quantity3->id,
                'stock_indices_name' => $quantity3->name . '  - FD',
                'stock_indices_month' => 'all'
            ]);
        $StockIndex1 = StockIndex::create([
                'stock_category_id' => $StockCategory1->id,
                'teaquantities_id' => $quantity4->id,
                'stock_indices_name' => $quantity4->name . '  - FD',
                'stock_indices_month' => 'all'
            ]);
        $StockIndex1 = StockIndex::create([
                'stock_category_id' => $StockCategory1->id,
                'teaquantities_id' => $quantity5->id,
                'stock_indices_name' => $quantity5->name . '  - FD',
                'stock_indices_month' => 'all'
            ]);
        $StockIndex1 = StockIndex::create([
                'stock_category_id' => $StockCategory1->id,
                'teaquantities_id' => $quantity6->id,
                'stock_indices_name' => $quantity6->name. '  - FD',
                'stock_indices_month' => 'all'
            ]);
        $StockIndex1 = StockIndex::create([
                'stock_category_id' => $StockCategory1->id,
                'teaquantities_id' => $quantity7->id,
                'stock_indices_name' => $quantity7->name. '  - FD',
                'stock_indices_month' => 'all'
            ]);
         $StockIndex1 = StockIndex::create([
                'stock_category_id' => $StockCategory1->id,
                'teaquantities_id' => $quantity8->id,
                'stock_indices_name' => $quantity8->name. '  - FD',
                'stock_indices_month' => 'all'
            ]);
          $StockIndex1 = StockIndex::create([
                'stock_category_id' => $StockCategory1->id,
                'teaquantities_id' => $quantity9->id,
                'stock_indices_name' => $quantity9->name. '  - FD',
                'stock_indices_month' => 'all'
            ]);
           $StockIndex1 = StockIndex::create([
                'stock_category_id' => $StockCategory1->id,
                'teaquantities_id' => $quantity10->id,
                'stock_indices_name' => $quantity10->name. '  - FD',
                'stock_indices_month' => 'all'
            ]);
            $StockIndex1 = StockIndex::create([
                'stock_category_id' => $StockCategory1->id,
                'teaquantities_id' => $quantity11->id,
                'stock_indices_name' => $quantity11->name. '  - FD',
                'stock_indices_month' => 'all'
            ]);



        $StockIndex1 = StockIndex::create([
                'stock_category_id' => $StockCategory2->id,
                'teaquantities_id' => $quantity1->id,
                'stock_indices_name' => $quantity1->name . '  - SR',
                'stock_indices_month' => 'all'
            ]);
        $StockIndex1 = StockIndex::create([
                'stock_category_id' => $StockCategory2->id,
                'teaquantities_id' => $quantity2->id,
                'stock_indices_name' => $quantity2->name. '  - SR',
                'stock_indices_month' => 'all'
            ]);
        $StockIndex1 = StockIndex::create([
                'stock_category_id' => $StockCategory2->id,
                'teaquantities_id' => $quantity3->id,
                'stock_indices_name' => $quantity3->name. '  - SR',
                'stock_indices_month' => 'all'
            ]);
        $StockIndex1 = StockIndex::create([
                'stock_category_id' => $StockCategory2->id,
                'teaquantities_id' => $quantity4->id,
                'stock_indices_name' => $quantity4->name. '  - SR',
                'stock_indices_month' => 'all'
            ]);
        $StockIndex1 = StockIndex::create([
                'stock_category_id' => $StockCategory2->id,
                'teaquantities_id' => $quantity5->id,
                'stock_indices_name' => $quantity5->name. '  - SR',
                'stock_indices_month' => 'all'
            ]);
        $StockIndex1 = StockIndex::create([
                'stock_category_id' => $StockCategory2->id,
                'teaquantities_id' => $quantity6->id,
                'stock_indices_name' => $quantity6->name. '  - SR',
                'stock_indices_month' => 'all'
            ]);
        $StockIndex1 = StockIndex::create([
                'stock_category_id' => $StockCategory2->id,
                'teaquantities_id' => $quantity7->id,
                'stock_indices_name' => $quantity7->name. '  - SR',
                'stock_indices_month' => 'all'
            ]);
         $StockIndex1 = StockIndex::create([
                'stock_category_id' => $StockCategory2->id,
                'teaquantities_id' => $quantity8->id,
                'stock_indices_name' => $quantity8->name. '  - SR',
                'stock_indices_month' => 'all'
            ]);
          $StockIndex1 = StockIndex::create([
                'stock_category_id' => $StockCategory2->id,
                'teaquantities_id' => $quantity9->id,
                'stock_indices_name' => $quantity9->name. '  - SR',
                'stock_indices_month' => 'all'
            ]);
           $StockIndex1 = StockIndex::create([
                'stock_category_id' => $StockCategory2->id,
                'teaquantities_id' => $quantity10->id,
                'stock_indices_name' => $quantity10->name. '  - SR',
                'stock_indices_month' => 'all'
            ]);
            $StockIndex1 = StockIndex::create([
                'stock_category_id' => $StockCategory2->id,
                'teaquantities_id' => $quantity11->id,
                'stock_indices_name' => $quantity11->name. '  - SR',
                'stock_indices_month' => 'all'
            ]);


        // $StockIndex2 = StockIndex::create([
        //         'stock_category_id' => $StockCategory2->id,
        //         'teaquantities_id' => $quantity1->id,
        //         'stock_indices_name' => '0.5KG SP Feb 2017',
        //         'stock_indices_month' => 'all'
        //     ]);

        // $StockIndex3 = StockIndex::create([
        //         'stock_category_id' => $StockCategory3->id,
        //         'teaquantities_id' => $quantity1->id,
        //         'stock_indices_name' => '0.5KG SP Feb 2017',
        //         'stock_indices_month' => '08-17'
        //     ]);

        // $Stocks = Stocks::create([
        //         'stock_indices_id' => $StockIndex1->id,
        //         'curent_date' => '2017-08-13',
        //         'stocks_past_deposit' => 188,
        //         'stocks_new_deposit' => 3,
        //         'stocks_new_deposit_cash_memo' => '',
        //         'stocks_sum_deposit' => 191,
        //         'stocks_quantity_sold' => 5,
        //         'stocks_quantity_sold_cash_memo' => '',
        //         'stocks_balance' => 186
        //     ]);
        // $vat1 = Vat::create([
        //         'vat_type' => 'General',
        //         'value' => 15
        //     ]);

        // $vat2 = Vat::create([
        //         'vat_type' => 'AIT',
        //         'value' => 1
        //     ]);

        // $vat3 = Vat::create([
        //         'vat_type' => 'BrokerAge',
        //         'value' => 10
        //     ]);
        // $unload_asset = UnloadAsset::create([
        //         'broker_id' => $brokers1->id,
        //         'submit_date' => '2017-11-26',
        //         'auction_date' => '2017-08-12',
        //         'prompt_date' => '2017-08-12',
        //         'total_packs' => 10,
        //         'total_kg' => 548.50,
        //         'total_amount' => 1212112.40
        // ]);

        // $unload_asset_list = UnloadAssetProductList::create([
        //         'unload_asset_id' => $unload_asset->id,
        //         'lot_no' => 25,
        //         'garden_id' => $garden1->id,
        //         'invoice_no' => '8X.Ap',
        //         'tea_grade_id' => $teaGrade1->id,
        //         'packs' => 10,
        //         'net_each' => 55.0,
        //         'sample' => 'Y',
        //         'payment' => 'paid',
        //         'total_kg' => 548.50,
        //         'per_price' => 255.00,
        //         'amount' => 1212112.40
        // ]);
        // $blend = Blend::create([
        //         'date' => '2017-11-05',
        //         'total_packs' => 100.00
        // ]);

        // $blend_list_1 = BlendList::create([
        //         'blend_id' => $blend->id,
        //         'tea_id' => $tea1->id,
        //         'tea_grade_id' => $teaGrade1->id,
        //         'packs' => 10.00
        // ]);

        // $home_rent_1 = HomeRent::create([
        //         'description' => 'new home 1',
        //         'value' => 20000
        // ]);

        // $home_rent_1 = HomeRent::create([
        //         'description' => 'new home 2',
        //         'value' => 2000000
        // ]);

        // $daily_spent_1 = DailySpent::create([
        //         'reason' => 'Van vara',
        //         'value' => 100
        // ]);

        // $daily_spent_2 = DailySpent::create([
        //         'reason' => 'Rickshaw vara',
        //         'value' => 30
        // ]);

        // $account_history_1 = AccountHistory::create([
        //         'description' => 'For Produce new Tea',
        //         'history_type' => 'cash',
        //         'account_type' => 'debit',
        //         'value' => 100
        // ]);

        // $account_history_2 = AccountHistory::create([
        //         'description' => 'By selling new Tea',
        //         'history_type' => 'cash',
        //         'account_type' => 'credit',
        //         'value' => 110
        // ]);

        // $asset_type_1 = AssetsType::create([
        //         'title' => 'Asset',
        //         'ref' => '100',
        //         'type' => 'primary'
        // ]);

        // $asset_type_2 = AssetsType::create([
        //         'title' => 'Liablities',
        //         'ref' => '200',
        //         'type' => 'primary'
        // ]);

        // $asset_type_3 = AssetsType::create([
        //         'title' => 'Equities',
        //         'ref' => '300',
        //         'type' => 'primary'
        // ]);

        // $asset_type_4 = AssetsType::create([
        //         'title' => 'Revenue',
        //         'ref' => '400',
        //         'type' => 'primary'
        // ]);

        // $asset_type_5 = AssetsType::create([
        //         'title' => 'Expense',
        //         'ref' => '500',
        //         'type' => 'primary'
        // ]);

        // $asset_type_6 = AssetsType::create([
        //         'title' => 'Gains',
        //         'ref' => '600',
        //         'type' => 'primary'
        // ]);

        // $asset_type_7 = AssetsType::create([
        //         'title' => 'Losses',
        //         'ref' => '700',
        //         'type' => 'primary'
        // ]);

        // $asset_type_8 = AssetsType::create([
        //         'title' => 'Cash',
        //         'ref' => '800',
        //         'type' => 'secondary'
        // ]);

        // $assets_list_1 = AssetTypeList::create([
        //         'parent_id' => 1,
        //         'children_id' => 8
        // ]);
            $account1 = AccountAssets::create([
                'account_type_name' => 'Assets',
                'type_ref'          =>  'A773TS'
            ]);

            $account2 = AccountAssets::create([
                'account_type_name' => 'Liablities',
                'type_ref'          =>  'L42B54T43S'
            ]);

            $account3 = AccountAssets::create([
                'account_type_name' => 'Owners Equity',
                'type_ref'          =>  'E78I8Y'
            ]);

            $account4 = AccountAssets::create([
                'account_type_name' => 'Revenues',
                'type_ref'          =>  'R38E68ES'
            ]);

            $account5 = AccountAssets::create([
                'account_type_name' => 'Expenses',
                'type_ref'          =>  'E97E67S3S'
            ]);

            $account6 = AccountAssets::create([
                'account_type_name' => 'Gains',
                'type_ref'          =>  'G24N7S'
            ]);

            $account7 = AccountAssets::create([
                'account_type_name' => 'Losses',
                'type_ref'          =>  'L67S3S'
            ]);

		    $generalLedger1 = AccountGeneralLedger::create([
			    'account_asset_type_id' => $account6->id,
			    'general_ledger_name'   => 'Salary',
			    'ref_no'                => $account6->type_ref . '-001',
			    'opening_balance'       => 0.00,
			    'status'                => 'open'
		    ]);

		    $generalLedger2 = AccountGeneralLedger::create([
			    'account_asset_type_id' => $account6->id,
			    'general_ledger_name'   => 'Bonus',
			    'ref_no'                => $account6->type_ref . '-002',
			    'opening_balance'       => 0.00,
			    'status'                => 'open'
		    ]);

		    $generalLedger3 = AccountGeneralLedger::create([
			    'account_asset_type_id' => $account1->id,
			    'general_ledger_name'   => 'Account Receivable',
			    'ref_no'                => $account1->type_ref . '-001',
			    'opening_balance'       => 0.00,
			    'status'                => 'open'
		    ]);

		    $generalLedger4 = AccountGeneralLedger::create([
			    'account_asset_type_id' => $account2->id,
			    'general_ledger_name'   => 'Account Payable',
			    'ref_no'                => $account2->type_ref . '-001',
			    'opening_balance'       => 0.00,
			    'status'                => 'open'
		    ]);

		    $transaction1 = AccountTransaction::create([
		    	'transaction_date'  => date('Y-m-d'),
			    'note'              => $generalLedger1->general_ledger_name . ' Ledger Created!',
			    'created_at'        =>  date('Y-m-d H:i:s'),
			    'updated_at'        =>  date('Y-m-d H:i:s')
		    ]);

		    $detailsStore1 = AccountDetailsStore::create([
		    	'account_transaction_id'    => $transaction1->id,
			    'store_date'                => date('Y-m-d'),
			    'account_asset_type_id'     => $account6->id,
			    'account_type_id'           => $generalLedger1->id,
			    'ledger_type'               => 'general',
			    'type'                      => 'debit',
			    'against_type_id'           => $generalLedger1->id,
			    'against_ledger_type'       => 'general',
			    'value'                     => $generalLedger1->opening_balance,
			    'created_at'        =>  date('Y-m-d H:i:s'),
			    'updated_at'        =>  date('Y-m-d H:i:s')
		    ]);

		    $transaction2 = AccountTransaction::create([
			    'transaction_date'  => date('Y-m-d'),
			    'note'              => $generalLedger1->general_ledger_name . ' Ledger Created!',
			    'created_at'        =>  date('Y-m-d H:i:s'),
			    'updated_at'        =>  date('Y-m-d H:i:s')
		    ]);

		    $detailsStore2 = AccountDetailsStore::create([
			    'account_transaction_id'    => $transaction2->id,
			    'store_date'                => date('Y-m-d'),
			    'account_asset_type_id'     => $account6->id,
			    'account_type_id'           => $generalLedger1->id,
			    'ledger_type'               => 'general',
			    'type'                      => 'debit',
			    'against_type_id'           => $generalLedger1->id,
			    'against_ledger_type'       => 'general',
			    'value'                     => $generalLedger1->opening_balance,
			    'created_at'        =>  date('Y-m-d H:i:s'),
			    'updated_at'        =>  date('Y-m-d H:i:s')
		    ]);

		    $transaction3 = AccountTransaction::create([
			    'transaction_date'  => date('Y-m-d'),
			    'note'              => $generalLedger3->general_ledger_name . ' Ledger Created!',
			    'created_at'        =>  date('Y-m-d H:i:s'),
			    'updated_at'        =>  date('Y-m-d H:i:s')
		    ]);

		    $detailsStore3 = AccountDetailsStore::create([
			    'account_transaction_id'    => $transaction3->id,
			    'store_date'                => date('Y-m-d'),
			    'account_asset_type_id'     => $account1->id,
			    'account_type_id'           => $generalLedger3->id,
			    'ledger_type'               => 'general',
			    'type'                      => 'debit',
			    'against_type_id'           => $generalLedger3->id,
			    'against_ledger_type'       => 'general',
			    'value'                     => $generalLedger3->opening_balance,
			    'created_at'        =>  date('Y-m-d H:i:s'),
			    'updated_at'        =>  date('Y-m-d H:i:s')
		    ]);

		    $transaction4 = AccountTransaction::create([
			    'transaction_date'  => date('Y-m-d'),
			    'note'              => $generalLedger4->general_ledger_name . ' Ledger Created!',
			    'created_at'        =>  date('Y-m-d H:i:s'),
			    'updated_at'        =>  date('Y-m-d H:i:s')
		    ]);

		    $detailsStore4 = AccountDetailsStore::create([
			    'account_transaction_id'    => $transaction4->id,
			    'store_date'                => date('Y-m-d'),
			    'account_asset_type_id'     => $account2->id,
			    'account_type_id'           => $generalLedger4->id,
			    'ledger_type'               => 'general',
			    'type'                      => 'credit',
			    'against_type_id'           => $generalLedger4->id,
			    'against_ledger_type'       => 'general',
			    'value'                     => $generalLedger4->opening_balance,
			    'created_at'        =>  date('Y-m-d H:i:s'),
			    'updated_at'        =>  date('Y-m-d H:i:s')
		    ]);
    }
}
