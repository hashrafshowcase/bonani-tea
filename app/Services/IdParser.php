<?php

namespace App\Services;

class IdParser
{
    public static function parse($id)
    {
        preg_match('/\d+/', $id, $matches);
        if (count($matches)) {
            return (int) reset($matches);
        }
        return $id;
    }
}
