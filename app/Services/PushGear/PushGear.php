<?php

namespace App\Services\PushGear;

use GuzzleHttp\Client;

class PushGear
{
	public function __construct(Client $client)
	{
		$this->client = $client;
	}

	public function notify($event, $data, $userIds = null)
	{
		return $this->client->post($this->getUrl('event'), [
		    'json' => [
		    	'recipients' => (Array) $userIds,
		    	'event' => $event,
		    	'data' => $data,
		    ]
		]);
	}

	protected function getUrl($path)
	{
		$url = config('services.pushgear.base_url');
		
		return trim($url, '/') . '/' . $path;
	}
}