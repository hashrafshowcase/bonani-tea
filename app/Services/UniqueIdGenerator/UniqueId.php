<?php

namespace App\Services\UniqueIdGenerator;

use Ramsey\Uuid\Uuid;

class UniqueId implements UniqueIdGeneratorInterface
{
    public static function make(array $options = [])
    {
        return Uuid::Uuid1();
    }
}
