<?php

namespace App\Services\UniqueIdGenerator;

interface UniqueIdGeneratorInterface
{
    public static function make(array $options = []);
}
