<?php

namespace App\Providers;

use App\User;
use App\Models\SalesReport;
use App\Models\UserRole;
use App\Jobs\JobsSalesReportCreate;
use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'notification' => ['App\Listeners\NotifyNotificationRecipient'],
        'Illuminate\Notifications\Events\NotificationSent' => [
            'App\Listeners\BroadcastNotification',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        SalesReport::saved(function($report) {
            foreach(UserRole::whereIn('role_id', [1,4])->get() as $user) {
                dispatch(new JobsSalesReportCreate($report, User::find($user->user_id)));
            }
        });
    }
}
