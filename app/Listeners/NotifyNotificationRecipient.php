<?php

namespace App\Listeners;

use App\Services\PushGear\PushGear;

class NotifyNotificationRecipient
{
    protected $pushGear = null;

    public function __construct(PushGear $pushGear)
    {
        $this->pushGear = $pushGear;
    }

    /**
     * Handle the event.
     *
     * @param  App\Models\User  $user
     * @return void
     */
    public function handle($data, $userId = null)
    {
        return $this->pushGear->notify('notification', $data, $userId);
    }
}
