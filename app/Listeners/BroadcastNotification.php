<?php

/**
 * Not in use
 */

namespace App\Listeners;

class BroadcastNotification
{
    /**
     * Handle the event.
     *
     * @param  App\Models\User  $user
     * @return void
     */
    public function handle($event)
    {
        event('notification', [$event->notification->id, $event->notifiable->id]);
    }
}
