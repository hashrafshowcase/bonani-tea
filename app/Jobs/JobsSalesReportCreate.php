<?php

namespace App\Jobs;

use App\User;
use App\Models\SalesReport;
use App\Notifications\MakeSalesReportNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class JobsSalesReportCreate implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $sales_report = null;
    protected $user = null;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($sales_report, $user)
    {  
        $this->user = $user;
        $this->sales_report = $sales_report;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
       $this->user->notify(new MakeSalesReportNotification($this->sales_report));
    }
}
