<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tea extends Model
{
	protected $fillable = ['name', 'slug'];

	protected $hidden = ['created_at', 'updated_at'];
	

	/**
	 * [quantities description]
	 * @return [type] [description]
	 */
    public function quantities() {
    	return $this->hasMany(Teaquantity::class);
    }
}

