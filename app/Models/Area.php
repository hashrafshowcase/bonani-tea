<?php

namespace App\Models;


use App\User;
use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    protected $fillable = ['area_name', 'area_slug'];

	protected $hidden = ['created_at', 'updated_at'];

	public function users() {

		return $this->hasMany(User::class);

	}

	public function customers() {

		return $this->hasMany(Customer::class);
	}

	public function localareas() {

		return $this->hasMany(LocalArea::class);
	}

	public function sales_report() {

		return $this->hasMany(SalesReport::class);
	}

	public function sales_report_customer() {

		return $this->hasMany(SRcustomer::class);
	}

	public function sales_report_spec_quantites () {

		return $this->hasMany(SRSpcQuantities::class);
	}
}
