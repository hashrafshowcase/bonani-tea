<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Teaquantity extends Model
{
	protected $guarded = ['id'];

	protected $hidden = ['created_at', 'updated_at'];
	
    public function tea() {

    	return $this->belongsTo(Tea::class);
    }

    public function sales_report_spec_teaquantities() {

    	return $this->hasMany(SRSpcQuantities::class);
    }
}
