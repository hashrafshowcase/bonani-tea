<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\User;

class SalesReport extends Model
{
    protected $guarded = ['id'];

	protected $hidden = ['created_at', 'updated_at'];


	/**
	 * [salesman description]
	 * @return [type] [description]
	 */
	public function salesman() {

		return $this->belongsTo(Salesman::class);
	}


	/**
	 * [authorized description]
	 * @return [type] [description]
	 */
	public function authorized() {

		return $this->belongsTo(User::class);

	}


	/**
	 * [area description]
	 * @return [type] [description]
	 */
	public function area() {

		return $this->belongsTo(Area::class);
	}


	/**
	 * [van description]
	 * @return [type] [description]
	 */
	public function van() {
		
		return $this->belongsTo(Van::class);

	}


	/**
	 * [sales_report_customer description]
	 * @return [type] [description]
	 */
	public function sales_report_customer() {

		return $this->hasMany(SRcustomer::class);
	}


	/**
	 * [sales_report_quantites description]
	 * @return [type] [description]
	 */
	public function sales_report_quantites() {

		return $this->hasMany(SRQuantities::class);
	}


	/**
	 * [sales_report_spec_quantites description]
	 * @return [type] [description]
	 */
	public function sales_report_spec_quantites() {

		return $this->hasMany(SRSpcQuantities::class);
	}


	public function scopeSalesman($query)
	{
		return $query->where('salesman_id', 1);
	}
	
}
