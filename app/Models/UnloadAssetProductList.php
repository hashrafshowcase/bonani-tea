<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UnloadAssetProductList extends Model
{
    protected $guarded = ['id'];

    public function unload_asset()
    {
    	return $this->belongsTo(UnloadAsset::class);
    }

    public function garden()
    {
    	return $this->belongsTo(Garden::class);
    }

    public function tea_grade()
    {
    	return $this->belongsTo(TeaGrade::class);
    }
}
