<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AccountDetailsStore extends Model
{
    protected $guarded = ['id'];

    public function account_transaction()
    {
    	return $this->belongsTo(AccountTransaction::class);
    }
}
