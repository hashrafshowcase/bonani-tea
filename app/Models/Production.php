<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Production extends Model
{
    public $guarded = ['id'];

    public function stock_index()
    {
    	return $this->belongsTo(StockIndex::class);
    }
}
