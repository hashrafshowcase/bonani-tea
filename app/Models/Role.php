<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = ['title'];

    protected $hidden = ['created_at', 'updated_at'];

    public function users() {

    	return $this->belongsToMany(User::Class, 'user_role', 'role_id', 'user_id');

    }
}
