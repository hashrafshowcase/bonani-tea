<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SRSpcQuantities extends Model
{
    protected $table = "sales_report_spec_quantities";


    protected $guarded = ['id'];

	protected $hidden = ['created_at', 'updated_at'];



	/**
	 * [sales_report description]
	 * @return [type] [description]
	 */
	public function sales_report() {

		return $this->belongsTo(SalesReport::class);
	}



	/**
	 * [teaquantity description]
	 * @return [type] [description]
	 */
	public function teaquantity() {

		return $this->belongsTo(Teaquantity::class);
	}



	public function area() {

		return $this->belongsTo(Area::class);
	}
}
