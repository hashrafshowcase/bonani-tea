<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = ['customer_name', 'customer_shop_name', 'customer_details','customer_phone_number', 'area_id' ,'local_area_id'];

	protected $hidden = ['created_at', 'updated_at'];


	public function area() {

		return $this->belongsTo(Area::class);
	}

	public function localArea() {

		return $this->belongsTo(LocalArea::class);
	}

	public function sales_report_customer() {

		return $this->hasMany(SRcustomer::class);
	}

	

}
