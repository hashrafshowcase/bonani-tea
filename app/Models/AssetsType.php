<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AssetsType extends Model
{
    protected $guraded = ['id'];

    protected $hidden = ['created_at', 'updated_at'];

    public function assetsChildren()
    {
    	return $this->hasOne(AssetTypeList::class, 'parent_id', 'id');
    }

    public function assetsPrimary()
    {
    	return $this->hasMany(AssetTypeList::class, 'children_id', 'id');
    }
}
