<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SRQuantities extends Model
{
    protected $table = "sales_report_quantities";

    // protected $fillable = ['sales_report_id', 'sales_report_customer_id', 'teaquantities_id', 'quantity', 'amount'];

    protected $guarded = ['id'];

	protected $hidden = ['created_at', 'updated_at'];


	/**
	 * [sales_report description]
	 * @return [type] [description]
	 */
	public function sales_report() {

		return $this->belongsTo(SalesReport::class);
	}


	/**
	 * [sales_report_customer description]
	 * @return [type] [description]
	 */
	public function sales_report_customer() {

		return $this->belongsTo(SRcustomer::class);
	}


	/**
	 * [teaquantity description]
	 * @return [type] [description]
	 */
	public function teaquantity() {

		return $this->belongsTo(Teaquantity::class);
	}
}
