<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LocalArea extends Model
{
    protected $fillable = ['area_id', 'local_area_name', 'local_area_name_slug'];

	protected $hidden = ['created_at', 'updated_at'];

	public function area() {

		return $this->belongsTo(Area::class);
	}

	public function customers() {

		return $this->hasMany(Customer::class);
	}

	public function sales_report_customer() {

		return $this->hasMany(SRcustomer::class);
	}

}
