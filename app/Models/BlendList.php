<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BlendList extends Model
{
    protected $guarded = ['id'];

    public function tea_grade()
    {
    	return $this->belongsTo(TeaGrade::class);
    }

    public function tea()
    {
    	return $this->belongsTo(Tea::class);
    }

    public function unload_asset_product_list()
    {
    	return $this->belongsTo(UnloadAssetProductList::class);
    }
}
