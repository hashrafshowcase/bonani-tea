<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UnloadAsset extends Model
{
    protected $guarded = ['id'];

    public function unload_asset_product_list()
    {
    	return $this->hasMany(UnloadAssetProductList::class);
    }

    public function broker()
    {
    	return $this->belongsTo(Brokers::class);
    }
}
