<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Blend extends Model
{
    protected $guarded = ['id'];

    public function blend_lists()
    {
    	return $this->hasMany(BlendList::class);
    }
}
