<?php

namespace App\Models;

use App\User;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{
    protected $table = 'user_role';

    protected $fillable = ['user_id', 'role_id'];

    protected $hidden = ['created_at', 'updated_at'];

    public function user() {

    	return $this->belongsTo(User::class);

    }

    public function role() {

    	return $this->belongsTo(Role::class);
    }
}
