<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TeaGrade extends Model
{
    protected $guarded = ['id'];
}
