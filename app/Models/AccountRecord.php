<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AccountRecord extends Model
{
    protected $guarded = ['id'];

    protected $hidden = ['created_at', 'updated_at'];

    public function assetsTypeDebit()
    {
    	return $this->belongsTo(AssetsType::class, 'debit_assets_type');
    }

    public function assetsTypeCredit()
    {
    	return $this->belongsTo(AssetsType::class, 'credit_assets_type');
    }
}
