<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AccountGeneralLedger extends Model
{
    protected $guarded = ['general_ledger_id'];

    public function account_asset_type()
    {
    	return $this->belongsTo(AccountAssets::class, 'account_asset_type_id');
    }
}
