<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Garden extends Model
{
    protected $fillable = ['garden_name', 'garden_address'];

	protected $hidden = ['created_at', 'updated_at'];

}
