<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AssetTypeList extends Model
{
    protected $guraded = ['id'];

    protected $hidden = ['created_at', 'updated_at'];

    public function assetsType()
    {
    	return $this->belongsTo(AssetsType::class, 'parent_id');
    }
}
