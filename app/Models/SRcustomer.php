<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\User;

class SRcustomer extends Model
{
    protected $table = "sales_report_customer";

    protected $guarded = ['id'];

	protected $hidden = ['created_at', 'updated_at'];



	/**
	 * [sales_report description]
	 * @return [type] [description]
	 */
	public function sales_report() {

		return $this->belongsTo(SalesReport::class);
	}

	public function salesman() {
		return $this->belongsTo(Salesman::class);
	}



	/**
	 * [customer description]
	 * @return [type] [description]
	 */
	public function customer() {

		return $this->belongsTo(Customer::class);
	}



	public function area() {

		return $this->belongsTo(Area::class);
	}



	/**
	 * [localarea description]
	 * @return [type] [description]
	 */
	public function local_area() {

		return $this->belongsTo(LocalArea::class);
	}



	/**
	 * [sales_report_quantites description]
	 * @return [type] [description]
	 */
	public function sales_report_quantites() {

		return $this->hasMany(SRQuantities::class);
	}


	/**
	 * [sales_report_customer_id description]
	 * @return [type] [description]
	 */
	public function sales_report_customer_id() {

		return $this->hasMany(SRQuantities::class);
	}

}
