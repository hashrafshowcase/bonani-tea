<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Salesman extends Model
{
    protected $fillable = ['user_id', 'salesman_phone_number'];

	protected $hidden = ['created_at', 'updated_at'];

	public function user() {
		return $this->belongsTo(User::class);
	}

	public function sales_report() {
		return $this->hasMany(SalesReport::class);
	}
}
