<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AccountSubsidiaryLedger extends Model
{
    protected $guarded = ['subsidiary_ledger_id'];

    public function account_asset_type()
    {
    	return $this->belongsTo(AccountAssets::class,'account_asset_type_id');
    }

    public function account_general_ledger()
    {
    	return $this->belongsTo(AccountGeneralLedger::class, 'account_general_ledger_id', 'general_ledger_id');
    }
}
