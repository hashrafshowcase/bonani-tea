<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StockIndex extends Model
{
    protected $guarded = ['id'];

    protected $hidden = ['created_at', 'updated_at'];

    public function stock_category()
    {
    	return $this->belongsTo(StockCategory::class);
    }

    public function teaquantity()
    {
    	return $this->belongsTo(Teaquantity::class);
    }

    public function stocks()
    {
    	return $this->hasMany(Stocks::class, 'stock_indices_id');
    }
}
