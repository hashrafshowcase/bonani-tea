<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Models\Tea;
use App\Models\Teaquantity;
use App\Http\Requests\TeaRequest;
use App\Models\StockIndex;

use Requent;
use PDF;
use Carbon\Carbon;

class TeaController extends Controller
{
	protected $request;
	protected $tea;
	protected $quantity;
  protected $requent;
  protected $carbon;
  protected $stockIndex;

	public function __construct(
    Request $request, Tea $tea, Teaquantity $quantity, Requent $requent, Carbon $carbon,
    StockIndex $stockIndex
  ) {

		$this->request = $request;
		$this->tea = $tea;
		$this->quantity = $quantity;
    $this->requent = $requent;
    $this->carbon = $carbon;
		$this->middleware('roles');
    $this->stockIndex = $stockIndex;

	}


	public function teaIndex() {


		  return $this->requent::resource($this->tea->orderBy('id', 'desc'))->get();

   }


 	public function findTea($id) {

 		return $this->requent::resource($this->tea)->find($id);
 	}



 	public function createOrUpdateTea() {


 		$input = $data[0] = $this->request->all();
   
     if(!isset($input['tea_id'])) {

        $this->tea->create([
            'name' => $input['name'],
            'slug' => str_slug($input['name'])
          ]);

        return response()->json([
            'message' => 'Tea Create Succesfully'
          ], 200);

     } else {

        $this->tea->where('id', $input['tea_id'])->update([
            'name' => $input['name'],
            'slug' => str_slug($input['name'])
          ]);

        return response()->json([
          'message' => 'Update Succesfully'
        ], 200);
     }
 	}



 	public function deleteTea($id) {

 		$tea = $this->tea->find($id);
      try{
          $delete = $tea->delete();
          if($delete){
              return response()->json([
                  'message' => ' Deleted Succesfully'
              ], 200);
          }
      } catch(\Exception $e) {
              return response()->json([
                  'message' => ' Already Use in quantities'
              ], 400);
      }
 		
 	}



 	public function quantitiesIndex() {

    
    return $this->requent::resource($this->quantity->orderBy('name', 'ASC')->orderBy('short_name', 'ASC')->orderBy('amount', 'ASC'))->get();
 	}



 	public function findQuantity($id) {

 		return $this->requent::resource($this->quantity)->find($id);
 	}


 	public function createOrUpdateQuantity() {


 		$input = $data[0] = $this->request->all();

     if(!isset($input['tea_quantity_id'])) {

   		$teaQuantityInsertId = $this->quantity->insert([
   				'tea_id' => $input['tea_id'],
   				'name' => $input['name'],
   				'short_name' => $input['short_name'],
          'kg' => $input['kg'],
   				'amount' => $input['amount']
   			]);

      $this->stockIndex->create([
        'stock_category_id' => 1,
        'teaquantities_id'  => $teaQuantityInsertId,
        'stock_indices_name' => $input['name'].' - FD',
        'stock_indices_month' => 'all'
      ]);

      $this->stockIndex->create([
        'stock_category_id' => 2,
        'teaquantities_id'  => $teaQuantityInsertId,
        'stock_indices_name' => $input['name'].' - SR',
        'stock_indices_month' => 'all'
      ]);

 		   return response()->json([
 					'message' => 'Succesfully Quantity Added.'
 				],200);
    } else {
        $this->quantity->where('id', $input['tea_quantity_id'])->update([
          'tea_id' => $input['tea_id'],
          'name' => $input['name'],
          'short_name' => $input['short_name'],
          'kg' => $input['kg'],
          'amount' => $input['amount']
        ]);
        return response()->json([
          'message' => 'Succesfully Quantity Updated.'
        ],200);
    }

 	}

  public function deleteTeaQuantities($id) {

    $tea = $this->quantity->find($id);
      try{
          $delete = $tea->delete();
          if($delete){
              return response()->json([
                  'message' => ' Deleted Succesfully'
              ], 200);
          }
      } catch(\Exception $e) {
              return response()->json([
                  'message' => ' Already Use in quantities'
              ], 400);
      }
    
  }

  public function export_tea()
  {
    
  }
}
