<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Models\StockIndex;
use App\Models\Production;
use App\Models\Stocks;
use Carbon\Carbon;

class ProductionController extends Controller
{
    protected $stockIndex;
    protected $production;
    protected $stocks;
    protected $request;
    protected $carbon;

    public function __construct(
    	StockIndex $stockIndex,
    	Production $production,
    	Stocks $stocks,
		Request $request,
		Carbon $carbon
    )
    {
    	$this->stockIndex = $stockIndex;
    	$this->production = $production;
    	$this->stocks = $stocks;
    	$this->request = $request;
    	$this->carbon = $carbon;
    }

    public function getProductIndex()
    {
    	$getIndex = $this->stockIndex->where('stock_category_id', 1)->get();

    	return response()->json([
    		'product_index' => $getIndex
    	], 200);
    }

    public function storeProduction()
    {
    	$input = $data[0] = $this->request->all();

    	foreach ($input['production_list'] as $key => $value) {
    		$this->production->create([
    			'production_date' => $input['production_date'],
    			'stock_index_id'  => $value['stock_index_id'],
    			'packets' 		  => $value['packet']
     		]);

     		$getStock = $this->stocks->where('stock_indices_id', $value['stock_index_id'])->whereDate('curent_date', $input['production_date'])->first();

     		if( ! $getStock ) {
     			$getLatestStock = $this->stocks->where('stock_indices_id', $value['stock_index_id'])->latest()->first();
     			if(! $getLatestStock ) {
     				$this->stocks->insert([
     					'stock_indices_id' => $value['stock_index_id'],
     					'curent_date'      => $input['production_date'],
     					'stocks_past_deposit' => 0,
     					'stocks_new_deposit' => $value['packet'],
     					'stocks_new_deposit_cash_memo' => 'First Deposit',
     					'stocks_sum_deposit' => $value['packet'],
     					'stocks_quantity_sold' => 0,
     					'stocks_quantity_sold_cash_memo' => 'First Cash Memo',
     					'stocks_balance' => $value['packet']
     				]);
     			} else {
     				$balance = $getLatestStock->stocks_balance + intval($value['packet']);
     				$this->stocks->insert([
     					'stock_indices_id' => $value['stock_index_id'],
     					'curent_date'      => $input['production_date'],
     					'stocks_past_deposit' => $getLatestStock->stocks_balance,
     					'stocks_new_deposit' => $value['packet'],
     					'stocks_new_deposit_cash_memo' => 'First Deposit',
     					'stocks_sum_deposit' => $balance,
     					'stocks_quantity_sold' => 0,
     					'stocks_quantity_sold_cash_memo' => 'First Cash Memo',
     					'stocks_balance' => $balance
     				]);

     			}
     		} else {
     			$currentNewStockBalance = $getStock->stocks_new_deposit + intval( $value['packet'] );
                $currentStockSum = $getStock->stocks_past_deposit + $currentNewStockBalance;
                $currentBalance = $currentStockSum - $getStock->stocks_quantity_sold;
     			$this->stocks->where('stock_indices_id', $value['stock_index_id'])->where('curent_date', $input['production_date'])->update([
     				'stocks_new_deposit' => $currentNewStockBalance,
                    'stocks_sum_deposit' => $currentStockSum,
     				'stocks_balance' => $currentBalance
     			]);
     		}
    	}

    	return response()->json([
    		'message' => 'Succesfully Added!'
    	], 200);
    }

     public function productionHistory($type = false)
    {
        $status = $type ? $type : 'daily';
        $status = explode('advance-', $status);
        $status = count($status) > 1 ? $status : $status[0];
        if($status == 'daily') {

            $productionHistoryDaily = $this->production->whereBetween('production_date', [$this->carbon::now()->startOfMonth(), $this->carbon::now()])->get();


            $productionDailyWise = $productionHistoryDaily->groupBy(function($date) {
                return $this->carbon::parse($date->production_date)->toDateString();
            });

            $productionDailyWiseModified = [];
            foreach ($productionDailyWise as $key => $value) {
                $data = [
                    'title' => $key,
                    'type' => 'daily',
                    'value' => $productionDailyWise[$key]->sum('packets')
                ];
                array_push($productionDailyWiseModified, $data);
            }

            $data = [
                'type' => $status,
                'current_year' => $this->carbon->format('Y'),
                'current_month' => $this->carbon->format('F'),
                'current_date' => $this->carbon::now()->toFormattedDateString(),
                'data' => $productionDailyWiseModified
            ];
            return response()->json($data, 200);
        }
        else if($status == 'monthly') {
            $productionHistoryMonthly = $this->production->whereBetween('production_date', [$this->carbon::now()->startOfYear(), $this->carbon::now()])->orderBy('production_date', 'DESC')->get();

            $productionMonthlyWise = $productionHistoryMonthly->groupBy(function($date) {
                return $this->carbon::parse($date->production_date)->format('F');
            });


            $productionMonthlyWiseModified = [];
            foreach ($productionMonthlyWise as $key => $value) {
                $data = [
                    'title' => $key,
                    'type' => 'monthly',
                    'value' => $productionMonthlyWise[$key]->sum('packets')
                ];
                array_push($productionMonthlyWiseModified, $data);
            }

            // return $productionMonthlyWiseModified;
            // return $productionHistoryMonthly;
            $data = [
                'type' => $status,
                'current_year' => $this->carbon->format('Y'),
                'current_month' => $this->carbon->format('F'),
                'current_date' => $this->carbon::now()->toFormattedDateString(),
                'data' => $productionMonthlyWiseModified
            ];
            return response()->json($data, 200);
        }
        else if($status == 'yearly')
        {
            $productionHistoryYearly = $this->production->whereBetween('production_date', [$this->carbon::create(2017, 01, 01, 00, 00, 00), $this->carbon::now()])->get();

            $productionHistoryYearyWise = $productionHistoryYearly->groupBy(function($date) {
                return $this->carbon::parse($date->production_date)->format('Y');
            });

            $productionHistoryYearyWiseModified = [];
            foreach ($productionHistoryYearyWise as $key => $value) {
               $data = [
                    'title' => $key,
                    'type' => 'yearly',
                    'value' => $productionHistoryYearyWise[$key]->sum('packets')
               ];
               array_push($productionHistoryYearyWiseModified, $data);
            }

            $data = [
                'type' => $status,
                'current_year' => $this->carbon->format('Y'),
                'current_month' => $this->carbon->format('F'),
                'current_date' => $this->carbon::now()->toFormattedDateString(),
                'data' => $productionHistoryYearyWiseModified
            ];
            return response()->json($data, 200);
        }
        else if(count($status) > 1) {

            $productionHistoryAdvance = $this->production->whereBetween('production_date', [$this->carbon::createFromFormat('Y-m-d', $status[1])->startOfMonth(), $this->carbon::createFromFormat('Y-m-d', $status[1])->endOfMonth()])->get();


            $productionAdvanceWise = $productionHistoryAdvance->groupBy(function($date) {
                return $this->carbon::parse($date->production_date)->toDateString();
            });

            // return $productionAdvanceWise;
            $productionAdvanceWiseModified = [];
            foreach ($productionAdvanceWise as $key => $value) {
                $data = [
                    'title' => $key,
                    'type' => 'advance',
                    'value' => $productionAdvanceWise[$key]->sum('packets')
                ];
                array_push($productionAdvanceWiseModified, $data);
            }

            $data = [
                'type' => 'advance',
                'current_year' => $this->carbon->format('Y'),
                'current_month' => $this->carbon->format('F'),
                'current_date' => $this->carbon::now()->toFormattedDateString(),
                'data' => $productionAdvanceWiseModified
            ];
            return response()->json($data, 200);
        }
    }

    public function productionSearch(){
        $input = $data[0] = $this->request->all();

        if(isset($input['id']))
        {
            $productionById = $this->production->with('stock_index')->where('id', '=', $input['id'])->get();
            return response()->json([
                'data' => $productionById
            ], 200);
        }
        else if(isset($input['daily']))
        {
            $startDay = $this->carbon::createFromFormat('Y-m-d', $input['daily'])->startOfDay();
            $endDay = $this->carbon::createFromFormat('Y-m-d', $input['daily'])->endOfDay();
            $productionDaily = $this->production->with('stock_index')->whereBetween('production_date', [$startDay, $endDay])->get();

            return $productionDaily;
        }
        else if(isset($input['monthly']))
        {
            $startDay = $this->carbon::createFromFormat('F', $input['monthly'])->startOfMonth();
            $endDay = $this->carbon::createFromFormat('F', $input['monthly'])->endOfMonth();
            $productionMonthly = $this->production->with('stock_index')->whereBetween('production_date', [$startDay, $endDay])->get();

            $productionDailyWise = $productionMonthly->groupBy(function($date) {
                return $this->carbon::parse($date->production_date)->toDateString();
            });

            $productionDailyWiseModified = [];
            foreach ($productionDailyWise as $key => $value) {
                $data = [
                    'title' => $key,
                    'type' => 'daily',
                    'value' => $productionDailyWise[$key]->sum('packets')
                ];
                array_push($productionDailyWiseModified, $data);
            }

            $data = [
                'type' => 'daily',
                'current_year' => $this->carbon->format('Y'),
                'current_month' => $this->carbon->format('F'),
                'current_date' => $this->carbon::now()->toFormattedDateString(),
                'data' => $productionDailyWiseModified
            ];
            return response()->json($data, 200);
        }
        else if(isset($input['yearly']))
        {
            $startMonth= $this->carbon::createFromFormat('Y', $input['yearly'])->startOfYear();
            $endMonth = $this->carbon::createFromFormat('Y', $input['yearly'])->endOfYear();
            $productionHistoryMonthly = $this->production->with('stock_index')->whereBetween('production_date', [$startMonth, $endMonth])->orderBy('production_date', 'DESC')->get();

            $productionMonthlyWise = $productionHistoryMonthly->groupBy(function($date) {
                return $this->carbon::parse($date->production_date)->format('F');
            });

            $productionMonthlyWiseModified = [];
            foreach ($productionMonthlyWise as $key => $value) {
                $data = [
                    'title' => $key,
                    'type' => 'monthly',
                    'value' => $productionMonthlyWise[$key]->sum('packets')
                ];
                array_push($productionMonthlyWiseModified, $data);
            }

            // return $productionMonthlyWiseModified;
            // return $productionHistoryMonthly;
            $data = [
                'type' => 'monthly',
                'current_year' => $this->carbon->format('Y'),
                'current_month' => $this->carbon->format('F'),
                'current_date' => $this->carbon::now()->toFormattedDateString(),
                'data' => $productionMonthlyWiseModified
            ];
            return response()->json($data, 200);
        }
    }
}
