<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Models\AccountAssets;
use App\Models\AccountGeneralLedger;
use App\Models\AccountSubsidiaryLedger;
use App\Models\AccountDetailsStore;
use App\Models\AccountTransaction;
use Carbon\Carbon;

class AccountAssetController extends Controller
{
    protected $accountAsset;
    protected $request;
    protected $general_ledger;
    protected $subsidiary_ledger;
    protected $store;
    protected $transaction;
    protected $carbon;

    public function __construct(
        Carbon $carbon,
        AccountTransaction $transaction,
        AccountDetailsStore $store,
        AccountSubsidiaryLedger $subsidiary_ledger,
    	AccountGeneralLedger $general_ledger,
    	AccountAssets $accountAsset,
    	Request $request
    )
    {
        $this->transaction = $transaction;
        $this->store = $store;
        $this->subsidiary_ledger = $subsidiary_ledger;
    	$this->general_ledger = $general_ledger;
    	$this->accountAsset = $accountAsset;
    	$this->request = $request;
        $this->carbon = $carbon;
    }

    public function balanceSheet()
    {
        $input = $data[0] = $this->request->all();

        $getAsset = $this->general_ledger->where('account_asset_type_id', 1)->where('status', 'open')->select('general_ledger_id as id')->get();

        $getLiabilities = $this->general_ledger->where('account_asset_type_id', 2)->where('status', 'open')->select('general_ledger_id as id')->get();

        $getEquity = $this->general_ledger->where('account_asset_type_id', 3)->where('status', 'open')->select('general_ledger_id as id')->get();

        $getAsset = collect($getAsset)->pluck('id');
        $getLiabilities = collect($getLiabilities)->pluck('id');
        $getEquity = collect($getEquity)->pluck('id');

        $getAssetDebit = collect($this->store->whereIn('account_type_id', $getAsset)->whereBetween('store_date', [$this->carbon::createFromFormat('Y-m-d', $input['date'])->startOfMonth(), $this->carbon::createFromFormat('Y-m-d', $input['date'])->endOfMonth()])->where('type', 'debit')->get())->pluck('value')->sum();

        $getAssetCredit = collect($this->store->whereIn('account_type_id', $getAsset)->whereBetween('store_date', [$this->carbon::createFromFormat('Y-m-d', $input['date'])->startOfMonth(), $this->carbon::createFromFormat('Y-m-d', $input['date'])->endOfMonth()])->where('type', 'credit')->get())->pluck('value')->sum();

        $getLiabilitiesDebit = collect($this->store->whereIn('account_type_id', $getLiabilities)->whereBetween('store_date', [$this->carbon::createFromFormat('Y-m-d', $input['date'])->startOfMonth(), $this->carbon::createFromFormat('Y-m-d', $input['date'])->endOfMonth()])->where('type', 'debit')->get())->pluck('value')->sum();

        $getLiabilitiesCredit = collect($this->store->whereIn('account_type_id', $getLiabilities)->whereBetween('store_date', [$this->carbon::createFromFormat('Y-m-d', $input['date'])->startOfMonth(), $this->carbon::createFromFormat('Y-m-d', $input['date'])->endOfMonth()])->where('type', 'credit')->get())->pluck('value')->sum();

        $getEquityDebit = collect($this->store->whereIn('account_type_id', $getEquity)->whereBetween('store_date', [$this->carbon::createFromFormat('Y-m-d', $input['date'])->startOfMonth(), $this->carbon::createFromFormat('Y-m-d', $input['date'])->endOfMonth()])->where('type', 'debit')->get())->pluck('value')->sum();
        $getEquityCredit = collect($this->store->whereIn('account_type_id', $getEquity)->whereBetween('store_date', [$this->carbon::createFromFormat('Y-m-d', $input['date'])->startOfMonth(), $this->carbon::createFromFormat('Y-m-d', $input['date'])->endOfMonth()])->where('type', 'credit')->get())->pluck('value')->sum();

        return response()->json([
            'assets' => max($getAssetDebit, $getAssetCredit),
            'liabilities' => max($getLiabilitiesDebit, $getLiabilitiesCredit),
            'equity' => max($getEquityDebit, $getEquityCredit),
            'lia_equ' => max($getLiabilitiesDebit, $getLiabilitiesCredit) + max($getEquityDebit, $getEquityCredit),
        ], 200);
    }

    public function netIncome()
    {
        $input = $data[0] = $this->request->all();

        $getRevenue = $this->general_ledger->where('account_asset_type_id', 4)->where('status', 'open')->select('general_ledger_id as id')->get();

        $getGain = $this->general_ledger->where('account_asset_type_id', 6)->where('status', 'open')->select('general_ledger_id as id')->get();

        $getExpense = $this->general_ledger->where('account_asset_type_id', 5)->where('status', 'open')->select('general_ledger_id as id')->get();

        $getLoss = $this->general_ledger->where('account_asset_type_id', 7)->where('status', 'open')->select('general_ledger_id as id')->get();

        $getRevenue = collect($getRevenue)->pluck('id');
        $getGain = collect($getGain)->pluck('id');
        $getExpense = collect($getExpense)->pluck('id');
        $getLoss = collect($getLoss)->pluck('id');

        $getRevenueCredit = collect($this->store->whereIn('account_type_id', $getRevenue)->whereBetween('store_date', [$this->carbon::createFromFormat('Y-m-d', $input['date'])->startOfMonth(), $this->carbon::createFromFormat('Y-m-d', $input['date'])->endOfMonth()])->where('type', 'credit')->get())->pluck('value')->sum();


        $getGainCredit = collect($this->store->whereIn('account_type_id', $getGain)->whereBetween('store_date', [$this->carbon::createFromFormat('Y-m-d', $input['date'])->startOfMonth(), $this->carbon::createFromFormat('Y-m-d', $input['date'])->endOfMonth()])->where('type', 'credit')->get())->pluck('value')->sum();

        $getExpenseDebit = collect($this->store->whereIn('account_type_id', $getExpense)->whereBetween('store_date', [$this->carbon::createFromFormat('Y-m-d', $input['date'])->startOfMonth(), $this->carbon::createFromFormat('Y-m-d', $input['date'])->endOfMonth()])->where('type', 'debit')->get())->pluck('value')->sum();


        $getLossDebit = collect($this->store->whereIn('account_type_id', $getLoss)->whereBetween('store_date', [$this->carbon::createFromFormat('Y-m-d', $input['date'])->startOfMonth(), $this->carbon::createFromFormat('Y-m-d', $input['date'])->endOfMonth()])->where('type', 'debit')->get())->pluck('value')->sum();

        return response()->json([
            'revenue' => $getRevenueCredit,
            'gain'    => $getGainCredit,
            'expense' => $getExpenseDebit,
            'loss'    => $getLossDebit,
            'negative' => ($getRevenueCredit + $getGainCredit) - ($getExpenseDebit + $getLossDebit) < 0,
            'total'   => abs(($getRevenueCredit + $getGainCredit) - ($getExpenseDebit + $getLossDebit))
        ], 200);
    }

    public function getAccountDetails()
    {
        $input = $data[0] = $this->request->all();

        $getDataFromStore = $this->store->where('account_type_id', $input['account_id'])->where('ledger_type', $input['account_type'])->whereBetween('store_date', [$input['start_date'], $input['end_date']])->get();

        foreach ($getDataFromStore as $key1 => $account) {
            $account->asset_type = $this->accountAsset->find($account->account_asset_type_id);
            if( $account->ledger_type == 'general' ) {
            $account->account = $this->general_ledger->where('general_ledger_id',$account->account_type_id)->first()->general_ledger_name;
            } else {
            $account->account = $this->subsidiary_ledger->where('subsidiary_ledger_id',$account->account_type_id)->first()->subsidiary_ledger_name;
            }

            if($account->against_ledger_type == 'general') {
                $account->against_name = $this->general_ledger->where('general_ledger_id',$account->against_type_id)->first()->general_ledger_name;
            } else {
                $account->against_name = $this->subsidiary_ledger->where('subsidiary_ledger_id',$account->against_type_id)->first()->subsidiary_ledger_name;
            }
       }

        return response()->json([ 
            'store' => $getDataFromStore
        ], 200);
    }

    public function getDateRangeJournalEntries()
    {
        $input = $data[0] = $this->request->all();

        $getStoreData = $this->transaction->with('account_details_stores')->whereBetween('transaction_date', [ $input['start_date'], $input['end_date'] ])->get();

        foreach ($getStoreData as $key => $store) {
           foreach ($store->account_details_stores as $key1 => $account) {
              $account->asset_type = $this->accountAsset->find($account->account_asset_type_id);
              if( $account->ledger_type == 'general' ) {
                $account->account = $this->general_ledger->where('general_ledger_id',$account->account_type_id)->first()->general_ledger_name;
              } else {
                $account->account = $this->subsidiary_ledger->where('subsidiary_ledger_id',$account->account_type_id)->first()->subsidiary_ledger_name;
              }
           }
        }

        return response()->json([ 
            'store' => $getStoreData
        ], 200);

    }

    public function storeTransactionDetailsInStore()
    {
        $input = $data[0] = $this->request->all();

        $getTransaction = $this->transaction->create([
            'transaction_date'  => $input['record_date'],
            'note'              => $input['note']
        ]);

        foreach ($input['store'] as $key => $store) {
            
            if( $store['account_type']['is_parent'] ) {
                $this->store->create([
                    'account_transaction_id' => $getTransaction->id,
                    'store_date'             => $input['record_date'],
                    'account_asset_type_id'  => $store['account_type']['account_asset_type_id'],
                    'account_type_id'        => isset($store['account_type']['parent_id']) ? $store['account_type']['parent_id'] : $store['account_type']['id'],
                    'ledger_type'            => 'general',
                    'against_type_id'        => $store['against_type_id'],
                    'against_ledger_type'   => $store['against_ledger_type'],
                    'type'                   => $store['type'],
                    'value'                  => $store['value']
                ]);
            }

            if( ! $store['account_type']['is_parent'] ) {
                $this->store->create([
                    'account_transaction_id' => $getTransaction->id,
                    'store_date'             => $input['record_date'],
                    'account_asset_type_id'  => $store['account_type']['account_asset_type_id'],
                    'account_type_id'        => $store['account_type']['parent_id'],
                    'ledger_type'            => 'general',
                    'against_type_id'        => $store['against_type_id'],
                    'against_ledger_type'   => $store['against_ledger_type'],
                    'type'                   => $store['type'],
                    'value'                  => $store['value']
                ]);
                $this->store->create([
                    'account_transaction_id' => $getTransaction->id,
                    'store_date'             => $input['record_date'],
                    'account_asset_type_id'  => $store['account_type']['account_asset_type_id'],
                    'account_type_id'        => $store['account_type']['id'],
                    'against_type_id'        => $store['against_type_id'],
                    'ledger_type'            => 'subsidiary',
                    'against_ledger_type'   => $store['against_ledger_type'],
                    'type'                   => $store['type'],
                    'value'                  => $store['value']
                ]);
            }

        }

        return response()->json([
            'message' => 'Successfully Stored!'
        ], 200);
    }

    public function getLedgerSheet() 
    {
        $general = $this->general_ledger->get();
        $subsidiary = $this->subsidiary_ledger->get();

        $data = array();

        foreach ($general as $key => $value) {
            $args = array(
                'id'            => $value->general_ledger_id,
                'type'          => 'general',
                'account_name'  => $value->general_ledger_name,
                'account_asset_type_id' => $value->account_asset_type_id,
                'is_parent'     => true
            );

            array_push($data, $args);
        }

        foreach ($subsidiary as $key => $value) {
            $args = array(
                'id'            => $value->subsidiary_ledger_id,
                'type'          => 'subsidiary',
                'account_name'  => $value->subsidiary_ledger_name,
                'account_asset_type_id' => $value->account_asset_type_id,
                'is_parent'     => false,
                'parent_id'     => $value->account_general_ledger_id
            );

            array_push($data, $args);
        }

        return response()->json([
            'ledger' => $data
        ], 200);
    }

    public function storeAccountSubsidiaryLedger()
    {
        $input = $data[0] = $this->request->all();
        if( isset($input['subsidiary_ledger_id']) ) {
            $getInsertId = $this->subsidiary_ledger->where('subsidiary_ledger_id', $input['subsidiary_ledger_id'])->update([
                'account_asset_type_id' => $input['account_asset_type_id'],
                'account_general_ledger_id' => $input['account_general_ledger_id'],
                'subsidiary_ledger_name'   => $input['subsidiary_ledger_name'],
                'ref_no'                => $input['ref_no'],
                'opening_balance'       => $input['opening_balance'],
                'note'                  => $input['note']
            ]);

            $getAccountAssets = $this->subsidiary_ledger->with(['account_asset_type', 'account_general_ledger'])->get();

            return response()->json([
                'message' => 'Successfully Updated!',
                'subsidiary' => $getAccountAssets
            ], 200);
        }
        $getInsertId = $this->subsidiary_ledger->create([
            'account_asset_type_id' => $input['account_asset_type_id'],
            'account_general_ledger_id' => $input['account_general_ledger_id'],
            'subsidiary_ledger_name'   => $input['subsidiary_ledger_name'],
            'ref_no'                => $input['ref_no'],
            'opening_balance'       => $input['opening_balance'],
            'note'                  => $input['note']
        ]);

        $getAccountAssets = $this->subsidiary_ledger->with(['account_asset_type', 'account_general_ledger'])->get();


        $input['subsidiary_ledger_id'] = $getInsertId->id;

        $transaction = $this->transaction->create([
            'transaction_date'  => $this->carbon::now(),
            'note'              => 'Opening Balance'
        ]);

        $input['account_transaction_id'] = $transaction->id;

        $this->store->create( $this->AddStoreOpeningBalance($input) );

        return response()->json([
            'message' => 'Successfully Created!',
            'subsidiary' => $getAccountAssets
        ], 200);

    }

    public function getAccountSubsidiaryLedger()
    {
        $getSubsidiaryLedger = $this->subsidiary_ledger->with(['account_asset_type', 'account_general_ledger'])->get();

        return response()->json([
            'subsidiary' => $getSubsidiaryLedger
        ], 200);
    }

    public function getAccountGeneralLedger()
    {
    	$getGeneralLedger = $this->general_ledger->with(['account_asset_type'])->get();

    	return response()->json([
    		'ledger' => $getGeneralLedger
    	], 200);
    }

    public function storeAccountGeneralLedger()
    {
    	$input = $data[0] = $this->request->all();
    	if( isset($input['general_ledger_id']) ) {
    		$getInsertId = $this->general_ledger->where('general_ledger_id', $input['general_ledger_id'])->update([
	    		'account_asset_type_id' => $input['account_asset_type_id'],
	    		'general_ledger_name'  	=> $input['general_ledger_name'],
	    		'ref_no' 				=> $input['ref_no'],
	    		'opening_balance' 		=> $input['opening_balance'],
	    		'note'					=> $input['note']
	    	]);

	    	$getAccountAssets = $this->general_ledger->with(['account_asset_type'])->get();

	    	return response()->json([
	    		'message' => 'Successfully Updated!',
	    		'ledger' => $getAccountAssets
	    	], 200);
    	}
    	$getInsertId = $this->general_ledger->create([
    		'account_asset_type_id' => $input['account_asset_type_id'],
    		'general_ledger_name'  	=> $input['general_ledger_name'],
    		'ref_no' 				=> $input['ref_no'],
    		'opening_balance' 		=> $input['opening_balance'],
    		'note'					=> $input['note']
    	]);

        $input['general_ledger_id'] = $getInsertId->id;

        $transaction = $this->transaction->create([
            'transaction_date'  => $this->carbon::now(),
            'note'              => 'Opening Balance'
        ]);

        $input['account_transaction_id'] = $transaction->id;

        $this->store->create( $this->AddStoreOpeningBalance($input) );

    	$getAccountAssets = $this->general_ledger->with(['account_asset_type'])->get();

    	return response()->json([
    		'message' => 'Successfully Created!',
    		'ledger' => $getAccountAssets
    	], 200);

    }

    public function getAccountAssets( $message = false )
    {
    	$getAccountAssets = $this->accountAsset->get();

    	return response()->json([
    		'message' => 'Successfully ' . $message,
    		'asset' => $getAccountAssets
    	], 200);
    }

    public function storeAccountAsset()
    {
    	$input = $data[0] = $this->request->all();

    	if( isset($input['id']) ) {
    		$this->accountAsset->where('id', $input['id'])->update([
	    		'account_type_name' => $input['account_type_name'],
	    		'type_ref' 			=> $input['type_ref']
	    	]);

	    	$getAccountAssets = $this->accountAsset->get();

	    	return response()->json([
	    		'message' => 'Successfully Updated!',
	    		'asset' => $getAccountAssets
	    	], 200);
    	}

    	$getInsertId = $this->accountAsset->insert([
    		'account_type_name' => $input['account_type_name'],
    		'type_ref' 			=> $input['type_ref']
    	]);

    	$getAccountAssets = $this->accountAsset->get();

    	return response()->json([
    		'message' => 'Successfully Created!',
    		'asset' => $getAccountAssets
    	], 200);
   
    }

    private function AddStoreOpeningBalance($input)
    {
        $data = array();

        if( isset($input['general_ledger_id']) ) {
            $data['store_date'] = $this->carbon::now();
            $data['account_transaction_id'] = $input['account_transaction_id'];
            $data['account_asset_type_id'] = $input['account_asset_type_id'];
            $data['against_type_id'] = $input['general_ledger_id'];
            $data['account_type_id'] = $input['general_ledger_id'];
            $data['ledger_type'] = 'general';
            $data['against_ledger_type'] = 'general';
            $data['type'] = $this->setAssetTypeRule($input['account_asset_type_id']);
            $data['value'] = $input['opening_balance'];
        }

        if( isset($input['subsidiary_ledger_id']) ) {
            $data['store_date'] = $this->carbon::now();
            $data['account_transaction_id'] = $input['account_transaction_id'];
            $data['account_asset_type_id'] = $input['account_asset_type_id'];
            $data['against_type_id'] = $input['subsidiary_ledger_id'];
            $data['account_type_id'] = $input['subsidiary_ledger_id'];
            $data['ledger_type'] = 'subsidiary';
            $data['against_ledger_type'] = 'subsidiary';
            $data['type'] = $this->setAssetTypeRule($input['account_asset_type_id']);
            $data['value'] = $input['opening_balance'];
        }

        return $data;
    }

    private function setAssetTypeRule($asset_id)
    {
        if( $asset_id == 1 ) {
            return 'debit';
        } else if( $asset_id == 2 ) {
            return 'credit';
        } else if( $asset_id == 3 ) {
            return 'credit';
        } else if( $asset_id == 4 ) {
            return 'credit';
        } else if( $asset_id == 5 ) {
            return 'debit';
        } else if( $asset_id == 6 ) {
            return 'credit';
        } else if( $asset_id == 7 ) {
            return 'debit';
        }
    }
}
