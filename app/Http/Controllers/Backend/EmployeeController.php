<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Models\Employee;
use App\Models\Attendence;
use App\Models\AccountSubsidiaryLedger;
use App\Models\AccountTransaction;
use App\Models\AccountDetailsStore;
use Carbon\Carbon;

class EmployeeController extends Controller
{

	public $employee;
	public $attendence;
	public $subsidiary;
	public $transaction;
	public $store;
	public $request;
	public $pre_subsidiary = array();
	public $carbon;

	public function __construct (
		Employee $employee ,
		Attendence $attendence ,
		AccountSubsidiaryLedger $subsidiary ,
		AccountTransaction $transaction ,
		AccountDetailsStore $store ,
		Request $request,
		Carbon $carbon
	)
	{
		$this->employee = $employee;
		$this->attendence = $attendence;
		$this->subsidiary = $subsidiary;
		$this->transaction = $transaction;
		$this->store = $store;
		$this->request = $request;
		$this->carbon = $carbon;
		$this->pre_subsidiary = array(
			'Salary' => (object) array(
				'general_id' => 1 ,
				'asset_type_id' => 6
			) ,
			'Bonus' => (object) array(
				'general_id' => 2 ,
				'asset_type_id' => 6
			) ,
			'Account_Receivable' => (object) array(
				'general_id' => 3 ,
				'asset_type_id' => 1
			) ,
			'Account_Payable' => (object) array(
				'general_id' => 4 ,
				'asset_type_id' => 2
			)

		);
	}

	public function getSingleEmployee ( $employee_id )
	{
		return response()->json( [
			'employee' => $this->employee->find( $employee_id )
		] , 200 );
	}

	public function getAllEmployee ()
	{
		$getEmployee = $this->employee->select( 'id' , 'employee_name' , 'profile_image_url' , 'designation' )->orderBy( 'employee_name' , 'ASC' )->get();

		return response()->json( [
			'employee' => $getEmployee
		] , 200 );
	}

	public function createEmployee ()
	{
		$input = $data[ 0 ] = $this->request->all();

		$getInsertId = $this->employee->insert( $input );

		$subname = explode(' ', $input['employee_name'])[0];

		foreach ( $this->pre_subsidiary as $key => $value ) {
			$this->storeTransactionDetailsInStore($key, $value, $subname);
		}

		return response()->json(
			[
				'status' => 'success' ,
				'message' => 'Successfully Created!'
			] , 200
		);
	}

	public function storeTransactionDetailsInStore ( $key , $value , $subname)
	{

		$getTransaction = $this->transaction->create( [
			'transaction_date' => date( 'Y-m-d' ) ,
			'note' => 'Add ' . $key
			]
		);

		$createSubsidiary = $this->subsidiary->create(
			[
				'account_asset_type_id' => $value->asset_type_id,
				'account_general_ledger_id' => $value->general_id,
				'subsidiary_ledger_name'    => $subname,
				'ref_no'                    => 'xxx-xxx-xxx',
				'opening_balance'           => 0.00,
				'note'                      => 'Open This Account'
			]
		);

		$subsidiary = AccountSubsidiaryLedger::latest()->first();

		$this->store->create( [
			'account_transaction_id' => $getTransaction->id ,
			'store_date' => date( 'Y-m-d' ) ,
			'account_asset_type_id' => $value->asset_type_id ,
			'account_type_id' => $value->general_id ,
			'ledger_type' => 'general' ,
			'against_type_id' => $value->general_id ,
			'against_ledger_type' => 'general' ,
			'type' => $this->setAssetTypeRule($value->asset_type_id) ,
			'value' => 0.00
		] );

		$this->store->create( [
			'account_transaction_id' => $getTransaction->id ,
			'store_date' => date( 'Y-m-d' ) ,
			'account_asset_type_id' => $value->asset_type_id,
			'account_type_id' => $subsidiary->subsidiary_ledger_id ,
			'against_type_id' => $subsidiary->subsidiary_ledger_id ,
			'ledger_type' => 'subsidiary' ,
			'against_ledger_type' => 'subsidiary' ,
			'type' => $this->setAssetTypeRule($value->asset_type_id) ,
			'value' => 0.00
		] );
	}

	private function setAssetTypeRule($asset_id)
	{
		if( $asset_id == 1 ) {
			return 'debit';
		} else if( $asset_id == 2 ) {
			return 'credit';
		} else if( $asset_id == 3 ) {
			return 'credit';
		} else if( $asset_id == 4 ) {
			return 'credit';
		} else if( $asset_id == 5 ) {
			return 'debit';
		} else if( $asset_id == 6 ) {
			return 'credit';
		} else if( $asset_id == 7 ) {
			return 'debit';
		}
	}

	public function setAndGetAllEmployee() {
		$getCurrentDate = date('Y-m-d');

		$checkSheetExists = $this->attendence->with('employee')->where('present_date', $getCurrentDate)->get();

		if( count($checkSheetExists) ) {

			return response()->json([
				'sheet' => $checkSheetExists
			], 200);

		}

		$getAllEmployee = $this->employee->select('id')->get();

		foreach ($getAllEmployee as $key => $value) {

			$args = [
				'employee_id'   => $value->id,
				'present_date'  => $getCurrentDate,
				'status'        => 'absent',
				'created_at'    => date('Y-m-d h:i:s'),
				'updated_at'    => date('Y-m-d h:i:s')
			];

			$this->attendence->create($args);

		}

		$getAttendence = $this->attendence->with('employee')->where('present_date', $getCurrentDate)->get();

		return response()->json([
			'sheet' => $getAttendence
		], 200);

	}

	public function getAttendenceSheetByDate($date) {

		$getCurrentDate = $date;

		$checkSheetExists = $this->attendence->with('employee')->where('present_date', $getCurrentDate)->get();

		if( count($checkSheetExists) ) {

			return response()->json([
				'sheet' => $checkSheetExists
			], 200);

		}

		$getAllEmployee = $this->employee->select('id')->get();

		foreach ($getAllEmployee as $key => $value) {

			$args = [
				'employee_id'   => $value->id,
				'present_date'  => $getCurrentDate,
				'status'        => 'absent',
				'created_at'    => date('Y-m-d h:i:s'),
				'updated_at'    => date('Y-m-d h:i:s')
			];

			$this->attendence->create($args);

		}

		$getAttendence = $this->attendence->with('employee')->where('present_date', $getCurrentDate)->get();

		return response()->json([
			'sheet' => $getAttendence
		], 200);

	}

	public function setAttendence()
	{
		$input = $data[0] = $this->request->all();

		$arg = [
			'status' => $input['status']
		];

		$this->attendence->where('id', $input['id'])->where('employee_id', $input['employee_id'])->update($arg);

		return response()->json([
			'status' => 'success',
			'message' => 'Successfully Updated!',
			'id' => $input['id']
		], 200);

	}


}
