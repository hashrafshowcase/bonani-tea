<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Models\Salesman;
use App\Models\User;
use Requent;

class SalesmanController extends Controller
{
    protected $salesman;
    protected $request;
    protected $user;


    public function __construct(Request $request, Salesman $salesman, User $user)
    {
    	$this->salesman = $salesman;
    	$this->request = $request;
        $this->user = $user;
    }

    public function createSalesMan()
    {
        $input = $data[0] = $this->request->all();

        if( ! $input['user_name'] ) {
            return response()->json([
                'message'   => 'User Name is not defined'
            ], 400);
        }

        if( ! $input['user_email'] ) {
            return response()->json([
                'message' => 'User email is not '
            ], 400);
        }

        if( ! $input['password'] ) {
            return response()->json([
                'message' => 'Password field is empty'
            ], 400);
        }

        if( ! $input['phone_number'] ) {
            return response()->json([
                'message' => 'Phone Number field is empty'
            ], 400);
        }

        $data = [
            'name' => $input['user_name'],
            'email' => $input['user_email'],
            'password' => bcrypt($input['password'])
        ];

        $getUserInsertId = $this->user->insert($data);

        $args = [
            'user_id'   => $getUserInsertId,
            'salesman_phone_number' => $input['phone_number']
        ];

        $this->salesman->insert($args);

        $currentUser = $this->user->find($getUserInsertId);

        $allUser = $this->user->get();

        return response()->json([
            'message'  => 'Salesman Created',
            'users' => $allUser,
            'current_user_add' => $currentUser
        ], 200);  
    }

    public function getAllUser() {
        return Requent::resource($this->user->orderBy('id', 'desc'))->get();
    }


    public function getSalesman() 
    {
    	return Requent::resource($this->salesman)->get();
    }

    public function getFindSalesMan($id)
    {
    	return Requent::resource($this->salesman)->find($id);
    }

    public function postSalesmanCreateOrUpdate()
    {
    	$input = $data[0] = $this->request->all();

    	if(!isset($input['salesman_id']))
    	{
    		$this->salesman->create([
    			'user_id' => $input['user_id'],
    			'salesman_phone_number' => $input['salesman_phone_number']
    		]);

    		return response()->json([
    				'message' => 'successfully assign salesman.'
    			], 200);
    	}
    	else
    	{
    		$this->salesman->where('id', $input['salesman_id'])->update([
    				'user_id' => $input['user_id'],
    				'salesman_phone_number' => $input['salesman_phone_number']
    			]);

    		return response()->json([
    				'message' => 'successfully updated assigned salesman.'
    			] , 200);
    	}
    }

    public function removeSalesMan($user_id)
    {
        //die("remove");
        $salesman = $this->salesman->find($user_id);

        try{
            $delete = $salesman->delete();
            if($delete){
                return response()->json([
                    
                    'message' => 'Deleted Succesfully',
                
                ], 200);
            }
        } catch(\Exception $e) {
                return response()->json([
                    'message' => ' Already Use in Another Section'
                ], 400);
        }
    }
}
