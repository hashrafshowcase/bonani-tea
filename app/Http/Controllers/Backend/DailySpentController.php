<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Models\DailySpent;
use App\Models\AccountHistory;
use Carbon\Carbon;

class DailySpentController extends Controller
{
    protected $request;
    protected $dailySpent;
    protected $carbon;
    protected $account;

    public function __construct(Request $request, DailySpent $dailySpent, Carbon $carbon, AccountHistory $account)
    {
    	$this->request = $request;
    	$this->dailySpent = $dailySpent;
    	$this->carbon = $carbon;
    	$this->account = $account;
    }

    public function dailySpentHistory($type = false)
    {
        $status = $type ? $type : 'daily';
        $status = explode('advance-', $status);
        $status = count($status) > 1 ? $status : $status[0];
        if($status == 'daily') {

            $dailySpentHistoryDaily = $this->dailySpent->whereBetween('created_at', [$this->carbon::now()->startOfMonth(), $this->carbon::now()])->get();


            $dailySpentDailyWise = $dailySpentHistoryDaily->groupBy(function($date) {
                return $this->carbon::parse($date->created_at)->toDateString();
            });

            $dailySpentDailyWiseModified = [];
            foreach ($dailySpentDailyWise as $key => $value) {
                $data = [
                    'title' => $key,
                    'type' => 'daily',
                    'value' => $dailySpentDailyWise[$key]->sum('value')
                ];
                array_push($dailySpentDailyWiseModified, $data);
            }

            $data = [
                'type' => $status,
                'current_year' => $this->carbon->format('Y'),
                'current_month' => $this->carbon->format('F'),
                'current_date' => $this->carbon::now()->toFormattedDateString(),
                'data' => $dailySpentDailyWiseModified
            ];
            return response()->json($data, 200);
        }
        else if($status == 'monthly') {
            $dailySpentHistoryMonthly = $this->dailySpent->whereBetween('created_at', [$this->carbon::now()->startOfYear(), $this->carbon::now()])->orderBy('created_at', 'DESC')->get();

            $dailySpentMonthlyWise = $dailySpentHistoryMonthly->groupBy(function($date) {
                return $this->carbon::parse($date->created_at)->format('F');
            });


            $dailySpentMonthlyWiseModified = [];
            foreach ($dailySpentMonthlyWise as $key => $value) {
                $data = [
                    'title' => $key,
                    'type' => 'monthly',
                    'value' => $dailySpentMonthlyWise[$key]->sum('value')
                ];
                array_push($dailySpentMonthlyWiseModified, $data);
            }

            // return $dailySpentMonthlyWiseModified;
            // return $dailySpentHistoryMonthly;
            $data = [
                'type' => $status,
                'current_year' => $this->carbon->format('Y'),
                'current_month' => $this->carbon->format('F'),
                'current_date' => $this->carbon::now()->toFormattedDateString(),
                'data' => $dailySpentMonthlyWiseModified
            ];
            return response()->json($data, 200);
        }
        else if($status == 'yearly')
        {
            $dailySpentHistoryYearly = $this->dailySpent->whereBetween('created_at', [$this->carbon::create(2017, 01, 01, 00, 00, 00), $this->carbon::now()])->get();

            $dailySpentHistoryYearyWise = $dailySpentHistoryYearly->groupBy(function($date) {
                return $this->carbon::parse($date->created_at)->format('Y');
            });

            $dailySpentHistoryYearyWiseModified = [];
            foreach ($dailySpentHistoryYearyWise as $key => $value) {
               $data = [
                    'title' => $key,
                    'type' => 'yearly',
                    'value' => $dailySpentHistoryYearyWise[$key]->sum('value')
               ];
               array_push($dailySpentHistoryYearyWiseModified, $data);
            }

            $data = [
                'type' => $status,
                'current_year' => $this->carbon->format('Y'),
                'current_month' => $this->carbon->format('F'),
                'current_date' => $this->carbon::now()->toFormattedDateString(),
                'data' => $dailySpentHistoryYearyWiseModified
            ];
            return response()->json($data, 200);
        }
        else if(count($status) > 1) {

            $dailySpentHistoryAdvance = $this->dailySpent->whereBetween('created_at', [$this->carbon::createFromFormat('Y-m-d', $status[1])->startOfMonth(), $this->carbon::createFromFormat('Y-m-d', $status[1])->endOfMonth()])->get();


            $dailySpentAdvanceWise = $dailySpentHistoryAdvance->groupBy(function($date) {
                return $this->carbon::parse($date->created_at)->toDateString();
            });

            // return $dailySpentAdvanceWise;
            $dailySpentAdvanceWiseModified = [];
            foreach ($dailySpentAdvanceWise as $key => $value) {
                $data = [
                    'title' => $key,
                    'type' => 'advance',
                    'value' => $dailySpentAdvanceWise[$key]->sum('value')
                ];
                array_push($dailySpentAdvanceWiseModified, $data);
            }

            $data = [
                'type' => 'advance',
                'current_year' => $this->carbon->format('Y'),
                'current_month' => $this->carbon->format('F'),
                'current_date' => $this->carbon::now()->toFormattedDateString(),
                'data' => $dailySpentAdvanceWiseModified
            ];
            return response()->json($data, 200);
        }
    }


    public function dailySpentStore()
    {
    	$input = $data[0] = $this->request->all();

        foreach ($input as $key => $value) {
            $this->dailySpent->create([
                'reason' => $value['reason'],
                'value' => (int) $value['value']
            ]);

            $this->account->create([
            	'description' => $value['reason'],
	            'history_type' => 'cash',
	            'account_type' => 'debit',
	            'value' => (int) $value['value']
            ]);
        }

        $data = [
            'message' => 'Successfully added!'
        ];

        return response()->json($data, 200);
    }

    public function dailySpentSearch(){
        $input = $data[0] = $this->request->all();

        if(isset($input['id']))
        {
            $dailySpentById = $this->dailySpent->where('id', '=', $input['id'])->get();
            return response()->json([
                'data' => $dailySpentById
            ], 200);
        }
        else if(isset($input['daily']))
        {
            $startDay = $this->carbon::createFromFormat('Y-m-d', $input['daily'])->startOfDay();
            $endDay = $this->carbon::createFromFormat('Y-m-d', $input['daily'])->endOfDay();
            $dailySpentDaily = $this->dailySpent->whereBetween('created_at', [$startDay, $endDay])->get();

            return $dailySpentDaily;
        }
        else if(isset($input['monthly']))
        {
            $startDay = $this->carbon::createFromFormat('F', $input['monthly'])->startOfMonth();
            $endDay = $this->carbon::createFromFormat('F', $input['monthly'])->endOfMonth();
            $dailySpentMonthly = $this->dailySpent->whereBetween('created_at', [$startDay, $endDay])->get();

            $dailySpentDailyWise = $dailySpentMonthly->groupBy(function($date) {
                return $this->carbon::parse($date->created_at)->toDateString();
            });

            $dailySpentDailyWiseModified = [];
            foreach ($dailySpentDailyWise as $key => $value) {
                $data = [
                    'title' => $key,
                    'type' => 'daily',
                    'value' => $dailySpentDailyWise[$key]->sum('value')
                ];
                array_push($dailySpentDailyWiseModified, $data);
            }

            $data = [
                'type' => 'daily',
                'current_year' => $this->carbon->format('Y'),
                'current_month' => $this->carbon->format('F'),
                'current_date' => $this->carbon::now()->toFormattedDateString(),
                'data' => $dailySpentDailyWiseModified
            ];
            return response()->json($data, 200);
        }
        else if(isset($input['yearly']))
        {
            $startMonth= $this->carbon::createFromFormat('Y', $input['yearly'])->startOfYear();
            $endMonth = $this->carbon::createFromFormat('Y', $input['yearly'])->endOfYear();
            $dailySpentHistoryMonthly = $this->dailySpent->whereBetween('created_at', [$startMonth, $endMonth])->orderBy('created_at', 'DESC')->get();

            $dailySpentMonthlyWise = $dailySpentHistoryMonthly->groupBy(function($date) {
                return $this->carbon::parse($date->created_at)->format('F');
            });

            $dailySpentMonthlyWiseModified = [];
            foreach ($dailySpentMonthlyWise as $key => $value) {
                $data = [
                    'title' => $key,
                    'type' => 'monthly',
                    'value' => $dailySpentMonthlyWise[$key]->sum('value')
                ];
                array_push($dailySpentMonthlyWiseModified, $data);
            }

            // return $dailySpentMonthlyWiseModified;
            // return $dailySpentHistoryMonthly;
            $data = [
                'type' => 'monthly',
                'current_year' => $this->carbon->format('Y'),
                'current_month' => $this->carbon->format('F'),
                'current_date' => $this->carbon::now()->toFormattedDateString(),
                'data' => $dailySpentMonthlyWiseModified
            ];
            return response()->json($data, 200);
        }
    }
}
