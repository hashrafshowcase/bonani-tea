<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Models\UnloadAsset;
use App\Models\UnloadAssetProductList;
use App\Models\Brokers;
use App\Models\TeaGrade;
use App\Models\AccountHistory;
use App\Models\Garden;
use Carbon\Carbon;

class PurchaseController extends Controller
{
    protected $purchase;
    protected $request;
    protected $carbon;
    protected $brokers;
    protected $purchaseList;
    protected $tea;
    protected $account;
    protected $garden;

    public function __construct(UnloadAssetProductList $purchaseList, Request $request, UnloadAsset $unload_asset, Carbon $carbon, Brokers $brokers, TeaGrade $tea, AccountHistory $account, Garden $garden)
    {
    	$this->request = $request;
    	$this->purchase = $unload_asset;
    	$this->carbon = $carbon;
    	$this->brokers = $brokers;
        $this->tea = $tea;
        $this->purchaseList = $purchaseList;
        $this->account = $account;
        $this->garden = $garden;
    }

    public function getPurchaseList()
    {
        $getPurchaseList = $this->purchaseList->with([
                                                'unload_asset' => function($q) {
                                                    return $q->with([
                                                        'broker' 
                                                    ]);
                                                },
                                                'garden',
                                                'tea_grade'
                                            ])
                                            ->where('status', 'Y')->get();
        $uniqueBroker = $this->purchase->distinct('broker_id')->pluck('broker_id');
        $uniqueGarden = $this->purchaseList->distinct('garden_id')->pluck('garden_id');
        $uniqueTeaGrade = $this->purchaseList->distinct('tea_grade_id')->pluck('tea_grade_id');

        $brokerData = array();

        foreach($uniqueBroker as $broker) {
            $data = $this->brokers->select('id', 'broker_name')->find($broker);
            $data = array(
                'text' => $data->broker_name,
                'value' => $data->broker_name
            );
            array_push($brokerData, $data);
        }

        $gardenData = array();
        foreach($uniqueGarden as $garden) {
            $data = $this->garden->select('id', 'garden_name')->find($garden);
            $data = array(
                'text' => $data->garden_name,
                'value' => $data->garden_name
            );
            array_push($gardenData, $data);
        }

        $teaGradeData = array();
        foreach ($uniqueTeaGrade as  $tea_grade) {
            $data = $this->tea->select('grade_name')->find($tea_grade);
            $data = array(
                'text' => $data->grade_name,
                'value' => $data->grade_name
            );
            array_push($teaGradeData, $data);
        }

        return response()->json([
            'purchase_list' => $getPurchaseList,
            'brokers'   => $brokerData,
            'gardens'   => $gardenData,
            'tea_grades' => $teaGradeData
        ]);
    }

    public function previewPurchaseHistory($type = false)
    {
    	$status = $type ? $type : 'daily';
        $status = explode('advance-', $status);
        $status = count($status) > 1 ? $status : $status[0];
    	if($status == 'daily') {

    		$purchaseHistoryDaily = $this->purchase->whereBetween('submit_date', [$this->carbon::now()->startOfMonth()->toDateString(), $this->carbon::now()->toDateString()])->get();

    		$purchaseBrokerWise = $purchaseHistoryDaily->groupBy(function($broker){
    			return $this->brokers->find($broker->broker_id)->broker_name;
    		});

            $purchaseDailyWise = $purchaseHistoryDaily->groupBy(function($date) {
                return $this->carbon::parse($date->submit_date)->toDateString();
            });

            // return $purchaseDailyWise;
            $purchaseDailyWiseModified = [];
            foreach ($purchaseDailyWise as $key => $value) {
                $data = [
                    'title' => $key,
                    'type' => 'daily',
                    'total_amount' => $purchaseDailyWise[$key]->sum('total_amount'),
                    'total_kg' => $purchaseDailyWise[$key]->sum('total_kg')
                ];
                array_push($purchaseDailyWiseModified, $data);
            }
    		//return $purchaseHistoryDaily;
    		$purchaseBrokerWiseModified = [];
    		foreach ($purchaseBrokerWise as $key => $value) {
    			$data = [
    				'total_amount' => $purchaseBrokerWise[$key]->sum('total_amount'),
    				'total_packs' => $purchaseBrokerWise[$key]->sum('total_packs'),
    				'total_kg' => $purchaseBrokerWise[$key]->sum('total_kg'),
    				'purchase_total' => $purchaseBrokerWise[$key]
    			];
    			$purchaseBrokerWiseModified[$key] = $data;
    		}

    		//return $purchaseBrokerWiseModified;

    		$data = [
    			'type' => $status,
    			'current_year' => $this->carbon->format('Y'),
    			'current_month' => $this->carbon->format('F'),
    			'current_date' => $this->carbon::now()->toFormattedDateString(),
    			'total_amount' => $purchaseHistoryDaily->sum('total_amount'),
    			'total_packs' => $purchaseHistoryDaily->sum('total_packs'),
    			'total_kg' => $purchaseHistoryDaily->sum('total_kg'),
    			'brokers' => array($purchaseBrokerWiseModified),
                'data' => $purchaseDailyWiseModified
    		];
    		return response()->json($data, 200);
    	}
    	else if($status == 'monthly') {
    		$purchaseHistoryMonthly = $this->purchase->whereBetween('submit_date', array(Carbon::now()->startOfYear()->toDateString(), Carbon::now()->toDateString()))->orderBy('submit_date', 'DESC')->get();

    		$purchaseBrokerWise = $purchaseHistoryMonthly->groupBy(function($broker){
    			return $this->brokers->find($broker->broker_id)->broker_name;
    		});

            $purchaseMonthlyWise = $purchaseHistoryMonthly->groupBy(function($date) {
                return $this->carbon::parse($date->submit_date)->format('F');
            });

    		$purchaseBrokerWiseModified = [];
    		foreach ($purchaseBrokerWise as $key => $value) {
    			$data = [
    				'total_amount' => $purchaseBrokerWise[$key]->sum('total_amount'),
    				'total_packs' => $purchaseBrokerWise[$key]->sum('total_packs'),
    				'total_kg' => $purchaseBrokerWise[$key]->sum('total_kg'),
    				'purchase_total' => $purchaseBrokerWise[$key]
    			];
    			$purchaseBrokerWiseModified[$key] = $data;
    		}

            // $dateString = '2017-02-17';
            // return $dateObject = $this->carbon::createFromFormat('Y-m-d', $dateString);
            // return $dateObject;

            $purchaseMonthlyWiseModified = [];
            foreach ($purchaseMonthlyWise as $key => $value) {
                $data = [
                    'title' => $key,
                    'type' => 'monthly',
                    'total_amount' => $purchaseMonthlyWise[$key]->sum('total_amount'),
                    'total_kg' => $purchaseMonthlyWise[$key]->sum('total_kg')
                ];
                array_push($purchaseMonthlyWiseModified, $data);
            }

            // return $purchaseMonthlyWiseModified;
    		// return $purchaseHistoryMonthly;
    		$data = [
    			'type' => $status,
    			'current_year' => $this->carbon->format('Y'),
    			'current_month' => $this->carbon->format('F'),
    			'current_date' => $this->carbon::now()->toFormattedDateString(),
    			'total_amount' => $purchaseHistoryMonthly->sum('total_amount'),
    			'total_packs' => $purchaseHistoryMonthly->sum('total_packs'),
    			'total_kg' => $purchaseHistoryMonthly->sum('total_kg'),
    			'brokers' => array($purchaseBrokerWiseModified),
                'data' => $purchaseMonthlyWiseModified
    		];
    		return response()->json($data, 200);
    	}
    	else if($status == 'yearly')
    	{
    		$purchaseHistoryYearly = $this->purchase->whereBetween('submit_date', [$this->carbon::create(2017, 01, 01, 00, 00, 00)->toDateString(), $this->carbon::now()->toDateString()])->get();

    		$purchaseBrokerWise = $purchaseHistoryYearly->groupBy(function($broker){
    			return $this->brokers->find($broker->broker_id)->broker_name;
    		});

            $purchaseHistoryYearyWise = $purchaseHistoryYearly->groupBy(function($date) {
                return $this->carbon::parse($date->submit_date)->format('Y');
            });

            // return $purchaseHistoryYearyWise;
            $purchaseHistoryYearyWiseModified = [];
            foreach ($purchaseHistoryYearyWise as $key => $value) {
               $data = [
                    'title' => $key,
                    'type' => 'yearly',
                    'total_amount' => $purchaseHistoryYearyWise[$key]->sum('total_amount'),
                    'total_kg' => $purchaseHistoryYearyWise[$key]->sum('total_kg')
               ];
               array_push($purchaseHistoryYearyWiseModified, $data);
            }

    		$purchaseBrokerWiseModified = [];
    		foreach ($purchaseBrokerWise as $key => $value) {
    			$data = [
    				'total_amount' => $purchaseBrokerWise[$key]->sum('total_amount'),
    				'total_packs' => $purchaseBrokerWise[$key]->sum('total_packs'),
    				'total_kg' => $purchaseBrokerWise[$key]->sum('total_kg'),
    				'purchase_total' => $purchaseBrokerWise[$key],
    			];
    			$purchaseBrokerWiseModified[$key] = $data;
    		}
    		// return $purchaseHistoryYearly;
    		$data = [
    			'type' => $status,
    			'current_year' => $this->carbon->format('Y'),
    			'current_month' => $this->carbon->format('F'),
    			'current_date' => $this->carbon::now()->toFormattedDateString(),
    			'total_amount' => $purchaseHistoryYearly->sum('total_amount'),
    			'total_packs' => $purchaseHistoryYearly->sum('total_packs'),
    			'total_kg' => $purchaseHistoryYearly->sum('total_kg'),
    			'brokers' => array($purchaseBrokerWiseModified),
                'data' => $purchaseHistoryYearyWiseModified
    		];
    		return response()->json($data, 200);
    	}
        else if(count($status) > 1) {

            $purchaseHistoryAdvance = $this->purchase->whereBetween('submit_date', [$this->carbon::createFromFormat('Y-m-d', $status[1])->startOfMonth(), $this->carbon::createFromFormat('Y-m-d', $status[1])->endOfMonth()])->get();

            // return $purchaseHistoryAdvance;

            $purchaseBrokerWise = $purchaseHistoryAdvance->groupBy(function($broker){
                return $this->brokers->find($broker->broker_id)->broker_name;
            });

            $purchaseAdvanceWise = $purchaseHistoryAdvance->groupBy(function($date) {
                return $this->carbon::parse($date->submit_date)->toDateString();
            });

            // return $purchaseAdvanceWise;
            $purchaseAdvanceWiseModified = [];
            foreach ($purchaseAdvanceWise as $key => $value) {
                $data = [
                    'title' => $key,
                    'type' => 'advance',
                    'total_amount' => $purchaseAdvanceWise[$key]->sum('total_amount'),
                    'total_kg' => $purchaseAdvanceWise[$key]->sum('total_kg')
                ];
                array_push($purchaseAdvanceWiseModified, $data);
            }
            //return $purchaseHistoryAdvance;
            $purchaseBrokerWiseModified = [];
            foreach ($purchaseBrokerWise as $key => $value) {
                $data = [
                    'total_amount' => $purchaseBrokerWise[$key]->sum('total_amount'),
                    'total_packs' => $purchaseBrokerWise[$key]->sum('total_packs'),
                    'total_kg' => $purchaseBrokerWise[$key]->sum('total_kg'),
                    'purchase_total' => $purchaseBrokerWise[$key]
                ];
                $purchaseBrokerWiseModified[$key] = $data;
            }

            //return $purchaseBrokerWiseModified;

            $data = [
                'type' => 'advance',
                'current_year' => $this->carbon->format('Y'),
                'current_month' => $this->carbon->format('F'),
                'current_date' => $this->carbon::now()->toFormattedDateString(),
                'total_amount' => $purchaseHistoryAdvance->sum('total_amount'),
                'total_packs' => $purchaseHistoryAdvance->sum('total_packs'),
                'total_kg' => $purchaseHistoryAdvance->sum('total_kg'),
                'brokers' => array($purchaseBrokerWiseModified),
                'data' => $purchaseAdvanceWiseModified
            ];
            return response()->json($data, 200);
        }
    }

    public function previewPurchaseListById() {
    	return $this->purchase->with(['broker', 'unload_asset_product_list'])->find(1);
    }

    public function purchaseStore()
    {
        $input = $data[0] = $this->request->all();
        $purchase = $this->purchase->create([
            'broker_id' => (int) $input['broker'],
            'submit_date' => $input['date'],
            'auction_date' => $input['auction_date'],
            'prompt_date' => $input['prompt_date'],
            'total_packs' => (int) $input['total_packs'],
            'total_kg' => $input['total_kg'],
            'total_amount' => $input['total_amount']
        ]);


        $purchase_list = $input['unload_assets_list'];
        foreach ($purchase_list as $value) {
           $tea = $this->tea->findOrFail($value['tea_grade']);
           if($tea) {
            $tea->total_packs = $tea->total_packs + floatval($value['packs']);
            $tea->total_kg = $tea->total_kg + floatval($value['total_kg']);
            $tea->save();
           }
            $this->purchaseList->create([
                'unload_asset_id' => $purchase->id,
                'lot_no' => (int) $value['lot_no'],
                'garden_id' => $value['garden'],
                'invoice_no' => $value['invoice_no'],
                'tea_grade_id' => $value['tea_grade'],
                'packs' => (int) $value['packs'],
                'available_packs' => (int) $value['packs'],
                'net_each' => floatval($value['net_each']),
                'sample' => $value['sample'],
                'payment' => $value['payment'],
                'total_kg' => floatval($value['total_kg']),
                'per_price' => floatval($value['per_price']),
                'amount' => floatval($value['total_amount'])
            ]);
            
        }
        $data = [
            'message' => 'Successfully Created'
        ];

        return response()->json($data, 200);
    }

    public function purchaseSearch(){
        $input = $data[0] = $this->request->all();

        if(isset($input['id']))
        {
            $purchaseById = $this->purchase->with([
                                                    'unload_asset_product_list' => function($query) {
                                                        return $query->with([
                                                            'garden',
                                                            'tea_grade'
                                                        ]);
                                                    }
                                                ])->where('id', '=', $input['id'])->get();
            return response()->json([
                'data' => $purchaseById
            ], 200);
        }
        else if(isset($input['daily']))
        {
            $purchaseDaily = $this->purchase->with(['broker'])->where('submit_date', '=', $input['daily'])->get();

            return $purchaseDaily;
        }
        else if(isset($input['monthly']))
        {
            $startDay = $this->carbon::createFromFormat('F', $input['monthly'])->startOfMonth()->toDateString();
            $endDay = $this->carbon::createFromFormat('F', $input['monthly'])->endOfMonth()->toDateString();
            $purchaseMonthly = $this->purchase->with(['broker'])->whereBetween('submit_date', [$startDay, $endDay])->get();

            $purchaseDailyWise = $purchaseMonthly->groupBy(function($date) {
                return $this->carbon::parse($date->submit_date)->toDateString();
            });

            $purchaseDailyWiseModified = [];
            foreach ($purchaseDailyWise as $key => $value) {
                $data = [
                    'title' => $key,
                    'type' => 'daily',
                    'total_amount' => $purchaseDailyWise[$key]->sum('total_amount'),
                    'total_kg' => $purchaseDailyWise[$key]->sum('total_kg')
                ];
                array_push($purchaseDailyWiseModified, $data);
            }

            $data = [
                'type' => 'daily',
                'current_year' => $this->carbon->format('Y'),
                'current_month' => $this->carbon->format('F'),
                'current_date' => $this->carbon::now()->toFormattedDateString(),
                'total_amount' => $purchaseMonthly->sum('total_amount'),
                'total_packs' => $purchaseMonthly->sum('total_packs'),
                'total_kg' => $purchaseMonthly->sum('total_kg'),
                'data' => $purchaseDailyWiseModified
            ];
            return response()->json($data, 200);
        }
        else if(isset($input['yearly']))
        {
            $startMonth= $this->carbon::createFromFormat('Y', $input['yearly'])->startOfYear()->toDateString();
            $endMonth = $this->carbon::createFromFormat('Y', $input['yearly'])->endOfYear()->toDateString();
            $purchaseHistoryMonthly = $this->purchase->whereBetween('submit_date', [$startMonth, $endMonth])->orderBy('submit_date', 'DESC')->get();

            $purchaseMonthlyWise = $purchaseHistoryMonthly->groupBy(function($date) {
                return $this->carbon::parse($date->submit_date)->format('F');
            });

            $purchaseMonthlyWiseModified = [];
            foreach ($purchaseMonthlyWise as $key => $value) {
                $data = [
                    'title' => $key,
                    'type' => 'monthly',
                    'total_amount' => $purchaseMonthlyWise[$key]->sum('total_amount'),
                    'total_kg' => $purchaseMonthlyWise[$key]->sum('total_kg')
                ];
                array_push($purchaseMonthlyWiseModified, $data);
            }

            // return $purchaseMonthlyWiseModified;
            // return $purchaseHistoryMonthly;
            $data = [
                'type' => 'monthly',
                'current_year' => $this->carbon->format('Y'),
                'current_month' => $this->carbon->format('F'),
                'current_date' => $this->carbon::now()->toFormattedDateString(),
                'total_amount' => $purchaseHistoryMonthly->sum('total_amount'),
                'total_packs' => $purchaseHistoryMonthly->sum('total_packs'),
                'total_kg' => $purchaseHistoryMonthly->sum('total_kg'),
                'data' => $purchaseMonthlyWiseModified
            ];
            return response()->json($data, 200);
        }
    }
}
