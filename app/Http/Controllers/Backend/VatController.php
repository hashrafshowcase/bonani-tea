<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Models\Vat;

use Requent;

class VatController extends Controller
{
   	protected $vat;
	protected $request;

	public function __construct(Vat $vat, Request $request)
	{
		$this->vat = $vat;
		$this->request = $request;
	}

	public function getAllVat()
	{
		return Requent::resource($this->vat->orderBy('id', 'asc'))->get();
	}

	public function VatCreateOrUpdate() {

		$input = $data[0] = $this->request->all();

		if(!isset($input['vat_id'])) {

			$vat = $this->vat->create([
					'vat_type' => $input['vat_type'],
					'value' => $input['value']

				]);

			return response()->json([
					'message' => 'Successfully create vat list.',
					'vatList' => $this->vat->orderBy('id', 'asc')->get()
				], 200);

		} else {

			$vat = $this->vat->where('id', $input['vat_id'])->update([
					'vat_type' => $input['vat_type'],
					'value' => $input['value']
				]);

			return response()->json([
					'message' => 'Successfully Update.',
					'vatList' => $this->vat->orderBy('id', 'asc')->get()

				], 200);
		}
	}

	public function VatRemove($id) {

		$vat = $this->vat->find($id);
        try{
            $delete = $vat->delete();
            $totalVat = $this->vat->orderBy('id', 'asc')->get();
            if($delete){
                return response()->json([
                    'message' => 'Deleted Succesfully',
                    'vatList' => $totalVat
                ], 200);
            }
        } catch(\Exception $e) {
                return response()->json([
                    'message' => ' Already Use in Another Section'
                ], 400);
        }

	}
}
