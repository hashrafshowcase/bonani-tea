<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Models\Area;
use App\Models\LocalArea;

use Requent;
use PDF;
use Carbon\Carbon;

class AreaController extends Controller {

	protected $area;
	protected $request;
	protected $localarea;
  protected $carbon;


	public function __construct(Area $area, LocalArea $localArea, Request $request, Carbon $carbon) {

		$this->area = $area;
		$this->localarea = $localArea;
		$this->request = $request;
    $this->carbon = $carbon;

    
		$this->middleware('roles');
	}


	public function area() {

		// $area = $this->area->with('localareas')->get();

		// if($this->request->wantsJson()) {
		// 	return response()->json([
		// 		'area' => $area
		// 	],200);
		// }
		
		return Requent::resource($this->area->orderBy('id', 'desc'))->get();
	}


	public function findArea($id) {

		return Requent::resource($this->area)->find($id);

	}


	public function createArea() {
		$input = $data[0] = $this->request->all();
		$slug = str_slug($input['area_name']);

		$this->area->create([
			'area_name' => $input['area_name'],
			'area_slug' => $slug
		]);

		return response()->json([
				'message' => 'Successfully add area.',
			], 200);

	}


	public function updateArea($id) {

		$input = $data[0] = $this->request->all();

		$slug = str_slug($input['area_name']);

		$this->area->where('id', $id)
					->update([
							'area_name' => $input['area_name'],
							'area_slug' => $slug
						]);
		return response()->json([
				'message' => 'update area successfully'
			], 200);
	}


	public function deleteArea($id) {

   		$area = $this->area->find($id);
        try{
            $delete = $area->delete();
            $totalArea = $this->area->with('localareas')->get();
            if($delete){
                return response()->json([
                    'message' => ' Deleted Succesfully',
                    'area' => $totalArea
                ], 200);
            }
        } catch(\Exception $e) {
                return response()->json([
                    'message' => ' Already Use in local area'
                ], 400);
        }
   		
   	}

   	public function localArea() {

   		// $localArea = $this->localarea->with('area')->get();

   		// if($this->request->wantsJson()) {

   		// 	return response()->json([
   		// 			'localarea' => $localArea
   		// 		], 200);

   		// }

   		// return $localArea;
   		
   		return Requent::resource($this->localarea->orderBy('id', 'desc'))->get();

   	}

   	public function CreateLocalArea() {

   		$input = $data[0] = $this->request->all();
   		$slug = str_slug($input['local_area_name']);


   		$this->localarea->create([
   				'area_id' => $input['area_id'],
   				'local_area_name' => $input['local_area_name'],
   				'local_area_name_slug' => $slug
   			]);

   		return response()->json([
				'message' => 'Successfully add local area.'
			], 200);

   	}


   	public function findLoalArea($id) {

      return Requent::resource($this->localarea)->find($id);

   	}


   	/**
   	 * update Local Area Controller
   	 * @param  [id]
   	 * @return [response]
   	 */
   	public function updateLocalArea($id) {

		$input = $data[0] = $this->request->all();

		$slug = str_slug($input['local_area_name']);

   		$this->localarea->where('id', $id)->update([
   				'area_id' => $input['area_id'],
   				'local_area_name' => $input['local_area_name'],
   				'local_area_name_slug' => $slug
   			]);
		return response()->json([
				'message' => 'update local area successfully'
			], 200);
	}


	/**
	 * Delete Local Area Controller
	 * @param  [id]
	 * @return [response]
	 */
	public function deleteLocalArea($id) {

		$localArea = $this->localarea->find($id);
    $delete = $localArea->delete();
    return response()->json([
                    'message' => ' Deleted Succesfully',
                ], 200);
	}

  public function export_all_area()
  {
    $area = $this->area->with(['localareas'])->orderBy('id', 'DESC')->get();

    $data = [
      'date' => $this->carbon->toDateString(),
      'data' => $area
    ];


    $pdfName = 'Area-bonani-tea-and-co-'.$this->carbon->toDateString().'-'.$this->carbon->toTimeString().'.pdf';

    $pdf = PDF::loadView('pdf.all-area', compact('data'));
    return $pdf->download($pdfName);
  }


}