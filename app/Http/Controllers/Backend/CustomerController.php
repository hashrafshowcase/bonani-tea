<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Models\Customer;
use Requent;
use Excel;
use PDF;
use Carbon\Carbon;

class CustomerController extends Controller {

	protected $customer;
	protected $request;
	protected $carbon;

	public function __construct(Request $request, Customer $customer, Carbon $carbon) {

		// $this->middleware('roles');

		$this->customer = $customer;
		$this->request = $request;
		$this->carbon = $carbon;

	}

	/**
	 * [CoustomerIndex description]
	 * It will be give all Customer Data
	 */
	public function CoustomerIndex() {

		return Requent::resource($this->customer)->get();
	}



	/**
	 * [FindCustomer description]
	 * @param [type] $id [description]
	 */
	public function FindCustomer($id) {

		return Requent::resource($this->customer)->find($id);
	}



	/**
	 * [CustomerCreateOrUpdate description]
	 * Customer Create And Update Here
	 */
	public function CustomerCreateOrUpdate() {

		$input = $data[0] = $this->request->all();


		//return $input['note'];
		if(!isset($input['customer_id'])) {

			$this->customer->fill($input)->save();

			return response()->json([
					'message' => 'Successfully Create Customer.'
				],200);
		} else {

			$this->customer->where('id', $input['customer_id'])->update([
					'customer_name' => $input['customer_name'],
					'customer_shop_name' => isset($input['customer_shop_name']) ? $input['customer_shop_name'] : null,
					'customer_phone_number' => isset($input['customer_phone_number']) ? $input['customer_phone_number'] : null,
					'customer_details'		=> $input['customer_details'],
					'area_id' => $input['area_id'],
					'local_area_id' => $input['local_area_id']
				]);

			return response()->json([
					'message' => 'Successfully Update Customer Details.'
				], 200); 
		}

	}


	public function CustomerRemove($id) {

		$customer = $this->customer->find($id);
        try{
            $delete = $customer->delete();
            $totalCustomer = $this->customer->with(['area','localarea'])->orderBy('id', 'desc')->get();
            if($delete){
                return response()->json([
                    'message' => ' Deleted Succesfully',
                    'area' => $totalCustomer
                ], 200);
            }
        } catch(\Exception $e) {
                return response()->json([
                    'message' => ' Already Use in Another Section'
                ], 400);
        }

	}

	public function export_customer() {
		$customers  = $this->customer->with(['area','sales_report_customer' => function($query) {
			$query->whereBetween('salesreport_created_at',['2017-08-12', '2017-08-12']);
		}])->get();
		// $items = [];
		$i = 0;
		foreach ($customers as $key => $item) {
			$item['total_amount'] = $item['sales_report_customer']->sum('total_amount');
			$i++;
		}

		$pdfName = 'Customer-bonani-tea-and-co-'.$this->carbon->toDateString().'-'.$this->carbon->toTimeString().'.pdf';

		$pdf = PDF::loadView('pdf.customer', compact('customers'));
		return $pdf->download($pdfName);


		// Excel::create('customers', function($excel) use($items) {
  //         	$excel->sheet('ExportFile', function($sheet) use($items) {
  //             $sheet->fromArray($items);
  //        	});
  //     	})->export('xls');


	}

}
