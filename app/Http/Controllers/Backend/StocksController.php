<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Requent;

use App\Models\StockCategory;
use App\Models\Stocks;
use App\Models\StockIndex;
use Carbon\Carbon;

class StocksController extends Controller
{
    protected $stockCategory;
    protected $stocks;
    protected $stockIndex;
    protected $request;
    protected $carbon;

    /**
     * [__construct description]
     * @param StockCategory $stockCategory [description]
     * @param Stocks        $stocks        [description]
     * @param StockIndex    $stockIndex    [description]
     * @param Request       $request       [description]
     */
    public function __construct(StockCategory $stockCategory, Stocks $stocks, StockIndex $stockIndex ,Request $request, Carbon $carbon)
    {
    	$this->stockCategory = $stockCategory;
        $this->stocks = $stocks;
        $this->stockIndex = $stockIndex;
    	$this->request = $request;
      $this->carbon = $carbon;
    }


    /**
     * [allCategories description]
     * @return [type] [description]
     */
    public function allCategories()
    {
    	return Requent::resource($this->stockCategory->orderBy('id', 'desc'))->get();
    }

    /**
     * [allStockIndex description]
     * @return [type] [description]
     */
    public function allStockIndex()
    {
        return Requent::resource($this->stockIndex->orderBy('id', 'desc'))->get();
    }

    /**
     * [stockIndexData description]
     * @param  [type] $stock_index_id [description]
     * @return [type]                 [description]
     */
    public function stockIndexData($stock_index_id)
    {
        return Requent::resource($this->stocks->where('stock_indices_id', $stock_index_id)->orderBy('curent_date', 'asc'))->get();
    }


    /**
     * [findCatById description]
     * @param  [type] $cat_id [description]
     * @return [type]         [description]
     */
    public function findCatById($cat_id)
    {
    	return Requent::resource($this->stockCategory)->find($cat_id);
    }


    /**
     * [createOrUpdateCategory description]
     * @return [type] [description]
     */
    public function createOrUpdateCategory()
    {
    	$input = $data[0] = $this->request->all();

    	if(!isset($input['id']))
    	{
    		$this->stockCategory->fill($this->request->all())->save();
            return response()->json([
                    'message' => 'successfully creted'
                ], 200);
    	}
    	else
    	{
    		$this->stockCategory->where('id', $input['id'])->update([
    				'stock_name' => $input['stock_name'],
    			]);

    		return response()->json([
    				'message' => 'successfully updated Stock Category.'
    			] , 200);
    	}
    }


    /**
     * [removeCategory description]
     * @param  [type] $cat_id [description]
     * @return [type]         [description]
     */
    public function removeCategory($id)
    {
    	$stocks = $this->stockCategory->find($id);
	    try{
          $delete = $stocks->delete();
          if($delete){
              return response()->json([
                  'message' => ' Deleted Succesfully'
              ], 200);
          }
	    } catch(\Exception $e) {
	        return response()->json([
	        	'message' => ' Already Use in another Properties'
	            ], 400);
	    }
    }

    public function addStocksData() 
    {
        $input = $data[0] = $this->request->all();
        if(isset($input['stock_indices_id']) && !isset($input['stocks_id']))
        {
            $this->stocks->create([
                'stock_indices_id' => $input['stock_indices_id'],
                'curent_date' => $input['curent_date'],
                'stocks_past_deposit' => $input['stocks_past_deposit'],
                'stocks_new_deposit' => $input['stocks_new_deposit'],
                'stocks_new_deposit_cash_memo' => $input['stocks_new_deposit_cash_memo'],
                'stocks_sum_deposit' => $input['stocks_sum_deposit'],
                'stocks_quantity_sold' => $input['stocks_quantity_sold'],
                'stocks_quantity_sold_cash_memo' => $input['stocks_quantity_sold_cash_memo'],
                'stocks_balance' => $input['stocks_balance']
            ]);
            return response()->json([
                    'message' => 'successfully created.'
                ], 200);
        }
        else
        {
            $this->stocks->where('id', $input['stocks_id'])->update([
                'stock_indices_id' => $input['stock_indices_id'],
                'curent_date' => $input['curent_date'],
                'stocks_past_deposit' => $input['stocks_past_deposit'],
                'stocks_new_deposit' => $input['stocks_new_deposit'],
                'stocks_new_deposit_cash_memo' => $input['stocks_new_deposit_cash_memo'],
                'stocks_sum_deposit' => $input['stocks_sum_deposit'],
                'stocks_quantity_sold' => $input['stocks_quantity_sold'],
                'stocks_quantity_sold_cash_memo' => $input['stocks_quantity_sold_cash_memo'],
                'stocks_balance' => $input['stocks_balance']
            ]);
            return response()->json([
                    'message' => 'successfully updated'
                ], 200);
        }
    }

    public function createOrUpdateCategoryIndex()
    {
        $input = $data[0] = $this->request->all();

        if(!isset($input['id']))
        {
            $this->stockIndex->fill($this->request->all())->save();
            return response()->json([
                    'message' => 'successfully creted'
                ], 200);
        }
        else
        {
            $this->stockIndex->where('id', $input['id'])->update([
                    'stock_category_id' => $input['stock_category_id'],
                    'teaquantities_id' => $input['teaquantities_id'],
                    'stock_indices_name' => $input['stock_indices_name'],
                    'stock_indices_month' => $input['stock_indices_month'],
                ]);

            return response()->json([
                    'message' => 'successfully updated Stock Index.'
                ] , 200);
        }
    }

    public function removeIndex($id)
    {
        $stocks = $this->stockIndex->find($id);
        try{
          $delete = $stocks->delete();
          if($delete){
              return response()->json([
                  'message' => ' Deleted Succesfully'
              ], 200);
          }
        } catch(\Exception $e) {
            return response()->json([
                'message' => ' Already Use in another Properties'
                ], 400);
        }
    }

    public function getDailyAllData()
    {
      $getDailyData = $this->stockCategory
                          ->with([
                            'stock_indices' => function($index) {
                              return $index->with([
                                'stocks' => function($stock) {
                                  return $stock->where('curent_date', $this->carbon::now()->toDateString());
                                }
                              ]);
                            }
                          ])
                          ->get();

      $data = [
        'current_date' => $this->carbon::now()->toDateString(),
        'daily_data'   => $getDailyData
      ];
      return response()->json($data, 200);
    }

    public function getAdvanceSearch()
    {
      $input = $data[0] = $this->request->all();
      $getAdvanceData = $this->stocks->where('stock_indices_id', $input['index_id'])
                             ->whereBetween('curent_date', [$input['start'], $input['end']])
                             ->get();
      return response()->json([
        'stocks' => $getAdvanceData
      ], 200);
    }
}
