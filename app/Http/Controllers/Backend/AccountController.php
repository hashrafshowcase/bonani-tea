<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Models\AssetsType;
use App\Models\AccountRecord;

use Carbon\Carbon;

class AccountController extends Controller
{
    protected $request;
    protected $assetsType;
    protected $accountRecord;
    protected $carbon;

    public function __construct(
    	Request $request,
    	AssetsType $assetsType,
        AccountRecord $accountRecord,
        Carbon $carbon
    )
    {
    	$this->request = $request;
    	$this->assetsType = $assetsType;
        $this->accountRecord = $accountRecord;
        $this->carbon = $carbon;
    }

    public function allAssetsType()
    {
    	$data = [
    		'assets_type' => $this->assetsType->with('assetsPrimary.assetsType')->get()
    	];
    	return response()->json($data, 200);
    }

    public function showAccountRecord($type = false)
    {
        $status = $type ? $type : 'daily';
        $status = explode('advance-', $status);
        $status = count($status) > 1 ? $status : $status[0];
        if($status == 'daily') {

            $accountRecordDaily = $this->accountRecord->whereBetween('input_date', [$this->carbon::now()->startOfMonth()->toDateString(), $this->carbon::now()->toDateString()])->get();


            $accountRecordDailyWise = $accountRecordDaily->groupBy(function($date) {
                return $this->carbon::parse($date->created_at)->toDateString();
            });

            $accountRecordDailyWiseModified = [];
            foreach ($accountRecordDailyWise as $key => $value) {
                $data = [
                    'title' => $key,
                    'type' => 'daily',
                ];
                array_push($accountRecordDailyWiseModified, $data);
            }

            $data = [
                'type' => $status,
                'current_year' => $this->carbon->format('Y'),
                'current_month' => $this->carbon->format('F'),
                'current_date' => $this->carbon::now()->toFormattedDateString(),
                'data' => $accountRecordDailyWiseModified
            ];
            return response()->json($data, 200);
        }
        else if($status == 'monthly') {
            $accountRecordMonthly = $this->accountRecord->whereBetween('input_date', [$this->carbon::now()->startOfYear()->toDateString(), $this->carbon::now()->toDateString()])->orderBy('created_at', 'DESC')->get();

            $accountRecordMonthlyWise = $accountRecordMonthly->groupBy(function($date) {
                return $this->carbon::parse($date->created_at)->format('F');
            });


            $accountRecordMonthlyWiseModified = [];
            foreach ($accountRecordMonthlyWise as $key => $value) {
                $data = [
                    'title' => $key,
                    'type' => 'monthly'
                ];
                array_push($accountRecordMonthlyWiseModified, $data);
            }

            // return $dailySpentMonthlyWiseModified;
            // return $dailySpentHistoryMonthly;
            $data = [
                'type' => $status,
                'current_year' => $this->carbon->format('Y'),
                'current_month' => $this->carbon->format('F'),
                'current_date' => $this->carbon::now()->toFormattedDateString(),
                'data' => $accountRecordMonthlyWiseModified
            ];
            return response()->json($data, 200);
        }
        else if($status == 'yearly')
        {
            $accountRecordYearly = $this->accountRecord->whereBetween('input_date', [$this->carbon::create(2017, 01, 01, 00, 00, 00)->toDateString(), $this->carbon::now()->toDateString()])->get();

            $accountRecordYearlyWise = $accountRecordYearly->groupBy(function($date) {
                return $this->carbon::parse($date->created_at)->format('Y');
            });

            $accountRecordYearlyWiseModified = [];
            foreach ($accountRecordYearlyWise as $key => $value) {
               $data = [
                    'title' => $key,
                    'type' => 'yearly'
               ];
               array_push($accountRecordYearlyWiseModified, $data);
            }

            $data = [
                'type' => $status,
                'current_year' => $this->carbon->format('Y'),
                'current_month' => $this->carbon->format('F'),
                'current_date' => $this->carbon::now()->toFormattedDateString(),
                'data' => $accountRecordYearlyWiseModified
            ];
            return response()->json($data, 200);
        }
        // else if($status == 'yearly
    }

    public function showAccountRecordByRef($ref,$type = false)
    {
        $status = $type ? $type : 'daily';
        $status = explode('advance-', $status);
        $status = count($status) > 1 ? $status : $status[0];
        if($status == 'daily') {

            $accountRecordDaily = $this->accountRecord->where('debit_assets_type', $ref)->orWhere('credit_assets_type', $ref)->whereBetween('input_date', [$this->carbon::now()->startOfMonth()->toDateString(), $this->carbon::now()->toDateString()])->get();


            $accountRecordDailyWise = $accountRecordDaily->groupBy(function($date) {
                return $this->carbon::parse($date->created_at)->toDateString();
            });

            $accountRecordDailyWiseModified = [];
            foreach ($accountRecordDailyWise as $key => $value) {
                $data = [
                    'title' => $key,
                    'type' => 'daily',
                ];
                array_push($accountRecordDailyWiseModified, $data);
            }

            $data = [
                'type' => $status,
                'current_year' => $this->carbon->format('Y'),
                'current_month' => $this->carbon->format('F'),
                'current_date' => $this->carbon::now()->toFormattedDateString(),
                'data' => $accountRecordDailyWiseModified
            ];
            return response()->json($data, 200);
        }
        else if($status == 'monthly') {
            $accountRecordMonthly = $this->accountRecord->where('debit_assets_type', $ref)->orWhere('credit_assets_type', $ref)->whereBetween('input_date', [$this->carbon::now()->startOfYear()->toDateString(), $this->carbon::now()->toDateString()])->orderBy('created_at', 'DESC')->get();

            $accountRecordMonthlyWise = $accountRecordMonthly->groupBy(function($date) {
                return $this->carbon::parse($date->created_at)->format('F');
            });


            $accountRecordMonthlyWiseModified = [];
            foreach ($accountRecordMonthlyWise as $key => $value) {
                $data = [
                    'title' => $key,
                    'type' => 'monthly'
                ];
                array_push($accountRecordMonthlyWiseModified, $data);
            }

            // return $dailySpentMonthlyWiseModified;
            // return $dailySpentHistoryMonthly;
            $data = [
                'type' => $status,
                'current_year' => $this->carbon->format('Y'),
                'current_month' => $this->carbon->format('F'),
                'current_date' => $this->carbon::now()->toFormattedDateString(),
                'data' => $accountRecordMonthlyWiseModified
            ];
            return response()->json($data, 200);
        }
        else if($status == 'yearly')
        {
            $accountRecordYearly = $this->accountRecord->where('debit_assets_type', $ref)->orWhere('credit_assets_type', $ref)->whereBetween('input_date', [$this->carbon::create(2017, 01, 01, 00, 00, 00)->toDateString(), $this->carbon::now()->toDateString()])->get();

            $accountRecordYearlyWise = $accountRecordYearly->groupBy(function($date) {
                return $this->carbon::parse($date->created_at)->format('Y');
            });

            $accountRecordYearlyWiseModified = [];
            foreach ($accountRecordYearlyWise as $key => $value) {
               $data = [
                    'title' => $key,
                    'type' => 'yearly'
               ];
               array_push($accountRecordYearlyWiseModified, $data);
            }

            $data = [
                'type' => $status,
                'current_year' => $this->carbon->format('Y'),
                'current_month' => $this->carbon->format('F'),
                'current_date' => $this->carbon::now()->toFormattedDateString(),
                'data' => $accountRecordYearlyWiseModified
            ];
            return response()->json($data, 200);
        }
        // else if($status == 'yearly
    }

    public function searchAcccountRecordByRef() {
        $input = $data[0] = $this->request->all();

        if(isset($input['daily'])) {
            $dailyAccountRecord = $this->accountRecord->with('assetsTypeDebit','assetsTypeCredit')
                                       ->where('debit_assets_type', $input['ref'])
                                       ->orWhere('credit_assets_type', $input['ref'])
                                       ->where('input_date', $input['daily'])
                                       ->get();
            return $dailyAccountRecord;
        }
        else if(isset($input['monthly'])) {
            $startDay = $this->carbon::createFromFormat('F', $input['monthly'])->startOfMonth()->toDateString();
            $endDay = $this->carbon::createFromFormat('F', $input['monthly'])->endOfMonth()->toDateString();
            $dailyAccountRecord = $this->accountRecord
                                        ->where('debit_assets_type', $input['ref'])
                                        ->orWhere('credit_assets_type', $input['ref'])
                                        ->whereBetween('input_date', [$startDay, $endDay])
                                        ->get();
            $dailyAccountRecordWise = $dailyAccountRecord->groupBy(function($date) {
                return $this->carbon::parse($date->created_at)->toDateString();
            });

            $dailyAccountRecordWiseModified = [];
            foreach ($dailyAccountRecordWise as $key => $value) {
                $data = [
                    'title' => $key,
                    'type' => 'daily'
                ];
                array_push($dailyAccountRecordWiseModified, $data);
            }

            $data = [
                'type' => 'daily',
                'current_year' => $this->carbon->format('Y'),
                'current_month' => $this->carbon->format('F'),
                'current_date' => $this->carbon::now()->toFormattedDateString(),
                'data' => $dailyAccountRecordWiseModified
            ];
            return response()->json($data, 200);
        }
        else if(isset($input['yearly']))
        {
            $startMonth= $this->carbon::createFromFormat('Y', $input['yearly'])->startOfYear()->toDateString();
            $endMonth = $this->carbon::createFromFormat('Y', $input['yearly'])->endOfYear()->toDateString();
            $accountRecordYearly = $this->accountRecord
                                        ->where('debit_assets_type', $input['ref'])
                                        ->orWhere('credit_assets_type', $input['ref'])
                                        ->whereBetween('input_date', [$startMonth, $endMonth])
                                        ->orderBy('input_date', 'DESC')
                                        ->get();

            $accountRecordYearlyWise = $accountRecordYearly->groupBy(function($date) {
                return $this->carbon::parse($date->created_at)->format('F');
            });


            $accountRecordYearlyWiseModified = [];
            foreach ($accountRecordYearlyWise as $key => $value) {
                $data = [
                    'title' => $key,
                    'type' => 'monthly'
                ];
                array_push($accountRecordYearlyWiseModified, $data);
            }

            // return $dailySpentMonthlyWiseModified;
            // return $dailySpentHistoryMonthly;
            $data = [
                'type' => 'monthly',
                'current_year' => $this->carbon->format('Y'),
                'current_month' => $this->carbon->format('F'),
                'current_date' => $this->carbon::now()->toFormattedDateString(),
                'data' => $accountRecordYearlyWiseModified
            ];
            return response()->json($data, 200);
        }
    }

    public function searchAccountRecord() {
        $input = $data[0] = $this->request->all();

        if(isset($input['daily'])) {
            $dailyAccountRecord = $this->accountRecord->with('assetsTypeDebit','assetsTypeCredit')->where('input_date', $input['daily'])->get();
            return $dailyAccountRecord;
        } 
        else if(isset($input['monthly'])) {
            $startDay = $this->carbon::createFromFormat('F', $input['monthly'])->startOfMonth()->toDateString();
            $endDay = $this->carbon::createFromFormat('F', $input['monthly'])->endOfMonth()->toDateString();
            $dailyAccountRecord = $this->accountRecord->whereBetween('input_date', [$startDay, $endDay])->get();
            $dailyAccountRecordWise = $dailyAccountRecord->groupBy(function($date) {
                return $this->carbon::parse($date->created_at)->toDateString();
            });

            $dailyAccountRecordWiseModified = [];
            foreach ($dailyAccountRecordWise as $key => $value) {
                $data = [
                    'title' => $key,
                    'type' => 'daily'
                ];
                array_push($dailyAccountRecordWiseModified, $data);
            }

            $data = [
                'type' => 'daily',
                'current_year' => $this->carbon->format('Y'),
                'current_month' => $this->carbon->format('F'),
                'current_date' => $this->carbon::now()->toFormattedDateString(),
                'data' => $dailyAccountRecordWiseModified
            ];
            return response()->json($data, 200);
        }
        else if(isset($input['yearly']))
        {
            $startMonth= $this->carbon::createFromFormat('Y', $input['yearly'])->startOfYear()->toDateString();
            $endMonth = $this->carbon::createFromFormat('Y', $input['yearly'])->endOfYear()->toDateString();
            $accountRecordYearly = $this->accountRecord->whereBetween('input_date', [$startMonth, $endMonth])->orderBy('input_date', 'DESC')->get();

            $accountRecordYearlyWise = $accountRecordYearly->groupBy(function($date) {
                return $this->carbon::parse($date->created_at)->format('F');
            });


            $accountRecordYearlyWiseModified = [];
            foreach ($accountRecordYearlyWise as $key => $value) {
                $data = [
                    'title' => $key,
                    'type' => 'monthly'
                ];
                array_push($accountRecordYearlyWiseModified, $data);
            }

            // return $dailySpentMonthlyWiseModified;
            // return $dailySpentHistoryMonthly;
            $data = [
                'type' => 'monthly',
                'current_year' => $this->carbon->format('Y'),
                'current_month' => $this->carbon->format('F'),
                'current_date' => $this->carbon::now()->toFormattedDateString(),
                'data' => $accountRecordYearlyWiseModified
            ];
            return response()->json($data, 200);
        }
    }

    public function storeAccountRecord()
    {
        $input = $data[0] = $this->request->get('record');

        foreach ($input['data'] as $key => $value) {
            $this->accountRecord->create([
                'input_date' => $input['record_date'],
                'debit_assets_type' => $value['debit_id'],
                'credit_assets_type' => $value['credit_id'],
                'amount' => $value['amount'],
                'notes' => ($value['notes']) ?  $value['notes'] : NULL
            ]);
        }

        return response()->json([
            'message' => 'Succesfully added!'
        ], 200);

    }
}
