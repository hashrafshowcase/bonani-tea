<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Models\Blend;
use App\Models\BlendList;
use App\Models\UnloadAssetProductList;
use Carbon\Carbon;
use PDF;

class BlendController extends Controller
{
    protected $request;
    protected $blend;
    protected $blendList;
    protected $carbon;
    protected $unloadAsset;

    public function __construct(
        Request $request, 
        Blend $blend, 
        BlendList $blendList, 
        Carbon $carbon,
        UnloadAssetProductList $unloadAsset
    )
    {
    	$this->request = $request;
    	$this->blend = $blend;
    	$this->blendList = $blendList;
        $this->carbon = $carbon;
        $this->unloadAsset = $unloadAsset;
    }

   public function blendHistory($type = false)
    {
        $status = $type ? $type : 'daily';
        $status = explode('advance-', $status);
        $status = count($status) > 1 ? $status : $status[0];
        if($status == 'daily') {

            $blendHistoryDaily = $this->blend->whereBetween('date', [$this->carbon::now()->startOfMonth(), $this->carbon::now()])->get();


            $blendDailyWise = $blendHistoryDaily->groupBy(function($date) {
                return $this->carbon::parse($date->date)->toDateString();
            });

            $blendDailyWiseModified = [];
            foreach ($blendDailyWise as $key => $value) {
                $data = [
                    'title' => $key,
                    'type' => 'daily',
                    'value' => $blendDailyWise[$key]->sum('total_packs')
                ];
                array_push($blendDailyWiseModified, $data);
            }

            $data = [
                'type' => $status,
                'current_year' => $this->carbon->format('Y'),
                'current_month' => $this->carbon->format('F'),
                'current_date' => $this->carbon::now()->toFormattedDateString(),
                'data' => $blendDailyWiseModified
            ];
            return response()->json($data, 200);
        }
        else if($status == 'monthly') {
            $blendHistoryMonthly = $this->blend->whereBetween('date', [$this->carbon::now()->startOfYear(), $this->carbon::now()])->orderBy('date', 'DESC')->get();

            $blendMonthlyWise = $blendHistoryMonthly->groupBy(function($date) {
                return $this->carbon::parse($date->date)->format('F');
            });


            $blendMonthlyWiseModified = [];
            foreach ($blendMonthlyWise as $key => $value) {
                $data = [
                    'title' => $key,
                    'type' => 'monthly',
                    'value' => $blendMonthlyWise[$key]->sum('total_packs')
                ];
                array_push($blendMonthlyWiseModified, $data);
            }

            // return $blendMonthlyWiseModified;
            // return $blendHistoryMonthly;
            $data = [
                'type' => $status,
                'current_year' => $this->carbon->format('Y'),
                'current_month' => $this->carbon->format('F'),
                'current_date' => $this->carbon::now()->toFormattedDateString(),
                'data' => $blendMonthlyWiseModified
            ];
            return response()->json($data, 200);
        }
        else if($status == 'yearly')
        {
            $blendHistoryYearly = $this->blend->whereBetween('date', [$this->carbon::create(2017, 01, 01, 00, 00, 00), $this->carbon::now()])->get();

            $blendHistoryYearyWise = $blendHistoryYearly->groupBy(function($date) {
                return $this->carbon::parse($date->date)->format('Y');
            });

            $blendHistoryYearyWiseModified = [];
            foreach ($blendHistoryYearyWise as $key => $value) {
               $data = [
                    'title' => $key,
                    'type' => 'yearly',
                    'value' => $blendHistoryYearyWise[$key]->sum('total_packs')
               ];
               array_push($blendHistoryYearyWiseModified, $data);
            }

            $data = [
                'type' => $status,
                'current_year' => $this->carbon->format('Y'),
                'current_month' => $this->carbon->format('F'),
                'current_date' => $this->carbon::now()->toFormattedDateString(),
                'data' => $blendHistoryYearyWiseModified
            ];
            return response()->json($data, 200);
        }
        else if(count($status) > 1) {

            $blendHistoryAdvance = $this->blend->whereBetween('date', [$this->carbon::createFromFormat('Y-m-d', $status[1])->startOfMonth(), $this->carbon::createFromFormat('Y-m-d', $status[1])->endOfMonth()])->get();


            $blendAdvanceWise = $blendHistoryAdvance->groupBy(function($date) {
                return $this->carbon::parse($date->date)->toDateString();
            });

            // return $blendAdvanceWise;
            $blendAdvanceWiseModified = [];
            foreach ($blendAdvanceWise as $key => $value) {
                $data = [
                    'title' => $key,
                    'type' => 'advance',
                    'value' => $blendAdvanceWise[$key]->sum('total_packs')
                ];
                array_push($blendAdvanceWiseModified, $data);
            }

            $data = [
                'type' => 'advance',
                'current_year' => $this->carbon->format('Y'),
                'current_month' => $this->carbon->format('F'),
                'current_date' => $this->carbon::now()->toFormattedDateString(),
                'data' => $blendAdvanceWiseModified
            ];
            return response()->json($data, 200);
        }
    }

    public function blendSearch(){
        $input = $data[0] = $this->request->all();

        if(isset($input['id']))
        {
            $blendById = $this->blend->with([
                                        'blend_lists' => function($r) {
                                            return $r->with([
                                            	'tea_grade',
                                                'unload_asset_product_list' => function($q) {
                                                    return $q->with([
                                                        'unload_asset' => function($s) {
                                                    	    return $s->with(['broker']);
                                                        },
                                                        'garden',
                                                        'tea_grade'
                                                    ]);
                                                }
                                            ]);
                                        }
                                        
                                     ])
                                     ->where('id', '=', $input['id'])->first();
            return response()->json([
                'data' => $blendById
            ], 200);
        }
        else if(isset($input['daily']))
        {
            $startDay = $this->carbon::createFromFormat('Y-m-d', $input['daily'])->startOfDay();
            $endDay = $this->carbon::createFromFormat('Y-m-d', $input['daily'])->endOfDay();
            $blendDaily = $this->blend->whereBetween('date', [$startDay, $endDay])->get();

            return $blendDaily;
        }
        else if(isset($input['monthly']))
        {
            $startDay = $this->carbon::createFromFormat('F', $input['monthly'])->startOfMonth();
            $endDay = $this->carbon::createFromFormat('F', $input['monthly'])->endOfMonth();
            $blendMonthly = $this->blend->whereBetween('date', [$startDay, $endDay])->get();

            $blendDailyWise = $blendMonthly->groupBy(function($date) {
                return $this->carbon::parse($date->date)->toDateString();
            });

            $blendDailyWiseModified = [];
            foreach ($blendDailyWise as $key => $value) {
                $data = [
                    'title' => $key,
                    'type' => 'daily',
                    'value' => $blendDailyWise[$key]->sum('total_packs')
                ];
                array_push($blendDailyWiseModified, $data);
            }

            $data = [
                'type' => 'daily',
                'current_year' => $this->carbon->format('Y'),
                'current_month' => $this->carbon->format('F'),
                'current_date' => $this->carbon::now()->toFormattedDateString(),
                'data' => $blendDailyWiseModified
            ];
            return response()->json($data, 200);
        }
        else if(isset($input['yearly']))
        {
            $startMonth= $this->carbon::createFromFormat('Y', $input['yearly'])->startOfYear();
            $endMonth = $this->carbon::createFromFormat('Y', $input['yearly'])->endOfYear();
            $blendHistoryMonthly = $this->blend->whereBetween('date', [$startMonth, $endMonth])->orderBy('date', 'DESC')->get();

            $blendMonthlyWise = $blendHistoryMonthly->groupBy(function($date) {
                return $this->carbon::parse($date->date)->format('F');
            });

            $blendMonthlyWiseModified = [];
            foreach ($blendMonthlyWise as $key => $value) {
                $data = [
                    'title' => $key,
                    'type' => 'monthly',
                    'value' => $blendMonthlyWise[$key]->sum('total_packs')
                ];
                array_push($blendMonthlyWiseModified, $data);
            }

            // return $blendMonthlyWiseModified;
            // return $blendHistoryMonthly;
            $data = [
                'type' => 'monthly',
                'current_year' => $this->carbon->format('Y'),
                'current_month' => $this->carbon->format('F'),
                'current_date' => $this->carbon::now()->toFormattedDateString(),
                'data' => $blendMonthlyWiseModified
            ];
            return response()->json($data, 200);
        }
    }

    public function previewBlendData()
    {
         $blend = $this->blend->with('blend_lists')->whereBetween('date', [$this->carbon::now()->startOfMonth()->toDateString(), $this->carbon::now()->toDateString()])->orderBy('date','DSEC')->get()->groupBy(function($date){
            return $this->carbon::parse($date->date)->toFormattedDateString();
         });

         $data = [
            'blend' => $blend
         ];

         return response()->json($data, 200);
    }

    public function previewBlendListByDate($date) {
        $blend = $this->blend->with(['blend_lists.tea', 'blend_lists.tea_grade'])->where('date', '=', $date)->get();
        $data = [
            'blend' => $blend
        ];

        return response()->json($data, 200);
    }

    public function storeBlend()
    {
    	$input = $data[0] = $this->request->all();

    	$blend = $this->blend->create([
    		'date' => $input['date'],
    		'total_packs' => $input['total_packs']
    	]);

    	$blend_list = $input['blend_list'];

    	foreach ($blend_list as $value) {
    		$this->blendList->create([
    			'blend_id' => $blend->id,
    			'tea_id' => $value['tea']['id'],
    			'tea_grade_id' => $value['tea_grade_id'],
                'unload_asset_product_list_id' => $value['unload_asset_list_id'],
    			'packs' => floatval($value['packs'])
    		]);
            $asset = $this->unloadAsset->find($value['unload_asset_list_id']);
            $updateAsset = $asset->available_packs - intval($value['packs']);
            if( $updateAsset > 0 ) {
                 $this->unloadAsset->where('id', $value['unload_asset_list_id'])->update([
                    'available_packs' => $updateAsset
                ]);
            } else {
                $this->unloadAsset->where('id', $value['unload_asset_list_id'])->update([
                    'available_packs' => $updateAsset,
                    'status'          => 'N'
                ]);
            }
    	}

    	$data = [
    		'message' => 'Successfully added!'
    	];

    	return response()->json($data, 200);

    }

    public function export_blend($date)
    {
        $blend = $this->blend->with(['blend_lists.tea', 'blend_lists.tea_grade'])->where('date', '=', $date)->get();

        $blends = [
            'date' => $this->carbon->toDateString(),
            'data' => []
        ];

        foreach ($blend as $key => $value) {
           foreach ($value->blend_lists as $index => $item) {
               array_push($blends['data'], $item);
           }
        }


        $pdfName = 'Blend-list-bonani-tea-and-co-'.$this->carbon->toDateString().'-'.$this->carbon->toTimeString().'.pdf';

        $pdf = PDF::loadView('pdf.blend', compact('blends'));
        return $pdf->download($pdfName);
    }
}
