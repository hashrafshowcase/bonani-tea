<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Models\Van;
use Requent;


class VanController extends Controller {

	protected $van;
	protected $request;

	public function __construct(Van $van, Request $request) {

		$this->van = $van;
		$this->request = $request;
	}



	/**
	 * [VanIndex description]
	 * van index will show all van list 
	 */
	public function VanIndex() {
		return Requent::resource($this->van->orderBy('id', 'desc'))->get();
	}



	/**
	 * [VanCreateOrUpdate description]
	 * van create and update
	 */
	public function VanCreateOrUpdate() {
		$input = $data[0] = $this->request->all();
		if(!isset($input['id'])) {

			$van = $this->van->create([
					'van_number_plate'      => $input['van_number_plate'],
                    'van_reg_no'            => $input['van_reg_no'],
                    'van_reg_no_issue_date' => $input['van_reg_no_issue_date'],
                    'van_reg_no_expired_date' => $input['van_reg_no_expired_date'],
                    'van_tax_token'         => $input['van_tax_token'],
                    'van_tax_token_issue_date' => $input['van_tax_token_issue_date'],
                    'van_tax_token_expired_date' => $input['van_tax_token_expired_date'],
                    'van_fitness_no'        => $input['van_fitness_no'],
                    'van_fitness_no_issue_date' => $input['van_fitness_no_issue_date'],
                    'van_fitness_no_expired_date' => $input['van_fitness_no_expired_date'],
                    'van_insurance_no'      => $input['van_insurance_no'],
                    'van_insurance_no_issue_date' => $input['van_insurance_no_issue_date'],
                    'van_insurance_no_expired_date' => $input['van_insurance_no_expired_date'],
                    'van_road_permit_no'    => $input['van_road_permit_no'],
                    'van_road_permit_no_issue_date' => $input['van_road_permit_no_issue_date'],
                    'van_road_permit_no_expired_date' => $input['van_road_permit_no_expired_date']
			]);

			return response()->json([
					'message' => 'Successfully create van list.',
					'vanList' => $this->van->orderBy('id', 'desc')->get()
				], 200);

		} else {

			$van = $this->van->where('id', $input['id'])->update([

                    'van_number_plate'      => $input['van_number_plate'],
                    
                    'van_reg_no'            => $input['van_reg_no'],
                    'van_reg_no_issue_date' => $input['van_reg_no_issue_date'],
                    'van_reg_no_expired_date' => $input['van_reg_no_expired_date'],

                    'van_tax_token'         => $input['van_tax_token'],
                    'van_tax_token_issue_date' => $input['van_tax_token_issue_date'],
                    'van_tax_token_expired_date' => $input['van_tax_token_expired_date'],

                    'van_fitness_no'        => $input['van_fitness_no'],
                    'van_fitness_no_issue_date' => $input['van_fitness_no_issue_date'],
                    'van_fitness_no_expired_date' => $input['van_fitness_no_expired_date'],

                    'van_insurance_no'      => $input['van_insurance_no'],
                    'van_insurance_no_issue_date' => $input['van_insurance_no_issue_date'],
                    'van_insurance_no_expired_date' => $input['van_insurance_no_expired_date'],

                    'van_road_permit_no'    => $input['van_road_permit_no'],
                    'van_road_permit_no_issue_date' => $input['van_road_permit_no_issue_date'],
                    'van_road_permit_no_expired_date' => $input['van_road_permit_no_expired_date'],
				]);

			return response()->json([
					'message' => 'Successfully Update.',
					'vanList' => $this->van->orderBy('id', 'desc')->get()
				], 200);
		}

	}


	/**
	 * [VanRemove description]
	 * @param [type] $id [description]
	 */
	public function VanRemove($id) {

		$van = $this->van->find($id);
        try{
            $delete = $van->delete();
            $totalVan = $this->van->orderBy('id', 'desc')->get();
            if($delete){
                return response()->json([
                    'message' => 'Deleted Succesfully',
                    'vanList' => $totalVan
                ], 200);
            }
        } catch(\Exception $e) {
                return response()->json([
                    'message' => ' Already Use in Another Section'
                ], 400);
        }

	}
}