<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Models\Role;
use App\Models\UserRole;
use App\Models\User;
use Requent;

class RoleMgmtController extends Controller
{
    protected $role;
    protected $user_role;
    protected $user;
    protected $request;

    public function __construct(Role $role, UserRole $user_role, User $user,Request $request)
    {
    	$this->role = $role;
    	$this->user_role = $user_role;
        $this->user = $user;
    	$this->request = $request;
    }

    public function getOwnerAdminAndAccountant()
    {
        return Requent::resource($this->user_role->whereIn('role_id', [1,3,4]))->get();
    }

    public function getAllRoles()
    {
    	return Requent::resource($this->role->orderBy('id', 'desc'))->get();
    }

    public function getFindRole($id)
    {
    	return Requent::resource($this->role->where('id', $id))->get();
    }

    public function postCreateUpdateRoles()
    {
        $input = $data[0] = $this->request->all();

        if(!isset($input['role_id']))
        {
            $this->role->create([
                'title' => $input['title']
            ]);

            return response()->json([
                    'message' => 'successfully create role.'
                ], 200);
        }
        else
        {
            $this->role->where('id', $input['role_id'])->update([
                    'title' => $input['title']
                ]);

            return response()->json([
                    'message' => 'successfully updated role.'
                ] , 200);
        }
    }

    public function getAllUserRole()
    {
        return Requent::resource($this->user)->get();
    }

    public function getUserRoleById($user_id)
    {
    	return Requent::resource($this->user_role->where('user_id', $user_id))->get();
    }

    public function postCreateUserRoles()
    {
        $input = $data[0] = $this->request->all();

        if(!isset($input['user_role_id']))
        {
            foreach ($input['roles'] as $item) {
                $this->user_role->create([
                    'user_id' => $input['user_id'],
                    'role_id' => $item
                ]);
            }

            return response()->json([
                    'message' => 'successfully assign user role.'
                ], 200);
        }
    }
}
