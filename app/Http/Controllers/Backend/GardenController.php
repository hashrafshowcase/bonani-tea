<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Models\Garden;
use App\Http\Requests\GardenRequest;
use Requent;

class GardenController extends Controller
{
	protected $garden;
	protected $request;

	public function __construct(Garden $garden, Request $request) {

		$this->garden = $garden;
		$this->request = $request;
	}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function GardenIndex()
    {
        //die("Gardenindex");
        return Requent::resource($this->garden->orderBy('garden_name', 'ASC'))->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function GardenCreate()
    {
        //return view('backend::gardenEdit');
        return response()->json([

                'message' => 'Gonna land to create garden page.',

        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function GardenStore()
    {
        $input = $data[0] = $this->request->all();

        if(!isset($input['garden_id']))
        {
            $this->garden->create([
                'garden_name' => $input['garden_name'],
                'garden_address' => $input['garden_address']
            ]);

            return response()->json([
                    'gardenList' => Requent::resource($this->garden->orderBy('id', 'desc'))->get(),
                    'message' => 'successfully create garden.'
                ], 200);
        }
        else
        {
            $this->garden->where('id', $input['garden_id'])->update([
                    'garden_name' => $input['garden_name'],
                    'garden_address' => $input['garden_address']
                ]);

            return response()->json([
                    'gardenList' => Requent::resource($this->garden->orderBy('id', 'desc'))->get(),
                    'message' => 'successfully updated garden.'
                ] , 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function GardenShow($id)
    {
        $garden = $this->agrden->find($id);
         
         //return view('backend::gardenInfo', $garden);
         
        return response()->jason([

            'message' => 'Garden info are stored in $garden variable !!'
        
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function GardenEdit($id)
    {
        $garden = $this->garden->find($id);
        //return view('backend::gardenEdit', $garden);
        return response([
            
            'message' => 'Gonna land to gardenEdit page.'

        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function GardenUpdate()
    {
        //die('update');
        $garden = $this->garden->find($id);

        $garden->garden_name = $request->garden_name;
        $garden->garden_address = $request->garden_address;
        $garden->save();

        return jason([
            'message' => 'Garden updated successfully',

            'gardenList' => $this->garden->orderBy('id', 'desc')->get()
        ], 200); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function GardenRemove($id)
    {
        //die("remove");
		$garden = $this->garden->find($id);

        try{
            $delete = $garden->delete();
            if($delete){
                return response()->json([
                    
                    'message' => 'Deleted Succesfully',
                    
                    'gardenList' => $this->garden->orderBy('id', 'desc')->get()
                ], 200);
            }
        } catch(\Exception $e) {
                return response()->json([
                    'message' => ' Already Use in Another Section'
                ], 400);
        }

    }
}
