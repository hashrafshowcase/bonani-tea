<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Models\Brokers;
use Requent;

class BrokerController extends Controller
{
    protected $request;
    protected $broker;

    /**
     * [__construct description]
     * @param Request $request [description]
     * @param Broker  $broker  [description]
     */
    public function __construct(Request $request, Brokers $broker)
    {
    	$this->request = $request;
    	$this->broker = $broker;
    }

    /**
     * [allBroker description]
     * @return [type] [description]
     */
    public function allBroker()
    {
    	return Requent::resource($this->broker->orderBy('broker_name', 'asc'))->get();
    }


    /**
     * [findBrokerById description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function findBrokerById($id)
    {
    	return Requent::resource($this->broker)->find($id);
    }


    /**
     * [brokerCreateOrUpdate description]
     * @return [type] [description]
     */
    public function brokerCreateOrUpdate()
    {
    	$input = $data[0] = $this->request->all();

    	if(!isset($input['broker_id']))
    	{
    		$this->broker->create([
    			'broker_name'         => $input['broker_name'],
          'broker_email'        => $input['broker_email'],
          'broker_phone_number' => $input['broker_phone_number'],
    			'broker_address'      => $input['broker_address']
    		]);

    		return response()->json([
                    'brokers' => Requent::resource($this->broker->orderBy('id', 'desc'))->get(),
    				'message' => 'successfully create Broker.'
    			], 200);
    	}
    	else
    	{
    		$this->broker->where('id', $input['broker_id'])->update([
    				'broker_name'         => $input['broker_name'],
            'broker_email'        => $input['broker_email'],
            'broker_phone_number' => $input['broker_phone_number'],
            'broker_address'      => $input['broker_address']
    			]);

    		return response()->json([
                    'brokers' => Requent::resource($this->broker->orderBy('id', 'desc'))->get(),
    				'message' => 'successfully updated broker.'
    			] , 200);
    	}
    }

    public function removeBroker($id)
    {
    	$broker = $this->broker->find($id);
	    try{
          $delete = $broker->delete();
          if($delete){
              return response()->json([
                  'brokers' => Requent::resource($this->broker->orderBy('id', 'desc'))->get(),
                  'message' => ' Deleted Succesfully'
              ], 200);
          }
	    } catch(\Exception $e) {
	        return response()->json([
	        	'message' => ' Already Use in quantities'
	            ], 400);
	    }
    }
}
