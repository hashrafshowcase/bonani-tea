<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Models\TeaGrade;
use Requent;

class TeaGradeController extends Controller
{
    protected $request;
    protected $tea_grade;

   /**
    * [__construct description]
    * @param Request  $request   [description]
    * @param TeaGrade $tea_grade [description]
    */
    public function __construct(Request $request, TeaGrade $tea_grade)
    {
    	$this->request = $request;
    	$this->tea_grade = $tea_grade;
    }

    /**
     * [allTeaGrade description]
     * @return [type] [description]
     */
    public function allTeaGrade()
    {
    	return Requent::resource($this->tea_grade->orderBy('id', 'desc'))->get();
    }


    /**
     * [findTeaGradeById description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function findTeaGradeById($id)
    {
    	return Requent::resource($this->tea_grade)->find($id);
    }


    /**
     * [teaGradeCreateOrUpdate description]
     * @return [type] [description]
     */
    public function teaGradeCreateOrUpdate()
    {
    	$input = $data[0] = $this->request->all();

    	if(!isset($input['tea_grade_id']))
    	{
    		$this->tea_grade->create([
    			'grade_name' => $input['grade_name'],
                'total_packs' => 0.00,
                'total_kg'  => 0.00
    		]);

    		return response()->json([
    				'message' => 'successfully create Tea Grade.'
    			], 200);
    	}
    	else
    	{
    		$this->tea_grade->where('id', $input['tea_grade_id'])->update([
    				'grade_name' => $input['grade_name'],
    			]);

    		return response()->json([
    				'message' => 'successfully updated Tea Grade.'
    			] , 200);
    	}
    }

    /**
     * [removeTeaGrade description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function removeTeaGrade($id)
    {
    	$teaGrade = $this->tea_grade->find($id);
	    try{
          $delete = $teaGrade->delete();
          if($delete){
              return response()->json([
                  'message' => ' Deleted Succesfully'
              ], 200);
          }
	    } catch(\Exception $e) {
	        return response()->json([
	        	'message' => ' Already Use in another Properties'
	            ], 400);
	    }
    }
}
