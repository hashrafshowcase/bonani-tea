<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\ImageUploadRequest;
use Illuminate\Support\Facades\Input;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Http\Request;

class FileUploadController extends Controller
{
	public function uploadImage(Request $request)
	{
		try {
			$file = $request->file('file');
			$fileName = strtotime(date('Y-m-d H:i:s')) . "-" . str_replace(' ', '-', $file->getClientOriginalName());
			$destinationPath = public_path('upload-images/' . $fileName);
			if ($request->get('size') == 'full') {
				Image::make($file->getRealPath())->save($destinationPath);
			} else {
				Image::make($file->getRealPath())->resize(
					300,
					null,
					function ($constraint) {
						$constraint->aspectRatio();
					}
				)->save($destinationPath);
			}
			$fileLink = url("/upload-images/" . $fileName);
			return response()->json([
				'status'   => 200,
				'file'     => $file,
				'fileName' => $fileName,
				'fileLink' => $fileLink,
				'message'  => 'File successfully uploaded!'
			], 200);
		} catch (Exception $e) {
			return response()->json([
				'status'  => 400,
				'message' => 'Failed to upload file',
				'error'   => $e->getMessage()
			], 400);
		}
	}

	public function uploadFile(Request $request)
	{
		$file  = $request->file('file');
		try {
			if ($file->getClientOriginalExtension() == 'doc' || $file->getClientOriginalExtension() == 'docx' || $file->getClientOriginalExtension() == 'pdf' || $file->getClientOriginalExtension() == 'jpg' || $file->getClientOriginalExtension() == 'png') {
				$fileName        = rand(1, 100000) . strtotime(date('Y-m-d H:i:s')) . "-" . str_replace(' ', '-', $file->getClientOriginalName());
				$destinationPath = public_path('upload-files/');
				$file->move($destinationPath, $fileName);
				$fileLink = url("/upload-files/" . $fileName);
				$fileMimeType = $file->getClientOriginalExtension();
				return response()->json([
					'status'   => 200,
					'file'     => $file,
					'fileName' => $fileName,
					'fileLink' => $fileLink,
					'mimeType' => $fileMimeType,
					'message'  => 'File successfully uploaded!'
				], 200);
			} else {
				return response()->json([
					'status'  => 400,
					'message' => 'Unsupported file type',
				], 400);
			}
		} catch (\Exception $exception) {
			return response()->json([
				'status'  => 400,
				'message' => 'Failed to upload file',
				'error'   => $exception->getMessage()
			], 400);
		}
	}
}
