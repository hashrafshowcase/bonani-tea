<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Models\HomeRent;
use Carbon\Carbon;
use PDF;

class HomeRentController extends Controller
{
    protected $request;
    protected $homeRent;
    protected $carbon;

    public function __construct(Request $request, HomeRent $homeRent, Carbon $carbon)
    {
    	$this->request = $request;
    	$this->homeRent = $homeRent;
    	$this->carbon = $carbon;
    }

    /**
     * Home Rent History
     * @return [type] [description]
     */
    public function homeRentHistory($type = false)
    {
        $status = $type ? $type : 'daily';
        $status = explode('advance-', $status);
        $status = count($status) > 1 ? $status : $status[0];
        if($status == 'daily') {

            $homeRentHistoryDaily = $this->homeRent->whereBetween('created_at', [$this->carbon::now()->startOfMonth(), $this->carbon::now()])->get();


            $homeRentDailyWise = $homeRentHistoryDaily->groupBy(function($date) {
                return $this->carbon::parse($date->created_at)->toDateString();
            });

            $homeRentDailyWiseModified = [];
            foreach ($homeRentDailyWise as $key => $value) {
                $data = [
                    'title' => $key,
                    'type' => 'daily',
                    'value' => $homeRentDailyWise[$key]->sum('value')
                ];
                array_push($homeRentDailyWiseModified, $data);
            }

            $data = [
                'type' => $status,
                'current_year' => $this->carbon->format('Y'),
                'current_month' => $this->carbon->format('F'),
                'current_date' => $this->carbon::now()->toFormattedDateString(),
                'data' => $homeRentDailyWiseModified
            ];
            return response()->json($data, 200);
        }
        else if($status == 'monthly') {
            $homeRentHistoryMonthly = $this->homeRent->whereBetween('created_at', [$this->carbon::now()->startOfYear(), $this->carbon::now()])->orderBy('created_at', 'DESC')->get();

            $homeRentMonthlyWise = $homeRentHistoryMonthly->groupBy(function($date) {
                return $this->carbon::parse($date->created_at)->format('F');
            });


            $homeRentMonthlyWiseModified = [];
            foreach ($homeRentMonthlyWise as $key => $value) {
                $data = [
                    'title' => $key,
                    'type' => 'monthly',
                    'value' => $homeRentMonthlyWise[$key]->sum('value')
                ];
                array_push($homeRentMonthlyWiseModified, $data);
            }

            // return $homeRentMonthlyWiseModified;
            // return $homeRentHistoryMonthly;
            $data = [
                'type' => $status,
                'current_year' => $this->carbon->format('Y'),
                'current_month' => $this->carbon->format('F'),
                'current_date' => $this->carbon::now()->toFormattedDateString(),
                'data' => $homeRentMonthlyWiseModified
            ];
            return response()->json($data, 200);
        }
        else if($status == 'yearly')
        {
            $homeRentHistoryYearly = $this->homeRent->whereBetween('created_at', [$this->carbon::create(2017, 01, 01, 00, 00, 00), $this->carbon::now()])->get();

            $homeRentHistoryYearyWise = $homeRentHistoryYearly->groupBy(function($date) {
                return $this->carbon::parse($date->created_at)->format('Y');
            });

            $homeRentHistoryYearyWiseModified = [];
            foreach ($homeRentHistoryYearyWise as $key => $value) {
               $data = [
                    'title' => $key,
                    'type' => 'yearly',
                    'value' => $homeRentHistoryYearyWise[$key]->sum('value')
               ];
               array_push($homeRentHistoryYearyWiseModified, $data);
            }

            $data = [
                'type' => $status,
                'current_year' => $this->carbon->format('Y'),
                'current_month' => $this->carbon->format('F'),
                'current_date' => $this->carbon::now()->toFormattedDateString(),
                'data' => $homeRentHistoryYearyWiseModified
            ];
            return response()->json($data, 200);
        }
        else if(count($status) > 1) {

            $homeRentHistoryAdvance = $this->homeRent->whereBetween('created_at', [$this->carbon::createFromFormat('Y-m-d', $status[1])->startOfMonth(), $this->carbon::createFromFormat('Y-m-d', $status[1])->endOfMonth()])->get();


            $homeRentAdvanceWise = $homeRentHistoryAdvance->groupBy(function($date) {
                return $this->carbon::parse($date->created_at)->toDateString();
            });

            // return $homeRentAdvanceWise;
            $homeRentAdvanceWiseModified = [];
            foreach ($homeRentAdvanceWise as $key => $value) {
                $data = [
                    'title' => $key,
                    'type' => 'advance',
                    'value' => $homeRentAdvanceWise[$key]->sum('value')
                ];
                array_push($homeRentAdvanceWiseModified, $data);
            }

            $data = [
                'type' => 'advance',
                'current_year' => $this->carbon->format('Y'),
                'current_month' => $this->carbon->format('F'),
                'current_date' => $this->carbon::now()->toFormattedDateString(),
                'data' => $homeRentAdvanceWiseModified
            ];
            return response()->json($data, 200);
        }
    }

    public function storeHomeRent()
    {
        $input = $data[0] = $this->request->all();

        foreach ($input as $key => $value) {
            $this->homeRent->create([
                'description' => $value['rent'],
                'value' => (int) $value['value']
            ]);
        }

        $data = [
            'message' => 'Successfully added!'
        ];

        return response()->json($data, 200);
    }

    public function homeRentSearch(){
        $input = $data[0] = $this->request->all();

        if(isset($input['id']))
        {
            $homeRentById = $this->homeRent->where('id', '=', $input['id'])->get();
            return response()->json([
                'data' => $homeRentById
            ], 200);
        }
        else if(isset($input['daily']))
        {
            $startDay = $this->carbon::createFromFormat('Y-m-d', $input['daily'])->startOfDay();
            $endDay = $this->carbon::createFromFormat('Y-m-d', $input['daily'])->endOfDay();
            $homeRentDaily = $this->homeRent->whereBetween('created_at', [$startDay, $endDay])->get();

            return $homeRentDaily;
        }
        else if(isset($input['monthly']))
        {
            $startDay = $this->carbon::createFromFormat('F', $input['monthly'])->startOfMonth();
            $endDay = $this->carbon::createFromFormat('F', $input['monthly'])->endOfMonth();
            $homeRentMonthly = $this->homeRent->whereBetween('created_at', [$startDay, $endDay])->get();

            $homeRentDailyWise = $homeRentMonthly->groupBy(function($date) {
                return $this->carbon::parse($date->created_at)->toDateString();
            });

            $homeRentDailyWiseModified = [];
            foreach ($homeRentDailyWise as $key => $value) {
                $data = [
                    'title' => $key,
                    'type' => 'daily',
                    'value' => $homeRentDailyWise[$key]->sum('value')
                ];
                array_push($homeRentDailyWiseModified, $data);
            }

            $data = [
                'type' => 'daily',
                'current_year' => $this->carbon->format('Y'),
                'current_month' => $this->carbon->format('F'),
                'current_date' => $this->carbon::now()->toFormattedDateString(),
                'data' => $homeRentDailyWiseModified
            ];
            return response()->json($data, 200);
        }
        else if(isset($input['yearly']))
        {
            $startMonth= $this->carbon::createFromFormat('Y', $input['yearly'])->startOfYear();
            $endMonth = $this->carbon::createFromFormat('Y', $input['yearly'])->endOfYear();
            $homeRentHistoryMonthly = $this->homeRent->whereBetween('created_at', [$startMonth, $endMonth])->orderBy('created_at', 'DESC')->get();

            $homeRentMonthlyWise = $homeRentHistoryMonthly->groupBy(function($date) {
                return $this->carbon::parse($date->created_at)->format('F');
            });

            $homeRentMonthlyWiseModified = [];
            foreach ($homeRentMonthlyWise as $key => $value) {
                $data = [
                    'title' => $key,
                    'type' => 'monthly',
                    'value' => $homeRentMonthlyWise[$key]->sum('value')
                ];
                array_push($homeRentMonthlyWiseModified, $data);
            }

            // return $homeRentMonthlyWiseModified;
            // return $homeRentHistoryMonthly;
            $data = [
                'type' => 'monthly',
                'current_year' => $this->carbon->format('Y'),
                'current_month' => $this->carbon->format('F'),
                'current_date' => $this->carbon::now()->toFormattedDateString(),
                'data' => $homeRentMonthlyWiseModified
            ];
            return response()->json($data, 200);
        }
    }

}
