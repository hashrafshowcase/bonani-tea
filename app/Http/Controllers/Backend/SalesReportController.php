<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Requent;
use App\Models\SalesReport;
use App\Models\SRcustomer;
use App\Models\SRQuantities;
use App\Models\SRSpcQuantities;
use App\Models\Salesman;
use App\Models\Area;
use App\Models\LocalArea;
use App\Models\AccountHistory;
use App\User;
use Carbon\Carbon;
use Auth;
use App\Models\StockIndex;
use App\Models\Stocks;
use App\Models\Teaquantity;

class SalesReportController extends Controller
{
    
	protected $request;
	protected $report;
    protected $customer;
    protected $quantities;
    protected $specQuantities;
    protected $salesman;
    protected $area;
    protected $localArea;
    protected $account;
    protected $stocks;
    protected $stockIndex;
    protected $user;
    protected $Teaquantity;

    public function __construct(Request $request, SalesReport $salesReport, SRcustomer $customer, SRQuantities $quantities, SRSpcQuantities $specQuantities, Salesman $salesman, Area $area, LocalArea $localArea, AccountHistory $account, User $user, StockIndex $stockIndex, Stocks $stocks, Teaquantity $Teaquantity)
    {

    	$this->request = $request;

    	$this->report = $salesReport;

        $this->customer = $customer;

        $this->quantities = $quantities;

        $this->specQuantities = $specQuantities;

        $this->salesman = $salesman;

        $this->area = $area;

        $this->localArea = $localArea;

        $this->account = $account;

        $this->user = $user;

        $this->stockIndex = $stockIndex;

        $this->stocks = $stocks;

        $this->Teaquantity = $Teaquantity;
    }


    public function getSalesReportBySalesmanIdAdvance3(){
        $input = $data[0] = $this->request->all();
         $getQuantity = $this->Teaquantity->get();
        $report = $this->customer->with(['salesman',
                                         'sales_report_quantites',
                                         'sales_report_quantites.teaquantity'
                                    ])
                                   ->whereBetween('salesreport_created_at',[$input['start_at'], $input['end_at']])
                                   ->where('local_area_id', $input['local_area_id'])
                                   ->where('area_id', $input['area_id'])
                                   ->get();
        foreach ($report as $key => $customer) {
            $report[$key]['reports'] = array();
            $data = array();
            foreach ($getQuantity as $key2 => $value) {
                foreach ($customer->sales_report_quantites as $key1 => $quantity) {
                  if($value->id == $quantity->teaquantity_id) {
                        array_push($data, $quantity->quantity);
                  }
                 
               }
           }
           $report[$key]['reports'] = $data;
        }

        return response()->json([
            'tea_product'  => $getQuantity,
            'customer'      => $report
        ], 200);
    }

    public function getSalesReportBySalesmanIdAdvance2(){
        $input = $data[0] = $this->request->all();
        $report = $this->report->with(['salesman','sales_report_customer' => function($q) use ($input) {
                                        return $q->where('local_area_id', $input['local_area_id']);
                                    }])
                                   ->whereBetween('salesreport_created_at',[$input['start_at'], $input['end_at']])
                                   ->where('area_id', $input['area_id'])
                                   ->get();

        return response()->json([
            'customer'      => $report
        ], 200);
    }

    public function getSalesReportBySalesmanIdAdvance1(){
        $input = $data[0] = $this->request->all();
        $customer = $this->customer->with([
                                        'local_area',
                                        'area',
                                        'customer',
                                        'sales_report' => function($query) use($input) {
                                            return $query->with([
                                                            'sales_report_quantites',
                                                            'sales_report_quantites.teaquantity',
                                                            'sales_report_spec_quantites'
                                                        ])
                                                         ->where('salesreport_created_at', $input['start_at']);
                                        },
                                        'sales_report_quantites',
                                        'sales_report_quantites.teaquantity',
                                   ])
                                   ->whereBetween('salesreport_created_at',[$input['start_at'], $input['end_at']])
                                   ->where('salesman_id', $input['salesman_id'])
                                   ->get();

        return response()->json([
            'customer'      => $customer
        ], 200);
    }

    public function getSalesReportBySalesmanId(){
        $input = $data[0] = $this->request->all();
        $customer = $this->customer->with([
                                        'local_area',
                                        'area',
                                        'customer',
                                        'sales_report' => function($query) use($input) {
                                            return $query->with([
                                                            'sales_report_quantites',
                                                            'sales_report_quantites.teaquantity',
                                                            'sales_report_spec_quantites'
                                                        ])
                                                         ->where('salesreport_created_at', $input['create_at']);
                                        },
                                        'sales_report_quantites',
                                        'sales_report_quantites.teaquantity',
                                   ])
                                   ->where('salesreport_created_at','=', $input['create_at'])
                                   ->where('salesman_id', $input['salesman_id'])
                                   ->get();

        return response()->json([
            'customer'      => $customer
        ], 200);
    }


    public function index() 
    {

    	return Requent::resource($this->report->orderBy('id', 'desc'))->get();

    }


    public function findById($id) 
    {

        return Requent::resource($this->report)->find($id);
    }



    public function findBySalesman($salesman_id) 
    {
        
        return Requent::resource($this->salesman)->find($salesman_id);
    }



    public function findByArea($area_id)
    {
        return Requent::resource($this->area)->find($area_id);
    }



    public function findByCashMemo($cash_memo)
    {
        return collect(Requent::resource($this->customer->where('cash_memo', $cash_memo))->get())->groupBy('sales_report');
    }



    public function findByLocalArea($local_area_id)
    {
        return Requent::resource($this->localArea)->find($local_area_id);
    }

    public function findBySalesManArea()
    {

        $model = $this->report;
        $start_time = $this->request->get('start_time');
        $end_time = $this->request->get('end_time');

        if($this->request->has('salesman_id') && $this->request->has('area_id') && $this->request->has('localarea_id') &&  $this->request->has('customer_id')) 
        {
             $model  = $this->customer->where('salesman_id', $this->request->get('salesman_id'))
                                ->where('area_id', $this->request->get('area_id'))
                                ->where('local_area_id', $this->request->get('localarea_id'))
                                ->where('customer_id', $this->request->get('customer_id'))
                                ->whereBetween('salesreport_created_at', [$start_time, $end_time]);
                                
             return Requent::resource($model)->get();
        }

        if($this->request->has('salesman_id') && $this->request->has('area_id') && $this->request->has('localarea_id')) 
        {
             $model  = $this->customer->where('salesman_id', $this->request->get('salesman_id'))
                                ->where('area_id', $this->request->get('area_id'))
                                ->where('local_area_id', $this->request->get('localarea_id'))
                                ->whereBetween('salesreport_created_at', [$start_time, $end_time]);
                                
             return Requent::resource($model)->get();
        }

        
        if($this->request->has('salesman_id') && $this->request->has('area_id'))  {
            $model  = $model->where('salesman_id', $this->request->get('salesman_id'))
                                ->where('area_id', $this->request->get('area_id'))
                                ->whereBetween('salesreport_created_at', [$start_time, $end_time]);
        }

        if($this->request->has('salesman_id')) {
            $model  = $model->where('salesman_id', $this->request->get('salesman_id'))
                             ->whereBetween('salesreport_created_at', [$start_time, $end_time]);
        }


        

        return Requent::resource($model)->get();
    }

    public function test()
    {
        dd(new Carbon($this->request->get('times')));
    }

    public function createOrUpdate() 
    {

    	$input = $data[0] = $this->request->all();

        

    	$SalesReport = $this->report->create([
    	        'sales_date' => $input['sales_date'],
                'salesman_id' => $input['selected_salesman'],
                'authorized_id' => Auth::user()->id,
                'area_id' => $input['selected_area'],
                'van_id' => $input['selected_van'],
                'total_kg' => $input['total_kg'],
                'total_amount' => $input['total_amount'],
                'salesreport_created_at' => $input['event_time']
            ]);

        // $this->account->create([
        //     'description' => $this->user->find($this->salesman->find($input['salesman'])->user_id)->name .' added a sales report and report '. $input['total_amount'] .' cash earned',
        //     'history_type' => 'cash',
        //     'account_type' => 'credit',
        //     'value' => $input['total_amount']
        // ]);

        // $this->account->create([
        //     'description' => $this->user->find($this->salesman->find($input['salesman'])->user_id)->name .' added a sales report and report '. $input['total_kg'] .' kg sale',
        //     'history_type' => 'stock',
        //     'account_type' => 'debit',
        //     'value' => $input['total_kg']
        // ]);
 
        foreach ($input['reports'] as $report) {
            $sales_customer_report = $this->customer->create([
                    'sales_report_id' => $SalesReport->id,
                    'salesman_id' => $input['selected_salesman'],
                    'customer_id' => $report['selected_customer'],
                    'area_id' => $input['selected_area'],
                    'local_area_id' => $report['local_area_id'],
                    'cash_memo' => $report['cash_memo'],
                    'total_amount' => $report['total'],
                    'comments' => 'Dummy Text',
                    'salesreport_created_at' => $input['event_time']
                ]);
            foreach($report['productList'] as $data) {

                $sales_report_quantity = $this->quantities->create([
                        'sales_report_id' => $SalesReport->id,
                        's_rcustomer_id' => $sales_customer_report->id,
                        'teaquantity_id' => $data['id'],
                        'quantity' => $data['value'],
                        'amount' => $data['total'],
                        'note'   => $data['note'],
                        'note_value'    => $data['note_value'],
                        'salesreport_created_at' => $input['event_time']
                    ]);

                $sales_Spec_Quantities = $this->specQuantities->create([
                        'sales_report_id' => $SalesReport->id,
                        'area_id' => $input['selected_area'],
                        'teaquantity_id' => $data['id'],
                        'total_quantity' => $data['total'],
                        'salesreport_created_at' => $input['event_time']
                    ]);

                $getStockIndexId = $this->stockIndex->where('teaquantities_id', $data['id'])->first()->id;

                $getStock = $this->stocks->where('stock_indices_id', $getStockIndexId)->where('curent_date', $input['event_time'])->first();

                if( ! $getStock ) {
                    $getLatestStock = $this->stocks->where('stock_indices_id', $data['id'])->latest()->first()->stocks_balance;
                    $this->stocks->insert([
                        'stock_indices_id' => $getStockIndexId,
                        'curent_date'      => $input['event_time'],
                        'stocks_past_deposit' => $getLatestStock,
                        'stocks_new_deposit' => 0,
                        'stocks_new_deposit_cash_memo' => 'Sales Report',
                        'stocks_sum_deposit' => $getLatestStock,
                        'stocks_quantity_sold' => $data['value'],
                        'stocks_quantity_sold_cash_memo' => 'First Cash Memo',
                        'stocks_balance' => $getLatestStock - floatval($data['value'])
                    ]);
                } else {
                    $this->stocks->where('id', $getStock->id)->update([
                        'stocks_quantity_sold' => $getStock->stocks_quantity_sold + intval($data['value']),
                        'stocks_balance' => $getStock->stocks_balance - intval($data['value'])
                    ]);
                }

            }
        }

    	return response()->json([
                'message' => 'successfully Created'
            ], 200);

    }

    public function currentDateSalesReport()
    {
        $input = $data[0] = $this->request->all();

        $getSalesMan = $this->report->where('sales_date', $input['current_date'])->distinct('salesman_id')->pluck('salesman_id');

        $salsman = array();

        foreach ($getSalesMan as $key => $value) {
            $data = $this->salesman->with('user')->find($value);
            array_push($salsman, $data);
        }

        $getSalesReport = $this->report->with([
                                            'sales_report_quantites'
                                        ])->where('sales_date', $input['current_date'])->get();

        $totalProuductList = $this->Teaquantity->get();

        foreach ($totalProuductList as $key => $value) {
            $totalProuductList[$key]['total'] = 0;
            $totalProuductList[$key]['quantity'] = 0;
        }

        foreach ($getSalesReport as $key => $report) {
            foreach ($report->sales_report_quantites as $key1 => $item) {
               $findIndex = $this->findArrayIndex($totalProuductList, $item->teaquantity_id);
               $totalProuductList[$findIndex]->quantity += $item->quantity * $totalProuductList[$findIndex]->kg;
               $totalProuductList[$findIndex]->total += $item->amount;
            }
        }

       $args = array(
            'today_salesman' => $salsman,
            'today_report'   => $totalProuductList
       );

       return response()->json($args, 200);

        

    }

    public function getSingleSalesReport($sales_report_id)
    {
        $getQuantity = $this->Teaquantity->get();

        $getSalesReport = $this->report->with([
                                            'area',
                                            'van',
                                            'salesman.user',
                                            'sales_report_customer.customer',
                                             'sales_report_customer.local_area',
                                            'sales_report_customer.sales_report_quantites'
                                        ])
                                        ->find($sales_report_id);
        foreach ($getSalesReport->sales_report_customer as $key => $customer) {
            $getSalesReport->sales_report_customer[$key]['reports'] = array();
            $data = array();
            foreach ($getQuantity as $key2 => $value) {
                foreach ($customer->sales_report_quantites as $key1 => $quantity) {
                  if($value->id == $quantity->teaquantity_id) {
                        array_push($data, $quantity->quantity);
                  }
                 
               }
           }
           $getSalesReport->sales_report_customer[$key]['reports'] = $data;
        }

        return response()->json([
            'tea_product' => $getQuantity,
            'tea_report' => $getSalesReport
        ]);
    }

    public function getSalesReportByDateRange()
    {
        $input = $data[0] = $this->request->all();

        $getSalesReport = $this->report->whereBetween('sales_date' , [$input['start_date'], $input['end_date']])->get();

        return response()->json([
            'sales_report_range' => $getSalesReport
        ]);
    }

    public function getSalesReportBySalesmanIdwithRange()
    {
         $input = $data[0] = $this->request->all();

        $getSalesReport = $this->report->with(['sales_report_customer', 'area', 'salesman'])->where('salesman_id', $input['salesman'])->whereBetween('sales_date' , [$input['start_date'], $input['end_date']])->get();

        return response()->json([
            'sales_report_range' => $getSalesReport
        ]);
    }

    public function getRangeProductList()
    {
        $input = $data[0] = $this->request->all();
        $getSalesReport = $this->report->with([
                                            'sales_report_quantites'
                                        ])->whereBetween('sales_date', [$input['start_date'], $input['end_date']])->get();

        $totalProuductList = $this->Teaquantity->get();

        foreach ($totalProuductList as $key => $value) {
            $totalProuductList[$key]['total'] = 0;
            $totalProuductList[$key]['quantity'] = 0;
        }

        foreach ($getSalesReport as $key => $report) {
            foreach ($report->sales_report_quantites as $key1 => $item) {
               $findIndex = $this->findArrayIndex($totalProuductList, $item->teaquantity_id);
               $totalProuductList[$findIndex]->quantity += $item->quantity * $totalProuductList[$findIndex]->kg;
               $totalProuductList[$findIndex]->total += $item->amount;
            }
        }

       $args = array(
            'today_report'   => $totalProuductList
       );

       return response()->json($args, 200);

    }

    public function findArrayIndex($array, $id) {
        foreach ($array as $key => $value) {
            if( $value->id == $id ) {
                return $key;
            }
        }
        return -1;
    } 
}
