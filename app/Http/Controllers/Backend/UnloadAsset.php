<?php

namespace App\Http\Controllers\Backend;

use App\Models\UnloadAsset;
use App\Models\UnloadAssetProductList;
use App\Models\TeaGrade;

use Illuminate\Http\Request;

use Requent;

class UnloadAsset extends Controller
{
	protected $request;
	protected $unload_asset;
	protected $unload_asset_lists;

    public function __construct(Request $request, UnloadAsset $unload_asset, UnloadAssetProductList $unload_asset_lists)
    {
    	$this->request = $request;
    	$this->unload_asset = $unload_asset;
    	$this->unload_asset_lists = $unload_asset_lists;
    }

    public function store()
    {
    	return $this->request->all();
    }
}
