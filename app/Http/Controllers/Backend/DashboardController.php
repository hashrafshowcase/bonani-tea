<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\User;
use App\Models\Tea;
use App\Models\Teaquantity;
use App\Models\Role;
use App\Models\Area;
use App\Models\LocalArea;
use App\Models\Customer;
use App\Models\UserRole;
use App\Models\Van;
use App\Models\Garden;
use App\Models\SalesReport;
use App\Models\SRcustomer;
use App\Models\SRQuantities;
use App\Models\SRSpcQuantities;
use App\Models\Salesman;
use App\Models\TeaGrade;
use App\Models\Brokers;
use App\Models\StockCategory;
use App\Models\StockIndex;
use App\Models\Stocks;
use App\Models\Vat;
use App\Models\UnloadAsset;
use App\Models\UnloadAssetProductList;
use App\Models\Blend;
use App\Models\BlendList;
use Carbon\Carbon;

class DashboardController extends Controller
{
    protected $area;
    protected $localarea;
    protected $customer;
    protected $salesman;
    protected $van;
    protected $brokers;
    protected $garden;
    protected $stocks;
    protected $unload_asset;
    protected $blend;
    protected $sales_report;

    protected $request;

    public function __construct(Area $area, LocalArea $localarea, Customer $customer, Salesman $salesman, Request $request, Brokers $brokers, Van $van, Garden $garden, Stocks $stocks, UnloadAsset $unload_asset,Blend $blend, SalesReport $sales_report) 
    {
    	$this->area = $area;
    	$this->localarea = $localarea;
    	$this->customer = $customer;
    	$this->salesman = $salesman;
    	$this->van = $van;
    	$this->brokers = $brokers;
    	$this->garden = $garden;
    	$this->stocks = $stocks;
    	$this->unload_asset = $unload_asset;
    	$this->blend = $blend;
        $this->sales_report = $sales_report;


    	$this->request = $request;
    }

    public function dashboardView()
    {
    	/**
    	 * daily purchase
    	 * @var [type]
    	 */
        $purchaseDaily = $this->unload_asset->whereBetween('created_at', [Carbon::now()->startOfDay(), Carbon::now()])->get();

    	$dailyPurchase = $this->unload_asset->whereBetween('created_at' , [Carbon::now()->startOfMonth(), Carbon::now()])->get()->groupBy(function($date) {
					        return Carbon::parse($date->created_at)->toDateString();
					  });
    	$daily = [];
    	foreach ($dailyPurchase as $key => $value) {
    		$daily[$key] = $value->sum('total_kg');
    	}

    	/**
    	 * Monthly Purchase
    	 * @var [type]
    	 */
    	$monthPurchase = $this->unload_asset->whereBetween('created_at' , [Carbon::now()->startOfYear(), Carbon::now()])->get()->groupBy(function($date) {
					        return Carbon::parse($date->created_at)->format('F');
					  });
    	$month = [];
    	foreach ($monthPurchase as $key => $value) {
    		$month[$key] = $value->sum('total_kg');
    	}

    	/**
    	 * Yearly purchase
    	 * @var [type]
    	 */
    	$yearlyPurchase = $this->unload_asset->whereBetween('created_at' , [Carbon::create(2017, 01, 01, 00, 00, 00), Carbon::now()])->get()->groupBy(function($date) {
					        return Carbon::parse($date->created_at)->format('Y');
					  });
    	$yearly = [];
    	foreach ($yearlyPurchase as $key => $value) {
    		$yearly[$key] = $value->sum('total_kg');
    	}

    	/**
    	 * Stock daily
    	 */
    	$stockDailyQuery = $this->stocks->whereBetween('created_at' , [Carbon::now()->startOfMonth(), Carbon::now()])->get()->groupBy(function($date) {
					        return Carbon::parse($date->created_at)->toDateString();
					  });
    	$stockDaily = [];
    	foreach ($stockDailyQuery as $key => $value) {
    		$stockDaily[$key] = $value->sum('stocks_balance');
    	}

    	/**
    	 * monthly Stock
    	 */
    	$stockMonthlyQuery = $this->stocks->whereBetween('created_at' , [Carbon::now()->startOfYear(), Carbon::now()])->get()->groupBy(function($date) {
					        return Carbon::parse($date->created_at)->format('F');
					  });
    	$stockMonthly = [];
    	foreach ($stockMonthlyQuery as $key => $value) {
    		$stockMonthly[$key] = $value->sum('stocks_balance');
    	}

    	/**
    	 * yearly stocks
    	 */
    	$stockYearlyQuery = $this->stocks->whereBetween('created_at' , [Carbon::create(2017, 01, 01, 00, 00, 00), Carbon::now()])->get()->groupBy(function($date) {
					        return Carbon::parse($date->created_at)->format('Y');
					  });
    	$stockYearly = [];
    	foreach ($stockYearlyQuery as $key => $value) {
    		$stockYearly[$key] = $value->sum('stocks_balance');
    	}

    	/**
    	 * blend daily
    	 */
        
        $blendReportDaily = $this->blend->where('date', '=', Carbon::now()->toDateString())->get();

    	$blendDailyQuery = $this->blend->whereBetween('created_at' , [Carbon::now()->startOfMonth(), Carbon::now()])->get()->groupBy(function($date) {
					        return Carbon::parse($date->created_at)->toDateString();
					  });
    	$blendDaily = [];
    	foreach ($blendDailyQuery as $key => $value) {
    		$blendDaily[$key] = $value->sum('total_packs');
    	}

    	/**
    	 * monthly Stock
    	 */
    	$blendMonthlyQuery = $this->blend->whereBetween('created_at' , [Carbon::now()->startOfYear(), Carbon::now()])->get()->groupBy(function($date) {
					        return Carbon::parse($date->created_at)->format('F');
					  });
    	$blendMonthly = [];
    	foreach ($blendMonthlyQuery as $key => $value) {
    		$blendMonthly[$key] = $value->sum('total_packs');
    	}

    	/**
    	 * yearly stocks
    	 */
    	$blendYearlyQuery = $this->blend->whereBetween('created_at' , [Carbon::create(2017, 01, 01, 00, 00, 00), Carbon::now()])->get()->groupBy(function($date) {
					        return Carbon::parse($date->created_at)->format('Y');
					  });
    	$blendYearly = [];
    	foreach ($blendYearlyQuery as $key => $value) {
    		$blendYearly[$key] = $value->sum('total_packs');
    	}

        /**
         * sales report daily
         */
        $salesReportDailyQuery = $this->sales_report->where(['status' => 'paid'])->whereBetween('created_at' , [Carbon::now()->startOfMonth(), Carbon::now()])->get()->groupBy(function($date) {
                            return Carbon::parse($date->created_at)->toDateString();
                      });
        $salesReportDaily = [];
        foreach ($salesReportDailyQuery as $key => $value) {
            $salesReportDaily[$key] = $value->sum('total_amount');
        }

        $salesReportDueDailyQuery = $this->sales_report->where(['status' => 'due'])->whereBetween('created_at' , [Carbon::now()->startOfMonth(), Carbon::now()])->get()->groupBy(function($date) {
                            return Carbon::parse($date->created_at)->toDateString();
                      });
        $salesReportDailyDue = [];
        foreach ($salesReportDueDailyQuery as $key => $value) {
            $salesReportDailyDue[$key] = $value->sum('total_amount');
        }

        /**
         * Sales Report Monthly
         */
        $salseReportMonthlyQuery = $this->sales_report->where(['status' => 'paid'])->whereBetween('created_at' , [Carbon::now()->startOfYear(), Carbon::now()])->get()->groupBy(function($date) {
                            return Carbon::parse($date->created_at)->format('F');
                      });
        $salesReportMonthly = [];
        foreach ($salseReportMonthlyQuery as $key => $value) {
            $salesReportMonthly[$key] = $value->sum('total_amount');
        }

        $salseReportMonthlyDueQuery = $this->sales_report->where(['status' => 'due'])->whereBetween('created_at' , [Carbon::now()->startOfYear(), Carbon::now()])->get()->groupBy(function($date) {
                            return Carbon::parse($date->created_at)->format('F');
                      });
        $salesReportMonthlyDue = [];
        foreach ($salseReportMonthlyDueQuery as $key => $value) {
            $salesReportMonthlyDue[$key] = $value->sum('total_amount');
        }

        /** 
         * Sales report yearly
         * @var [type]
         */
        $salesReportYearlyQuery = $this->sales_report->whereBetween('created_at' , [Carbon::create(2017, 01, 01, 00, 00, 00), Carbon::now()])->get()->groupBy(function($date) {
                            return Carbon::parse($date->created_at)->format('Y');
                      });
        $salesReportYearly = [];
        foreach ($salesReportYearlyQuery as $key => $value) {
            $salesReportYearly[$key] = $value->sum('total_amount');
        }

        $salesReportYearlyDueQuery = $this->sales_report->where('status', '=', 'due')->whereBetween('created_at' , [Carbon::create(2017, 01, 01, 00, 00, 00), Carbon::now()])->get()->groupBy(function($date) {
                            return Carbon::parse($date->created_at)->format('Y');
                      });
        $salesReportYearlyDue = [];
        foreach ($salesReportYearlyDueQuery as $key => $value) {
            $salesReportYearlyDue[$key] = $value->sum('total_amount');
        }

        $salesReportCurrentDate = $this->sales_report->whereBetween('created_at',[Carbon::now()->startOfDay(), Carbon::now()])->get();


    	$data = [
            'current_date' => Carbon::now()->toFormattedDateString(),
    		'total_areas_number' => $this->area->count(),
    		'total_local_areas_number' => $this->localarea->count(),
    		'total_customer_number' => $this->customer->count(),
    		'total_sales_man_number' => $this->salesman->count(),
    		'total_vans_number' => $this->van->count(),
    		'total_brokers_number' => $this->brokers->count(),
    		'total_gardens_number' => $this->garden->count(),
    		'purchase' => [
                'current' => [
                    'total_amount' => $purchaseDaily->sum('total_amount'),
                    'total_kg' => $purchaseDaily->sum('total_kg'),
                    'total_packs' => $purchaseDaily->sum('total_packs')
                ],
    			'daily' => $daily,
    			'monthly' => $month,
    			'yearly' => $yearly
    		],
    		'stocks' => [
    			'daily' => $stockDaily,
    			'monthly' => $stockMonthly,
    			'yearly' => $stockYearly
    		],
    		'blends' => [
                'current' => $blendReportDaily->sum('total_packs'),
    			'daily' => $blendDaily,
    			'monthly' => $blendMonthly,
    			'yearly' => $blendYearly
    		],
            'sales_report' => [
                'current' => [
                    'paid' => $salesReportCurrentDate->where('status', '=', 'paid')->sum('total_amount'),
                    'due' => $salesReportCurrentDate->where('status', '=', 'due')->sum('total_amount'),
                    'total_kg' => $salesReportCurrentDate->sum('total_kg')
                ],
                'paid' => [
                    'daily' => $salesReportDaily,
                    'monthly' => $salesReportMonthly,
                    'yearly' => $salesReportYearly
                ],
                'due' => [
                    'daily' => $salesReportDailyDue,
                    'monthly' => $salesReportMonthlyDue,
                    'yearly' => $salesReportYearlyDue
                ]
            ]
    	];
    	return response()->json($data, 200);
    }
}
