<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group([
	'prefix' => 'admin',
	'namespace' => 'Backend'
	], function() {

	require 'backend/tea.php';
	require 'backend/area.php';
	require 'backend/customer.php';
	require 'backend/salesmans.php';
	require 'backend/vans.php';
	require 'backend/garden.php';
	require 'backend/sales_report.php';
	require 'backend/stocks.php';
	require 'backend/roles.php';
	require 'backend/broker.php';
	require 'backend/tea_grade.php';
	require 'backend/vat.php';
	require 'backend/dashboard.php';
	require 'backend/purchase.php';
	require 'backend/blend.php';
	require 'backend/home_rent.php';
	require 'backend/daily_spent.php';
	require 'backend/account-history.php';
	require 'backend/assets_type.php';
	require 'backend/production.php';
	require 'backend/account.php';
	require 'backend/employee.php';
	require 'backend/upload-file.php';

});

Route::get('/home', 'HomeController@index')->name('home');

