<?php


/*
|--------------------------------------------------------------------------
| Garden Section Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([
		
        'prefix' => 'garden'

	], function() {

		Route::get('/', 'GardenController@GardenIndex')->name('garden.all');
		Route::post('/create', 'GardenController@GardenCreate')->name('garden.create');
		Route::get('/show/{id}', 'GardenController@GardenShow')->name('garden.show');
		Route::get('/edit/{id}', 'GardenController@GardenEdit')->name('garden.edit');
		Route::get('/remove/{id}', 'GardenController@GardenRemove')->name('garden.remove');
		
		Route::post('/store', 'GardenController@GardenStore')->name('garden.store');
		Route::post('/update/{id}', 'GardenController@GardenUpdate')->name('garden.update');
		
});