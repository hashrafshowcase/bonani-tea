<?php

Route::group([
	'prefix' => 'employee'

], function() {

	Route::get('/all-employee', 'EmployeeController@getAllEmployee')->name('employee.all');
	Route::post('/create-employee', 'EmployeeController@createEmployee')->name('employee.create');
	Route::get('/single-employee/{employee_id}', 'EmployeeController@getSingleEmployee')->name('employee.show');
	Route::get('/set-get-current-date-attendence', 'EmployeeController@setAndGetAllEmployee')->name('employee.attendence');
	Route::get('/get-sheet-by-date/{date}', 'EmployeeController@getAttendenceSheetByDate')->name('employee.get_sheet_date');
	Route::post('/attendence', 'EmployeeController@setAttendence')->name('employee.set_attendence');
});