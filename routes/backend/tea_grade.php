<?php


/*
|--------------------------------------------------------------------------
| Tea Section Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([
		'prefix' => 'tea-grade',
		//'middleware' => 'roles',
		//'roles' => ['owner', 'admin', 'accountant']
	], function() {

		Route::get('/all', 'TeaGradeController@allTeaGrade')->name('tea.grade.all');
		Route::get('/find/{id}', 'TeaGradeController@findTeaGradeById')->name('tea.grade.find.id');
		Route::post('/create-update', 'TeaGradeController@teaGradeCreateOrUpdate')->name('tea.grade.create.update');
		Route::get('/remove/{id}', 'TeaGradeController@removeTeaGrade')->name('tea.grade.remove');
});