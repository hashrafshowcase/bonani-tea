<?php


/*
|--------------------------------------------------------------------------
| Garden Section Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([
        'prefix' => 'assets-type'

	], function() {

		Route::get('/all-assets-type', 'AccountController@allAssetsType')->name('assets.all');
		Route::post('/store-account-record', 'AccountController@storeAccountRecord')->name('store.account.record');
		Route::get('/store-account-record-show/{type?}', 'AccountController@showAccountRecord')->name('show.account.record.show');
		Route::get('/store-account-record-show-by-ref/{ref}/{type?}', 'AccountController@showAccountRecordByRef')->name('show.account.record.show.by.ref');
		Route::post('/search', 'AccountController@searchAccountRecord')->name('search.account');
		Route::post('/search-by-ref', 'AccountController@searchAcccountRecordByRef')->name('search.account.ref');
	
});