<?php


/*
|--------------------------------------------------------------------------
| Garden Section Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([
        'prefix' => 'purchase'

	], function() {

		Route::get('/history/{type?}', 'PurchaseController@previewPurchaseHistory')->name('purchase.history');
		Route::get('/history-broker/{broker_id}', 'PurchaseController@previewPurchaseListById')->name('purchase.view.id');
		Route::post('/store', 'PurchaseController@purchaseStore')->name('purchase.store');
		Route::post('/search', 'PurchaseController@purchaseSearch')->name('purchase.search');
		Route::get('/purchase-list', 'PurchaseController@getPurchaseList')->name('purchase.list');
});