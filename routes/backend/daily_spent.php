<?php


/*
|--------------------------------------------------------------------------
| Garden Section Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([
        'prefix' => 'daily-spent'

	], function() {

		Route::get('/history/{type?}', 'DailySpentController@dailySpentHistory')->name('daily_spent_history');

		Route::post('/store-home-rent', 'DailySpentController@dailySpentStore')->name('daily_spent_store');

		Route::post('/search', 'DailySpentController@dailySpentSearch')->name('daily_spent_search');
});