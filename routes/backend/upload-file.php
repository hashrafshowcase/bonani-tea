<?php


Route::group([
	'prefix' => 'upload',
	//'middleware' => 'roles',
	//'roles' => ['owner', 'admin', 'accountant']
], function() {

	Route::post('/image-upload', 'FileUploadController@uploadImage')->name('image.upload');
	Route::post('/file-upload', 'FileUploadController@uploadFIle')->name('file.upload');

});