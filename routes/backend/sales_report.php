<?php


/*
|--------------------------------------------------------------------------
| Tea Section Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([
		'prefix' => 'sales-report',
		//'middleware' => 'roles',
		//'roles' => ['owner', 'admin', 'accountant']
	], function() {

		Route::get('/', 'SalesReportController@index')->name('sales-report.all');

		Route::get('/find/{id}', 'SalesReportController@findById')->name('sales-report.find');

		Route::get('/find/salesman/{salesman_id}', 'SalesReportController@findBySalesman')->name('sales-report-find-salesman');

		Route::get('/find/area/{area_id}', 'SalesReportController@findByArea')->name('sales-report-find-area');

		Route::get('/find/local-area/{local_area_id}', 'SalesReportController@findByLocalArea')->name('sales-report-find-local-area');

		Route::get('/find/cash-memo/{cash_memo}', 'SalesReportController@findByCashMemo')->name('sales-report-find-cash-memo');

		Route::get('/find-salesman-area', 'SalesReportController@findBySalesManArea')->name('sales-report-find-salesman-area');
		
		Route::post('/create', 'SalesReportController@createOrUpdate')->name('sales-report.create');

		Route::post('/test', 'SalesReportController@test')->name('sales.test');

		Route::post('/sales-report-by-salesman-id', 'SalesReportController@getSalesReportBySalesmanId')->name('salesreport-by-salesman');
		
		Route::post('/sales-report-by-salesman-id-advance-1', 'SalesReportController@getSalesReportBySalesmanIdAdvance1')->name('salesreport-by-salesman-adv-1');

		Route::post('/sales-report-by-salesman-id-advance-2', 'SalesReportController@getSalesReportBySalesmanIdAdvance2')->name('salesreport-by-salesman-adv-2');

		Route::post('/sales-report-by-salesman-id-advance-3', 'SalesReportController@getSalesReportBySalesmanIdAdvance3')->name('salesreport-by-salesman-adv-3');

		Route::post('/daily-sales-report', 'SalesReportController@currentDateSalesReport')->name('salesreport-daily');

		Route::post('/get-sales-report-by-date-range', 'SalesReportController@getSalesReportByDateRange')->name('sales-report-by-date-range');
		
		Route::get('/single-report/{sales_report_id}', 'SalesReportController@getSingleSalesReport')->name('sales-report-single-by-id');

		Route::post('/sales-report-by-salesman-id-with-range', 'SalesReportController@getSalesReportBySalesmanIdwithRange')->name('salesreport-with-salesman-id-range');

		Route::post('/sales-report-product-range', 'SalesReportController@getRangeProductList')->name('salesreport-product-list-range');
});