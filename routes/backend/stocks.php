<?php


/*
|--------------------------------------------------------------------------
| Tea Section Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([
		'prefix' => 'stocks',
	], function() {

		Route::get('/category', 'StocksController@allCategories')->name('stocks.all.category');
		Route::get('/category/find/{cat_id}', 'StocksController@findCatById')->name('stocks.cat.find');
		Route::post('/category/create', 'StocksController@createOrUpdateCategory')->name('stock.cat.create.update');
		Route::post('/category/remove/{cat_id}', 'StocksController@removeCategory')->name('stocks.cat.remove');
		Route::get('/stock-index/all' , 'StocksController@allStockIndex')->name('stocks.index');
		Route::get('/stock-index/data/find/{stock_index_id}' , 'StocksController@stockIndexData')->name('stocks.find.data');

		Route::post('/stocks-list/add', 'StocksController@addStocksData')->name('stock.list.add');
		Route::post('/stock-index/create', 'StocksController@createOrUpdateCategoryIndex')->name('stock.list.index');
		Route::get('/stock-index/remove/{id}', 'StocksController@removeIndex')->name('stock.index.remove');
		Route::get('/daily-data', 'StocksController@getDailyAllData')->name('stock.daily.data');
		Route::post('/advance-search', 'StocksController@getAdvanceSearch')->name('stock.advance.search');
		
});