<?php


/*
|--------------------------------------------------------------------------
| Tea Section Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([
		'prefix' => 'van',
		//'middleware' => 'roles',
		//'roles' => ['owner', 'admin']
	], function() {

		Route::get('/', 'VanController@VanIndex')->name('van.all');
		Route::post('/create', 'VanController@VanCreateOrUpdate')->name('van.create-update');
		Route::post('/remove/{id}', 'VanController@VanRemove')->name('van.remove');
});