<?php


/*
|--------------------------------------------------------------------------
| Tea Section Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([
		'prefix' => 'area',
		//'middleware' => 'roles',
		//'roles' => ['owner', 'admin']
	], function() {

		Route::get('/', 'AreaController@area')->name('area.all');
		Route::get('/find/{id}', 'AreaController@findArea')->name('area.find');
		Route::post('/create', 'AreaController@createArea')->name('area.create');
		Route::post('/update/{id}', 'AreaController@updateArea')->name('area.update');
		Route::post('/remove/{id}', 'AreaController@deleteArea')->name('area.remove');
		Route::get('/export-area', 'AreaController@export_all_area')->name('area.export');

		Route::group(['prefix' => 'local-area'], function() {

			Route::get('/', 'AreaController@localArea')->name('localarea.all');
			Route::get('/find/{id}', 'AreaController@findLoalArea')->name('localarea.find');
			Route::post('/create', 'AreaController@CreateLocalArea')->name('localarea.create');
			Route::post('/update/{id}', 'AreaController@updateLocalArea')->name('localarea.update');
			Route::post('/remove/{id}', 'AreaController@deleteLocalArea')->name('localarea.remove');
		});
		
});