<?php


/*
|--------------------------------------------------------------------------
| Garden Section Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([
        'prefix' => 'account'

	], function() {

		Route::get('/asset-type/get-asset-type', 'AccountAssetController@getAccountAssets')->name('get-asset-type');
		Route::post('/asset-type/store', 'AccountAssetController@storeAccountAsset')->name('store-asset-type');
		Route::post('/asset-type/update-store', 'AccountAssetController@updateAccountAsset')->name('update-asset-type');

		Route::get('/general-ledger/get-general-ledger', 'AccountAssetController@getAccountGeneralLedger')->name('get-general-ledger');

		Route::post('/general-ledger/store', 'AccountAssetController@storeAccountGeneralLedger')->name('store-general-ledger');

		Route::get('/subsidiary-ledger/get-subsidiary-ledger', 'AccountAssetController@getAccountSubsidiaryLedger')->name('get-subsidiary-ledger');

		Route::post('/subsidiary-ledger/store', 'AccountAssetController@storeAccountSubsidiaryLedger')->name('store-subsidiary-ledger');

		Route::get('/get-all-ledger', 'AccountAssetController@getLedgerSheet')->name('get-all-ledger');

		Route::post('/store-all-transaction', 'AccountAssetController@storeTransactionDetailsInStore')->name('post-all-transaction');

		Route::post('/get-transaction-by-date-range', 'AccountAssetController@getDateRangeJournalEntries')->name('get-transaction-by-date-range');

		Route::post('/get-account-details', 'AccountAssetController@getAccountDetails')->name('get_acccount_details');
		Route::post('/net-income', 'AccountAssetController@netIncome')->name('get-net-income');
		Route::post('/balance-sheet', 'AccountAssetController@balanceSheet')->name('get-balance-sheet');
});