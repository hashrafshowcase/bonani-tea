<?php


/*
|--------------------------------------------------------------------------
| Tea Section Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([
		'prefix' => 'brokers',
		//'middleware' => 'roles',
		//'roles' => ['owner', 'admin', 'accountant']
	], function() {

		Route::get('/all', 'BrokerController@allBroker')->name('broker.all');
		Route::get('/find/{id}', 'BrokerController@findBrokerById')->name('broker.find.id');
		Route::post('/create-update', 'BrokerController@brokerCreateOrUpdate')->name('broker.create.update');
		Route::get('/remove/{id}', 'BrokerController@removeBroker')->name('broker.remove');
});