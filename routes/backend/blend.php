<?php


/*
|--------------------------------------------------------------------------
| Garden Section Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([
        'prefix' => 'blend'

	], function() {

		// Route::get('/history', 'BlendController@previewBlendData')->name('blend.history');

		Route::get('/history/get-data/{date}', 'BlendController@previewBlendListByDate')->name('blend.by-date');

		Route::post('/store', 'BlendController@storeBlend')->name('blend.store');

		Route::get('/export-blend-date/{date}', 'BlendController@export_blend')->name('blend.date.export');

		Route::get('/history/{type?}', 'BlendController@blendHistory')->name('blend.history');
		
		Route::post('/search', 'BlendController@blendSearch')->name('blend.search');
});