<?php


/*
|--------------------------------------------------------------------------
| Tea Section Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([
		'prefix' => 'production',
		//'middleware' => 'roles',
		//'roles' => ['owner', 'admin']
	], function() {

		Route::get('/product-index', 'ProductionController@getProductIndex')->name('production.index');
		Route::post('/store-product', 'ProductionController@storeProduction')->name('production.store');
		Route::get('/history/{type?}', 'ProductionController@productionHistory')->name('production.history');
		Route::post('/search', 'ProductionController@productionSearch')->name('production_search');
});