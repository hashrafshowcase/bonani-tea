<?php


/*
|--------------------------------------------------------------------------
| Tea Section Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([
		'prefix' => 'tea',
		// 'middleware' => 'roles',
		// 'roles' => ['owner', 'admin']
	], function() {

		Route::get('/', 'TeaController@teaIndex')->name('tea.all');
		Route::get('/find/{id}', 'TeaController@findTea')->name('tea.find');
		Route::post('/create', 'TeaController@createOrUpdateTea')->name('tea.create-update');
		Route::post('/remove/{id}', 'TeaController@deleteTea')->name('tea.remove');


		Route::group(['prefix' => 'quantity'], function() {

			Route::get('/', 'TeaController@quantitiesIndex')->name('quantity.all');
			Route::get('/find/{id}', 'TeaController@findQuantity')->name('quantity.find');
			Route::post('/create', 'TeaController@createOrUpdateQuantity')->name('quantity.create');
			Route::post('/remove/{id}', 'TeaController@deleteTeaQuantities')->name('quantity.remove');

		});
		
});