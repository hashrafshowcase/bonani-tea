<?php


/*
|--------------------------------------------------------------------------
| Tea Section Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([
		'prefix' => 'vat',
		//'middleware' => 'roles',
		//'roles' => ['owner', 'admin']
	], function() {

		Route::get('/', 'VatController@getAllVat')->name('vat.all');
		Route::post('/create', 'VatController@VatCreateOrUpdate')->name('vat.create-update');
		Route::post('/remove/{id}', 'VatController@VatRemove')->name('vat.remove');
});