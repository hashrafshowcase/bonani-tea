<?php


/*
|--------------------------------------------------------------------------
| Garden Section Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([
        'prefix' => 'home-rent'

	], function() {

		Route::get('/history/{type?}', 'HomeRentController@homeRentHistory')->name('home.rent.history');

		Route::post('/store-home-rent', 'HomeRentController@storeHomeRent')->name('home_rent_store');

		Route::post('/search', 'HomeRentController@homeRentSearch')->name('home_rent_search');
});