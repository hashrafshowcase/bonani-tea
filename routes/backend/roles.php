<?php


/*
|--------------------------------------------------------------------------
| Tea Section Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([
		'prefix' => 'roles',
		//'middleware' => 'roles',
		//'roles' => ['owner', 'admin']
	], function() {

		Route::get('/', 'RoleMgmtController@getAllRoles')->name('roles.all');
		Route::get('/authorize-user', 'RoleMgmtController@getOwnerAdminAndAccountant')->name('roles.authorize.user');
		Route::get('/find/{id}', 'RoleMgmtController@getFindRole')->name('roles.find');
		Route::post('/create', 'RoleMgmtController@postCreateUpdateRoles')->name('roles.create');
		Route::get('/user-role/all', 'RoleMgmtController@getAllUserRole')->name('user.roles.all');
		Route::get('/user-role/find/{user_id}', 'RoleMgmtController@getUserRoleById')->name('user.roles.find');
		Route::post('/user-role/create', 'RoleMgmtController@postCreateUserRoles')->name('user.roles.create');
		
});