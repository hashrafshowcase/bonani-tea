<?php


/*
|--------------------------------------------------------------------------
| Tea Section Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([
		'prefix' => 'salesmans',
		//'middleware' => 'roles',
		//'roles' => ['owner', 'admin']
	], function() {

		Route::get('/', 'SalesmanController@getSalesman')->name('salesman.all');
		Route::get('/users', 'SalesmanController@getAllUser')->name('salesman.all.user');
		Route::get('/find/{id}', 'SalesmanController@getFindSalesMan')->name('salesman.find');
		Route::post('/create', 'SalesmanController@postSalesmanCreateOrUpdate')->name('salesman.create.update');
		Route::post('/create-user', 'SalesmanController@createSalesMan')->name('salesman.create.new');
		Route::post('/remove/{user_id}', 'SalesmanController@removeSalesMan')->name('salesman.remove');
});