<?php


/*
|--------------------------------------------------------------------------
| Tea Section Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([
		'prefix' => 'customer',
		//'middleware' => 'roles',
		//'roles' => ['owner', 'admin', 'accountant']
	], function() {

		Route::get('/', 'CustomerController@CoustomerIndex')->name('customer.all');
		Route::post('/create', 'CustomerController@CustomerCreateOrUpdate')->name('customer.create-update');
		Route::get('/find/{id}' , 'CustomerController@FindCustomer')->name('customer.find');
		Route::post('/remove/{id}', 'CustomerController@CustomerRemove')->name('customer.remove');
		Route::get('/export-customer', 'CustomerController@export_customer')->name('customer.export');
		
		
});