import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter);


import DashboardApp from './dashboard.vue'
import DashboardIndex from './components/index.vue'

/**
 * Settings Area
 */
import DashboardArea from './components/settings/Area/Area.vue'
import AreaCustomers from './components/settings/Area/AreaCustomers.vue'


/**
 * Settings Local Area
 */
import DashboardLocalArea from './components/settings/Local_Area/Localarea.vue'
import local_area_customers from './/components/settings/Local_Area/LocalAreaCustomers.vue'

/*
* Setting for Van Section
*/

import DashboardVan from './components/settings/Van/VanPreview.vue'


/* Tea */
import Tea from './components/settings/Tea/Tea.vue'
import TeaQuantity from './components/settings/Tea/TeaQuantity.vue'

/*
 *Setting for Salesman
 */
 import Salesman from './components/settings/Salesman/salesman.vue'

 /*
  * Setting for Customers
  */
  import Customer from './components/settings/customers/Customers.vue'
/**
 * Settings Brokers 
 */
import Brokers from './components/settings/Brokers/Brokers.vue'

/**
 * settings Tea Grade
 */
import TeaGrade from './components/settings/Tea-grade/TeaGrade.vue'

/**
 * Settings for garden
 */
import Garden from './components/settings/Garden/Garden.vue'

/**
 * Setting for Vat
 */
import Vat from './components/settings/Vat/vat.vue'

/*sales reports*/
import AddSalesReport from './components/Sales-Report/add.vue'
import view_sales_report from './components/Sales-Report/view/view.vue'

/* stock */
import AddStock from './components/Stocks/add/StockAdd.vue'
import StocksCategory from './components/Stocks/category/StocksCategory.vue'
import StocksIndex from './components/Stocks/index/StockIndex.vue'
import StockView from './components/Stocks/view/StockView.vue'

/* Roles */
import roles_list from './components/Roles/Roles_List/roles_list.vue'
import user_roles_list from './components/Roles/User_Roles/user_roles.vue'

/**
 * Unload Assets
 */
import unloadAssets from './components/Unload-Assets/Add/add.vue'
import purchase_view from './components/Unload-Assets/view/index.vue'
import purchase_preview from './components/Unload-Assets/view/preview.vue'

/**
 * Blending
 */
import BlendAdd from './components/Blend/add/add.vue'
import BlendView from './components/Blend/view/view.vue'
import BlendDateView from './components/Blend/view/dateView.vue'
import blend_preview from './components/Blend/view/blend_preview.vue'


/**
 * Home Rent
 */
import HomeRentAdd from './components/Home-rent/add/add.vue'
import HomeRentView from './components/Home-rent/view/view.vue'
import home_rent_preview from './components/Home-rent/view/preview.vue'

/**
 * Account History
 */
import AccountHistoryView from './components/Account-history/view/view.vue'

/**
 * Daily Spent
 */
import DailySpentAdd from './components/Daily-spent/add/add.vue'
import DailySpentView from './components/Daily-spent/view/view.vue'
import daily_spent_preview from './components/Daily-spent/view/preview.vue'

/*--Account--*/
import assets_type from './components/settings/assets_type/assets_type.vue';
import AccountAdd from './components/Account-history/add/add.vue';
import account_preview from './components/Account-history/view/preview.vue';
import JournalEntriesPreview from './components/Account-history/view/JournalEntries.vue';
import PerAssets from './components/Account-history/view/per_assets_type.vue';
import PerAssetsView from './components/Account-history/view/per_assets_type_view.vue';

import advanceReport1 from './components/Sales-Report/advance/advance_1.vue';

/*--Production--*/
import Production_add from './components/Production/production_add.vue'
import production_view from './components/Production/production_index.vue'
import production_preview from './components/Production/production_view.vue'

import single from './components/Sales-Report/advance/single.vue'


import accountAssetType from './components/Account/account-asset-type.vue'
import accountGeneralLedger from './components/Account/account-general-ledger.vue'
import accountSubsidiaryLedger from './components/Account/account-subsidiary-ledger.vue'
import transaction from './components/Account/transaction.vue'

import general_ledger_view from './components/Account/general-ledger.vue'
import journal_entries from './components/Account/journal-entries.vue'
import net_income from './components/Account/net-income.vue'
import balance_sheet from './components/Account/balance-sheet.vue'

/**
 * Employee
 */
import allEmployee from './components/employee/index.vue'
import addNewEmployee from './components/employee/addNew.vue'
import showEmployee from './components/employee/showEmployee.vue'
import attendence from './components/employee/attendence.vue'

export default new VueRouter({
    routes: [
        {
        	path: '/',
        	component: DashboardApp,
        	children: [
        		{
        			path: '',
        			name: 'index',
        			component: DashboardIndex
        		},
                {
                    path: '/employee',
                    name: 'all_employee',
                    component: allEmployee
                },
                {
                    path: '/employee/add',
                    name: 'add_all_employee',
                    component: addNewEmployee
                },
                {
                    path: '/employee/show/:employee_id',
                    name: 'show_employee',
                    component: showEmployee
                },
                {
                    path: '/employee/attendence',
                    name: 'all_attendence',
                    component: attendence
                },
        		{
        			path: '/settings/area',
        			name: 'area',
        			component: DashboardArea
        		},
                {
                    path: 'addNewEmployeesettings/area/:area_id/customer',
                    name: 'area_customer',
                    component: AreaCustomers
                },
        		{
        			path: '/settings/local-area',
        			name: 'localarea',
        			component:  DashboardLocalArea
        		},
                {
                   path: '/settings/local-area/:local_area_id/customers',
                   name: 'local_area_customers',
                   component:  local_area_customers
                },
                {
                    path: '/settings/tea',
                    name: 'tea',
                    component: Tea
                },
                {
                    path: '/settings/tea-quantity',
                    name: 'tea-quantity',
                    component: TeaQuantity
                },
				{
					path:'/settings/van',
					name:'van',
					component: DashboardVan
				},
                {
                    path: '/settings/salesman',
                    name: 'salesman',
                    component: Salesman
                },
                {
                    path: '/settings/customers',
                    name: 'customers',
                    component: Customer
                },
                {
                    path: '/settings/brokers',
                    name: 'brokers',
                    component: Brokers
                },
                {
                    path: '/settings/tea-grade',
                    name: 'tea_grade',
                    component: TeaGrade
                },
                {
                    path: '/settings/garden',
                    name: 'garden',
                    component: Garden
                },
                {
                    path: '/settings/vat',
                    name: 'vat',
                    component: Vat
                },
                {
                    path: '/settings/assets-type',
                    name: 'assets_type',
                    component: assets_type
                },


                /*--production routes--*/
                {
                    path:'/production/add',
                    name:'production_add',
                    component: Production_add
                },
                {
                    path: '/production/view',
                    name: 'production_view',
                    component: production_view
                },
                {
                    path: 'production/preview/daily/:preview_date/:id',
                    name: 'production_preview_daily_id',
                    component: production_preview
                },
                {
                    path: 'production/preview/daily/:preview_date',
                    name: 'production_preview_daily',
                    component: production_preview
                },
                {
                    path: 'production/preview/monthly/:preview_month/:preview_date/:id',
                    name: 'production_preview_monthly_date_id',
                    component: production_preview
                },
                {
                    path: 'production/preview/monthly/:preview_month/:preview_date',
                    name: 'production_preview_monthly_date',
                    component: production_preview
                },
                {
                    path: 'production/preview/monthly/:preview_month',
                    name: 'production_preview_monthly',
                    component: production_preview
                },
                {
                    path: 'production/preview/yearly/:preview_yearly/:preview_month/:preview_date/:id',
                    name: 'production_preview_yearly_month_date_id',
                    component: production_preview
                },
                {
                    path: 'production/preview/yearly/:preview_yearly/:preview_month/:preview_date',
                    name: 'production_preview_yearly_month_date',
                    component: production_preview
                },
                {
                    path: 'production/preview/yearly/:preview_yearly/:preview_month',
                    name: 'production_preview_yearly_month',
                    component: production_preview
                },
                {
                    path: 'production/preview/yearly/:preview_yearly',
                    name: 'production_preview_yearly',
                    component: production_preview
                },



				{
					path:'/sales-report/add',
					name:'AddSalesReport',
					component: AddSalesReport
				},
                {
                    path: '/sales-report/single/:id',
                    name: 'single_sales_report',
                    component: single
                },
                {
                    path:'/sales-report/view',
                    name:'view_sales_report',
                    component:view_sales_report
                },
                {
                    path: '/sales-report/view-sales-report/:create_at/:salesman_id',
                    name: 'view_sales_report_by_salesman',
                    component: advanceReport1
                },
                {
                    path: 'stocks/add',
                    name: 'stocks_add',
                    component: AddStock
                },
                {
                    path: 'stocks/view',
                    name: 'stocks_view',
                    component: StockView
                },
                {
                    path: 'stocks/category',
                    name: 'StocksCategory',
                    component: StocksCategory
                },
                {
                    path: 'stocks/index',
                    name: 'StocksIndex',
                    component: StocksIndex
                },
                {
                    path: 'roles/roles-list',
                    name: 'roles_list',
                    component: roles_list
                },
                {
                    path: 'roles/user-roles',
                    name: 'user_roles_list',
                    component: user_roles_list
                },


                {
                    path: 'purchase/add',
                    name: 'unload_assets_add',
                    component: unloadAssets
                },
                {
                    path: 'purchase/view',
                    name: 'purchase_view',
                    component: purchase_view
                },
                {
                    path: 'purchase/preview/daily/:preview_date/:id',
                    name: 'purchase_preview_daily_id',
                    component: purchase_preview
                },
                {
                    path: 'purchase/preview/daily/:preview_date',
                    name: 'purchase_preview_daily',
                    component: purchase_preview
                },
                {
                    path: 'purchase/preview/monthly/:preview_month/:preview_date/:id',
                    name: 'purchase_preview_monthly_date_id',
                    component: purchase_preview
                },
                {
                    path: 'purchase/preview/monthly/:preview_month/:preview_date',
                    name: 'purchase_preview_monthly_date',
                    component: purchase_preview
                },
                {
                    path: 'purchase/preview/monthly/:preview_month',
                    name: 'purchase_preview_monthly',
                    component: purchase_preview
                },
                {
                    path: 'purchase/preview/yearly/:preview_yearly/:preview_month/:preview_date/:id',
                    name: 'purchase_preview_yearly_month_date_id',
                    component: purchase_preview
                },
                {
                    path: 'purchase/preview/yearly/:preview_yearly/:preview_month/:preview_date',
                    name: 'purchase_preview_yearly_month_date',
                    component: purchase_preview
                },
                {
                    path: 'purchase/preview/yearly/:preview_yearly/:preview_month',
                    name: 'purchase_preview_yearly_month',
                    component: purchase_preview
                },
                {
                    path: 'purchase/preview/yearly/:preview_yearly',
                    name: 'purchase_preview_yearly',
                    component: purchase_preview
                },

                {
                    path: 'blend/view/:date/',
                    name: 'blend_date_view',
                    component: BlendDateView
                },





                /*--blend routes--*/
                {
                    path: 'blend/add',
                    name: 'blend_add',
                    component: BlendAdd
                },
                {
                    path: 'blend/view',
                    name: 'blend_view',
                    component: BlendView
                },
                {
                    path: 'blend/preview/daily/:preview_date/:id',
                    name: 'blend_preview_daily_id',
                    component: blend_preview
                },
                {
                    path: 'blend/preview/daily/:preview_date',
                    name: 'blend_preview_daily',
                    component: blend_preview
                },
                {
                    path: 'blend/preview/monthly/:preview_month/:preview_date/:id',
                    name: 'blend_preview_monthly_date_id',
                    component: blend_preview
                },
                {
                    path: 'blend/preview/monthly/:preview_month/:preview_date',
                    name: 'blend_preview_monthly_date',
                    component: blend_preview
                },
                {
                    path: 'blend/preview/monthly/:preview_month',
                    name: 'blend_preview_monthly',
                    component: blend_preview
                },
                {
                    path: 'blend/preview/yearly/:preview_yearly/:preview_month/:preview_date/:id',
                    name: 'blend_preview_yearly_month_date_id',
                    component: blend_preview
                },
                {
                    path: 'blend/preview/yearly/:preview_yearly/:preview_month/:preview_date',
                    name: 'blend_preview_yearly_month_date',
                    component: blend_preview
                },
                {
                    path: 'blend/preview/yearly/:preview_yearly/:preview_month',
                    name: 'blend_preview_yearly_month',
                    component: blend_preview
                },
                {
                    path: 'blend/preview/yearly/:preview_yearly',
                    name: 'blend_preview_yearly',
                    component: blend_preview
                },





                /*-- home-rent routes --*/
                {
                    path: 'home-rent/add',
                    name: 'home_rent_add',
                    component: HomeRentAdd
                },
                {
                    path: 'home-rent/view',
                    name: 'home_rent_view',
                    component: HomeRentView
                },
               {
                    path: 'home-rent/preview/daily/:preview_date/:id',
                    name: 'home_rent_preview_daily_id',
                    component: home_rent_preview
                },
                {
                    path: 'home-rent/preview/daily/:preview_date',
                    name: 'home_rent_preview_daily',
                    component: home_rent_preview
                },
                {
                    path: 'home-rent/preview/monthly/:preview_month/:preview_date/:id',
                    name: 'home_rent_preview_monthly_date_id',
                    component: home_rent_preview
                },
                {
                    path: 'home-rent/preview/monthly/:preview_month/:preview_date',
                    name: 'home_rent_preview_monthly_date',
                    component: home_rent_preview
                },
                {
                    path: 'home-rent/preview/monthly/:preview_month',
                    name: 'home_rent_preview_monthly',
                    component: home_rent_preview
                },
                {
                    path: 'home-rent/preview/yearly/:preview_yearly/:preview_month/:preview_date/:id',
                    name: 'home_rent_preview_yearly_month_date_id',
                    component: home_rent_preview
                },
                {
                    path: 'home-rent/preview/yearly/:preview_yearly/:preview_month/:preview_date',
                    name: 'home_rent_preview_yearly_month_date',
                    component: home_rent_preview
                },
                {
                    path: 'home-rent/preview/yearly/:preview_yearly/:preview_month',
                    name: 'home_rent_preview_yearly_month',
                    component: home_rent_preview
                },
                {
                    path: 'home-rent/preview/yearly/:preview_yearly',
                    name: 'home_rent_preview_yearly',
                    component: home_rent_preview
                },
                
                {
                    path: 'account/add',
                    name: 'account_add',
                    component: AccountAdd
                },

                {
                    path: 'account/view',
                    name: 'account_view',
                    component: AccountHistoryView
                },
                {
                    path: 'account/preview/daily/:preview_date',
                    name: 'account_preview_daily',
                    component: account_preview
                },
                {
                    path: 'account/preview/daily/:preview_date/show',
                    name: 'account_preview_daily_show',
                    component: JournalEntriesPreview
                },
                {
                    path: 'account/preview/monthly/:preview_month/:preview_date',
                    name: 'account_preview_monthly_date',
                    component: account_preview
                },
                {
                    path: 'account/preview/monthly/:preview_month/:preview_date/show',
                    name: 'account_preview_monthly_date_show',
                    component: JournalEntriesPreview
                },
                {
                    path: 'account/preview/monthly/:preview_month',
                    name: 'account_preview_monthly',
                    component: account_preview
                },
                {
                    path: 'account/preview/yearly/:preview_yearly/:preview_month/:preview_date/:id',
                    name: 'account_preview_yearly_month_date_id',
                    component: account_preview
                },
                {
                    path: 'account/preview/yearly/:preview_yearly/:preview_month/:preview_date/show',
                    name: 'account_preview_yearly_month_date_show',
                    component: JournalEntriesPreview
                },
                {
                    path: 'account/preview/yearly/:preview_yearly/:preview_month/:preview_date',
                    name: 'account_preview_yearly_month_date',
                    component: account_preview
                },
                {
                    path: 'account/preview/yearly/:preview_yearly/:preview_month',
                    name: 'account_preview_yearly_month',
                    component: account_preview
                },
                {
                    path: 'account/preview/yearly/:preview_yearly',
                    name: 'account_preview_yearly',
                    component: account_preview
                },
                {
                    path: 'account/per-assets',
                    name: 'account_per_assets',
                    component: PerAssets
                },
                {
                    path: 'account/per-assets/:ref/view',
                    name: 'account_per_assets_ref_view',
                    component: PerAssetsView
                },
                {
                    path: 'daily-spent/add',
                    name: 'daily_spent_add',
                    component: DailySpentAdd
                },
                {
                    path: 'daily-spent/view',
                    name: 'daily_spent_view',
                    component: DailySpentView
                },
                {
                    path: 'daily-spent/preview/daily/:preview_date/:id',
                    name: 'daily_spent_preview_daily_id',
                    component: daily_spent_preview
                },
                {
                    path: 'daily-spent/preview/daily/:preview_date',
                    name: 'daily_spent_preview_daily',
                    component: daily_spent_preview
                },
                {
                    path: 'daily-spent/preview/monthly/:preview_month/:preview_date/:id',
                    name: 'daily_spent_preview_monthly_date_id',
                    component: daily_spent_preview
                },
                {
                    path: 'daily-spent/preview/monthly/:preview_month/:preview_date',
                    name: 'daily_spent_preview_monthly_date',
                    component: daily_spent_preview
                },
                {
                    path: 'daily-spent/preview/monthly/:preview_month',
                    name: 'daily_spent_preview_monthly',
                    component: daily_spent_preview
                },
                {
                    path: 'daily-spent/preview/yearly/:preview_yearly/:preview_month/:preview_date/:id',
                    name: 'daily_spent_preview_yearly_month_date_id',
                    component: daily_spent_preview
                },
                {
                    path: 'daily-spent/preview/yearly/:preview_yearly/:preview_month/:preview_date',
                    name: 'daily_spent_preview_yearly_month_date',
                    component: daily_spent_preview
                },
                {
                    path: 'daily-spent/preview/yearly/:preview_yearly/:preview_month',
                    name: 'daily_spent_preview_yearly_month',
                    component: daily_spent_preview
                },
                {
                    path: 'daily-spent/preview/yearly/:preview_yearly',
                    name: 'daily_spent_preview_yearly',
                    component: daily_spent_preview
                },
                {
                    path: 'account/asset-type',
                    name: 'account_asset_type',
                    component: accountAssetType
                },
                {
                    path:'account/general-ledger',
                    name: 'account_general_ledger',
                    component: accountGeneralLedger
                },
                {
                    path:'account/subsidiary-ledger',
                    name: 'account_subsidiary_ledger',
                    component: accountSubsidiaryLedger
                },
                {
                    path:'account/transaction',
                    name: 'account_transaction',
                    component: transaction
                },
                {
                    path: 'account/general-ledger/:account_type/:account_id/view',
                    name: 'account_general_ledger_view',
                    component: general_ledger_view
                },
                {
                    path: 'account/journal-entries',
                    name: 'journal_entries',
                    component: journal_entries
                },
                {
                    path: 'account/net-income',
                    name: 'net_income',
                    component: net_income
                },
                {
                    path: 'account/balance-sheet',
                    name: 'balance_sheet',
                    component: balance_sheet
                }
        	]
        }
    ]
})