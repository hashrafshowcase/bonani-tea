export default {
	namespaced: true,
	state: {
		area: [],
		customers: [],
		localarea: [],
	},
	getters: {
		areas: (state) => state.area,
		localareas: (state) => state.localarea,
	},
	mutations: {
		setArea(state, data) {
			state.area = data
		},
		setLocalAreaById(state, data) {
			state.localarea = data
		},
		notification(state,data){
			elem.notify(data.status,data.message);
		},

	},
	actions: {
		getAllArea({commit},query) {
			const root = home.area_show
			axios
				.get(root, {params : query})
				.then((response) => {
					commit('setArea', response.data.data)
				})
				.catch((error) => {
					console.log(error.response)
				})
		},

		addArea({commit},data) {
			const url = home.area_create.replace('#id#', data.id);
			axios.post(url, data)
             .then(response => {
             	console.log(response);
             	if(response){
             		commit('notification',{status: 'success', message:response.data.message});
             		return true;
             	}
             })
             .catch(e => {
                console.log(e.response);
                return false;
             })
		},

		deleteArea({commit},id) {
			const AreaDeleteUrl = home.area_delete.replace('#id#', id)
	        axios.post(AreaDeleteUrl)
	             .then(response => {
	                if(response) {
	                	console.log(response.data.area);
	              		commit('setArea',response.data.area);
	                	commit('notification',{status:'success',message:response.data.message});
	                	return true;
	                }
	                
	             })
	             .catch(e => {
	             	commit('notification',{status:'error',message:e.response.data.message});
	             	return false;
	             })
		},

		getFindAllLocalAreaById({commit}, id) {
			const root = home.area_find.replace('#id#', id) + '?fields=localareas'

			axios
				.get(root)
				.then((response) => {
					commit('setLocalAreaById', response.data.localareas)
				})
		}
	}
}