export default {
	namespaced: true,
	state: {
		allVat: []
	},
	getters: {
		vat: (state) => state.allVat
	},
	mutations: {
		setAllVatData(state, data) {
			state.allVat = data
		}
	},
	actions: {
		getAllVatData({commit}) {
			const root = home.vat_all
			axios
				.get(root)
				.then(res => {
					commit('setAllVatData', res.data)
				})
				.catch(err => {
					console.log(err)
				})
		}
	}
}