export default {
	namespaced: true,
	state: {
		localarea: [],
		selectOptions:[],
		show : '',
		customers :[]

	},
	getters: {
		localareas: (state) => state.localarea,
		selectOptions:(state)=> state.selectOptions,
		show : (state)=> state.show,
		customers:(state) => state.customers,
	},
	mutations: {
		setlocalareas(state, data) {
			state.localarea = data
		},
		setSelectOption(state,data) {
			state.selectOptions = data;
		},
		setCustomers(state,data) {
			state.customers = data;
		},
		notification(state,data){
			elem.notify(data.status,data.message);
		},
		show_partition(state,data) {
			state.show = data;
		},
	},
	actions: {
		fetch_local_data({commit},query) {
			const AreaFetchData = home.local_area_show
	        axios.get(AreaFetchData, {params: query})
	             .then(response => {
	                commit('setlocalareas',response.data.data);
	                return true;
	             })
	             .catch(error =>{
	             	console.log(error.response);
	             	return false;
	             })
		},
		prepareLocalAreaAdd({commit}) {
	        const AreaGetUrl = home.area_show
	        axios.get(AreaGetUrl)
	             .then(response => {

	                commit('setSelectOption',response.data);
	             })
	             .catch(e => {
	                console.log(e.response)
	             })
	    },
		getFindLocalArea({commit}, data) {
			const root = home.local_area_find.replace('#id#', data.id)
			axios
				.get(root, {params : data.query})
				.then((response) => {
					commit('setCustomers', response.data.customers)
				})
				.catch((error) => {
					console.log(error.response)
				})
		},
		addLocalArea({commit},data) {
	        const AddLocalAreaUrl = home.local_area_create
	        axios.post(AddLocalAreaUrl, data)
	             .then(response => {
	                commit('notification',{status:'success', message: response.data.message});
	                commit('show_partition',true);
	             })
	             .catch(e => {
	                console.log(e.response);
	                return false;
	             })
	    },
	    deleteArea({commit},id) {
	        const LocalAreaDeleteUrl = home.local_area_remove.replace('#id#', id)
	        axios.post(LocalAreaDeleteUrl)
	             .then(response => {
	                commit('notification',{status:'success', message: response.data.message});
	             })
	             .catch(e => {
	                commit('notification',{status:'error', message: e.response.data.message});
	             })
	    },

	    updateLocalArea({commit},form) {
	        const AreaUpdateUrl = home.local_area_update.replace('#id#', form.id);
	        axios.post(AreaUpdateUrl, form)
	             .then(response => {
	                commit('notification', {status :'success', message : response.data.message});
	                return true;
	             })
	             .catch(e => {
	                console.log(e.response);
	                return false;
	             })
	      },

	   
	}
}