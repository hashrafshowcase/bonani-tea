import customers from './customers'
import area from './area'
import localarea from './localarea'
import salesman from './salesman'
import brokers from './brokers'
import tea_grade from './tea_grade'
import garden from './garden'
import vat from './vat'
import tea from './tea'
import van from './van'
import assets_type from './assets_type'

export default {
	namespaced: true,
	state: {
	},
	getters: {
	},
	mutations: {
	},
	actions: {
	},
	modules: {
		area,
		customers,
		localarea,
		salesman,
		brokers,
		tea_grade,
		garden,
		vat,
		tea,
		van,
		assets_type
	}
}