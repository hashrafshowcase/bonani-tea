export default {
	namespaced: true,
	state: {
		AllGarden : []
	},
	getters: {
		Garden : (state) => state.AllGarden
	},
	mutations: {
		setAllGarden(state, data) {
			state.AllGarden = data
		},
		notification(state, data) {
			elem.notify(data.type, data.message);
		}
	},
	actions: {
		getAllGarden({commit}) {
			const root = home.garden_all

			axios
				.get(root)
				.then(res => {
					commit('setAllGarden', res.data)
					return true;
				})
				.catch(err => {
					return false;
				})
		},
		PostCreateGarden({commit}, data) {
			const root = home.garden_create_update
			axios
				.post(root, data)
				.then(res => {
					let message = {
						type : 'success',
						message : res.data.message
					};
					commit('notification', message);
					commit('setAllGarden', res.data.gardenList);
					return true;
				})
				.catch(err => {
					return false;
				})
		},
		GetRemoveGarden({commit}, id) {
			const root = home.garden_remove.replace('#id#', id);
			axios
				.get(root)
				.then(res => {
					let message = {
						type : 'success',
						message : res.data.message
					};
					commit('notification', message);
					commit('setAllGarden', res.data.gardenList)
					return true;
				})
				.catch(err => {
					let message = {
						type : 'warning',
						message : 'Alraedy use in another field'
					};
					commit('notification', message);
					return false;
				})
		}
	}
}