export default {
	namespaced: true,
	state: {
		allBrokers : []
	},
	getters: {
		broker : (state) => _.cloneDeep(state.allBrokers)
	},
	mutations: {
		setAllBrokers(state, data) {
			state.allBrokers = _.cloneDeep(data)
		},
		notification(state, data) {
			elem.notify(data.type, data.message);
		} 
	},
	actions: {
		GetAllBrokers({commit}) {
			const root = home.broker_all
			axios
				.get(root)
				.then(res => {
					commit('setAllBrokers', res.data)
					return true;
				})
				.catch(err => {
					return false;
				})
		},
		PostCreateBroker({commit}, data) {
			const root = home.broker_create_update
			axios
				.post(root, data)
				.then(res => {
					let message = {
						type : 'success',
						message : res.data.message
					};
					commit('notification', message);
					commit('setAllBrokers', res.data.brokers)
					return true;
				})
				.catch(err => {
					return false;
				})
		},
		GetRemoveBroker({commit}, id) {
			const root = home.broker_remove.replace('#id#', id);
			axios
				.get(root)
				.then(res => {
					let message = {
						type : 'success',
						message : res.data.message
					};
					commit('notification', message);
					commit('setAllBrokers', res.data.brokers)
					return true;
				})
				.catch(err => {
					let message = {
						type : 'warning',
						message : 'Already use in another field'
					};
					commit('notification', message);
					return false;
				})
		}
	}
}