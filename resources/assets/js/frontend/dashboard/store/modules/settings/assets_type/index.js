export default {
	namespaced: true,
	state:{
		assets: [],
		filter_assets: []
	},
	getters: {
		all_assets: (state) => state.assets,
		filter_assets: (state) => state.filter_assets
	},
	mutations: {
		setAllAssets(state, data) {
			_.forEach(data, (item) => {
				if( item.type == 'secondary' ) {
					item.parent = item.assets_primary[0].assets_type.title
				} else {
					item.parent = item.title
				}
			})
			state.assets = data;
		},
		filterSecondary(state, data) {
			state.filter_assets = _.filter(data, (item) => {
				return item.type == 'secondary';
			})
		}
	},
	actions: {
		
		getAllAssetsType({commit}) {
			const url = home.all_assets_type;
			axios
				.get(url)
				.then(({ data: {assets_type} } ) => {
					commit('setAllAssets', assets_type);
					commit('filterSecondary', assets_type);
				})
				.catch(err => {
					console.log(err)
				}) 
		}
	}
}