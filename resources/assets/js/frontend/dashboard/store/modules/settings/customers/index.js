export default {
	namespaced: true,
	state: {
		customer: []
	},
	getters: {
		customers: (state) => state.customer
	},
	mutations: {
		setCustomers(state, data) {
			state.customer = data
		}
	},
	actions: {
		getAllCustomers({commit}) {
			const root = home.customer_show + '?fields=area,localarea'
			axios
				.get(root)
				.then((response) => {
					commit('setCustomers', response.data)
				})
				.catch((error) => {
					console.log(error.response)
				})
		},

		postCreateCustomer({commit}, data) {
			const root = home.customer_create_update

			axios
				.post(root, data)
				.then((response) => {
					elem.notify('success', response.data.message)
				})
				.catch((error) => {
					console.log('error')
				})
		}
	}
}