export default {
	namespaced: true,
	state: {
		tea: []
	},
	getters: {
		all_tea : (state) => state.tea
	},
	mutations: {
		set_tea_data(state, data) {
			state.tea = data
		}
	},
	actions: {
		getAllTea({commit}) {
			const root = home.tea_show
			axios
				.get(root)
				.then(res => {
					console.log(res)
					commit('set_tea_data', res.data)
				})
				.catch(err => {
					console.log(err)
				})
		}
	}
}