export default {
	namespaced: true,
	state: {
		User: [],
		Salesman: []
	},
	getters: {
		users: (state) => state.User,
		salesmans: (state) => state.Salesman

	},
	
	mutations: {
		setUsers(state, data) {
			data.forEach((item) => {
				item.status = false
			})
			state.User = data
		},
		setSalesmans(state, data) {
			state.Salesman = data
		}
	},

	actions: {
		getAllsalesman({commit}) {
			const root = home.salesman_show + '?fields=user'
			axios
				.get(root)
				.then((response) => {
					commit('setSalesmans', response.data)
				})
				.catch((error) => {
					console.log(error.response)
				})
		},

		getAllUsers({commit}) {
			const root = home.salesman_all_user 
			axios
				.get(root)
				.then((response) => {
					commit('setUsers', response.data)
				})
				.catch((error) => {
					console.log('error')
				})
		},

		postCreateSalesman({commit}, data) {
			const root = home.salesman_create
			axios
				.post(root, data)
				.then((response) => {
					elem.notify('success', response.data.message)
				})
				.catch((error) => {
					console.log('error')
				})
		}
	}
}