export default {
	namespaced: true,
	state: {
		allTeaGrade: []
	},
	getters: {
		TeaGrade: (state) => state.allTeaGrade
	},
	mutations: {
		setAllTeaGrade(state, data) {
			state.allTeaGrade = data
		}
	},
	actions: {
		getAllTeaGrade({commit}) {
			const root = home.tea_grade_all
			axios
				.get(root)
				.then(res => {
					commit('setAllTeaGrade', res.data)
				}) 
				.catch(err => {
					console.log(err)
				})
		}
	}

}