export default {
	namespaced: true,
	state : {
		van 	: [],
		add_status: false,
		update_status: false,
		fetch_data: false
	},
	getters : {
		vans_list : (state) => state.van,
		visible   : (state) => state.visible,
		add_data: (state) => state.add_status,
		update_data: (state) => state.update_status,
		fetch_data: (state) => state.fetch_data
	},
	mutations : {
		setFetchData(state, data) {
			state.fetch_data = data;
		},
		setAddStatus(state, data) {
			state.add_status = data;
		},
		setUpdateStatus(state, data) {
			state.update_status = data;
		},
		setVan(state,data) {
			state.van = data
		},
		section_visibility(state,data){
			state.visible = data;
		},
		notification(state,data){
			elem.notify(data.status,data.message);
		},
	},
	actions: {
		fetch_all_van_data({commit},query){
			const VanFetchData = home.van_show
	        axios.get(VanFetchData, {params: query})
	             .then(response => {
	               	commit('setVan',response.data.data);
	                commit('setFetchData', true);
	             })
	             .catch(error => {
	             	console.log(error.response);
	             	return false;
	             })
		},
		addVan({commit},data) {
	        const url = home.van_create_update;
	        axios.post(url, data)
	             .then(response => {
	               if(response) {
	                  commit('notification',{status:'success', message:response.data.message});
	                  commit('setAddStatus', true);
	                  return true;
	               }
	             })
	             .catch(e => {
	                console.log(e);
	                return false;
	             })
	    },

      deleteVan({commit},id) {
        const url = home.van_remove.replace('#id#', id)
        axios.post(url)
             .then(response => {
                elem.notify('success', response.data.message);
                return true;
             })
             .catch(e => {
                elem.notify('error', e.response.data.message);
                return false;
             })
      },



      updateVanPlateNum({commit},data) {
        const url = home.van_create_update;
        axios.post(url, data)
             .then(response => {
                if(response.status==200) {
                    elem.notify('success', response.data.message);
                    commit('setUpdateStatus', true);
                }
             })
             .catch(e => {
                console.log(e.response);
                return false;
             })
      },
	}
}