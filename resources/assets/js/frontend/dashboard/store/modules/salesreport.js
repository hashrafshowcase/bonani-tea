export default {
	namespaced: true,
	state: {
		vandata: [],
		salesman: [],
		columns: [],
		area: [],
		localArea: [],
		getColllectData: [],
		customer: []
	},
	getters: {
		assignArea(state) {
			return state.area
		},
		assignVanData(state) {
			return state.vandata
		},
		assignSalesman(state) {
			return state.salesman
		},
		assignColumns(state) {
			return state.columns
		},
		assignGetCollectData(state) {
			return state.getColllectData
		},
		assignLocalArea : (state) => state.localArea,
		assignCustomer: (state) => state.customer
	},
	mutations: {
		setVanData(state, data) {
			state.vandata = data
		},
		setSalesman(state, data) {
			state.salesman = data
		},
		setColumnsData(state, data) {
			state.columns = data
		},
		setArea(state, data) {
			console.log(data);
			state.area = data
		},
		setCollectData(state, data) {
			state.getColllectData = data
		},
		setLocalArea(state, data) {
			state.localArea = data
		},
		setCustomer(state, data) {
			state.customer = data
		}
	},
	actions: {
		getCustomerByArea({commit}, id) {
			const customer = home.area_find.replace('#id#', id)
			axios
			    .get(customer  + '?fields=customers')
			    .then(res => {
			    	commit('setCustomer', res.data.customers)
			    })
			    .catch(e => {
			    	console.log(e.response)
			    })
		},
		getVanData(ctx) {
			const van = home.van_show
			axios.get(van)
			     .then(res => {
			     	ctx.commit('setVanData', res.data)
			     })
			     .catch(e => {
			     	console.log(e.response)
			     })
		},
		getArea(ctx) {
			const area = home.area_show;
			axios.get(area)
			     .then(res => {
			     	ctx.commit('setArea', res.data)
			     })
			     .catch(e => {
			     	console.log(e.response)
			     })
		},
		getLocalArea({commit}, id) {
			const localarea = home.area_find.replace('#id#', id)
			axios
			    .get(localarea + '?fields=localareas')
			    .then(res => {
			    	console.log(res)
			    	commit('setLocalArea', res.data.localareas)
			    })
			    .catch(e => {
			    	console.log(e.response)
			    })
		},
		getSalesmanData(ctx) {
			const salesman = home.salesman_show;
			axios.get(salesman + '?fields=user')
			.then(res => {
				ctx.commit('setSalesman', res.data)
			})
			.catch(e=>{
				console.log(e.response);
			})
		},
		getColumnsData(ctx) {
			const quantity_url = home.quantity_show
			axios.get(quantity_url)
			     .then(res => {
			     	ctx.commit('setColumnsData', res.data)
			     })
			     .catch(e => {
			     	console.log(e.res)
			     })
		}
	}
}