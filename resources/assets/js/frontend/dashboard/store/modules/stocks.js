export default {
	namespaced: true,
	state: {
		assignStockIndex: [],
		assignStockData: [],
		assignStockPastDeposit: '',
		assignStockSumDeposit: '',
		assignStockBalance: '',
		assignCurrentDate: new Date(),
		assignStockCategory: []
	},
	getters: {
		StockIndexes : (state) => state.assignStockIndex,
		StockData: (state) => state.assignStockData,
		StockLastDeposit: (state) => state.assignStockPastDeposit,
		StockSumDeposit: (state) => state.assignStockSumDeposit,
		StockBalance: (state) => state.assignStockBalance,
		StockCurrentDate : (state) => moment(state.assignCurrentDate).format('YYYY-MM-DD'),
		StockCategory: (state) => state.assignStockCategory
	},
	mutations: {
		setStockBalance(state, data) {
			state.assignStockBalance = state.assignStockSumDeposit - parseInt(data)
			state.assignStockBalance = state.assignStockBalance || ''
		},
		setStockSumDeposit(state, data) {
			state.assignStockSumDeposit = state.assignStockPastDeposit + parseInt(data)
			state.assignStockSumDeposit = state.assignStockSumDeposit || ''
		},
		setStockIndexData(state, data) {
			state.assignStockData = data
			state.assignStockPastDeposit = ''
			state.assignStockSumDeposit = ''
			state.assignStockBalance = ''
			if(data.length > 0) {
				let lastItem = data[data.length - 1]
				state.assignStockPastDeposit = parseInt(lastItem.stocks_balance) || ''
			}
		},
		setStockIndex(state, data) {
			state.assignStockIndex = data
		},
		setStockCategory(state, data) {
			state.assignStockCategory = data
		}
	},
	actions: {
		getStocksCategory({commit}) {
			const root =  home.stocks_category_all + '?fields=stock_indices'
			axios
				.get(root)
				.then(res => {
					commit('setStockCategory', res.data)
				})
				.catch(e => {
					console.log(e.response)
				})

		},
		getStockIndex({commit}) {
			const root = home.stocks_index + '?fields=stock_category'
			axios
				.get(root)
				.then(res => {
					console.log(res)
					commit('setStockIndex', res.data)
				})
				.catch(e => {
					console.log(e.response)
				})
		},
		getStockIndexData(ctx, id) {
			const root = home.stock_find_data.replace('#id#', id)
			axios
				.get(root)
				.then(res => {
					console.log(res)
					ctx.commit('setStockIndexData', res.data)
					ctx.commit('setStockSumDeposit', 0)
					ctx.commit('setStockBalance', 0)
				})
				.catch(e => {
					console.log(e.response)

				})
		},
		postStocksData(ctx, data) {
			const root = home.stock_list_add
			axios
				.post(root, data)
				.then(res => {
					console.log(res)
					elem.notify('success', res.data.message)
				})
				.catch(e => {
					console.log(e)
				})
		},
		
	}
}