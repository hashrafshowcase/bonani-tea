export default {
	namespaced: true,
	state: {
		blendData: {
			label: [],
			data: {}
		}
	},
	getters: {
		blendAllDate: (state) => state.blendData
	},
	mutations: {
		blendDataMutation(state, data) {
			state.blendData = {
				label: [],
				data: {}
			}
			_.forEach(data, (blend, key) => {
				_.forEach(blend, (list) => {
					blend.total = 0;
					_.forEach(list.blend_lists, (packs) => {
						blend.total += packs.packs
					})
				})
				state.blendData.label.push(key)
				state.blendData.data[key] = blend
			})
		}
	},
	actions: {
		getBlendData({commit}) {
			const url = home.blend_history;
			axios
				.get(url)
				.then((res) => {
					commit('blendDataMutation', res.data.blend)
				})
				.catch((err) => {
					console.log(err)
				})
		},
		storeBlend({commit}, data) {
			console.log(data)
			return
			const url = home.purchase_store;
			axios
				.post(url, data)
				.then((res) => {
					console.log(res)
				})
				.catch((err) => {
					console.log(err)
				})
		}
	}
}