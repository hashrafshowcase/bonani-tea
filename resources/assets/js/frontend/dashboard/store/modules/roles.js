export default {
	namespaced: true,
	state: {
		role: [],
		user_role: [],
		authorize_role: []
	},
	getters: {
		roles: (state) => state.role,
		user_roles: (state) => state.user_role,
		notify_users: (state) => state.authorize_role
	},
	mutations: {
		setRoles(state, data) {
			state.role = data
		},

		setUserRole(state, data) {
			_.map(data, (item) => {
				item.roles_name = []
				_.map(item.roles, (role) => {
					item.roles_name.push(role.title)
				})
				item.roles_name = item.roles_name.join(',')
			})
			state.user_role = data
		},
		setAuthorizeUser(state, data) {
			let sorted  = []
			let lastItem = []
			_.map(data, (item) => {
				sorted.push(item.user_id)
			})
			let removeDuplicate = _.uniq(sorted)
			_.map(sorted, (item) => {
				let findIndex = _.findIndex(data, {user_id: item})
				console.log(findIndex)
				lastItem.push(data[findIndex])
			})
			removeDuplicate = _.uniq(lastItem)
			state.authorize_role = removeDuplicate
		}
	},
	actions: {
		getAllRoles({commit}) {
			const root = home.roles_show
			axios
				.get(root)
				.then((response) => {
					commit('setRoles', response.data)
				})
				.catch((error) => {
					console.log('error')
				})
		},

		postCreateRole({commit} , data) {
			const root = home.roles_create
			axios.post(root, data)
				.then((response) => {
					elem.notify('success', response.data.message)
				})
				.catch((error) => {
					console.log('error')
				})
		},

		getUserRole({commit}) {
			const root = home.user_role_all + '?fields=roles'
			axios
				.get(root)
				.then((response) => {
					commit('setUserRole', response.data)
				})
				.catch((error) => {
					console.log(error)
				})
		},

		getUserRoleById({commit}, id) {
			const root = home.user_roles_find.replace('#id#', id)
			axios
				.get(root)
				.then((response) => {
					commit('setUserRoleById', response.data)
				})
				.catch((error) => {
					console.log('error')
				})
		},
		getAuthorizeRolesUser({commit}) {
			const root = home.user_authorize + '?fields=user'
			axios
				.get(root)
				.then((response) => {
					commit('setAuthorizeUser', response.data)
				})
				.catch((error) => {
					console.log('error');
				})
		}

	}
}