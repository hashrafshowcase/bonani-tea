import Vue from 'vue'
import Vuex from 'vuex'
// import * as state from './_state'
// import * as getters from './_getters'
// import * as mutations from './_mutations'
// import * as actions from './_actions'
import salesreport from './modules/salesreport'
import stocks from './modules/stocks.js'
import settings from './modules/settings'
import roles from './modules/roles.js'
import unload_assets from './modules/unload_assets.js'
import blend from './modules/blend.js'

Vue.use(Vuex)

export default new Vuex.Store({
	state: {},
	getters: {},
	mutations: {},
	actions: {},
	modules: {
		salesreport,
		stocks,
		settings,
		roles,
		unload_assets,
		blend
	}
})