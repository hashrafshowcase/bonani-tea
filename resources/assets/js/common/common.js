import {Notification} from 'element-ui'

export default ({
    notify: function(type, message) {
        return Notification({
            title: type[0].toUpperCase() + type.slice(1).toLowerCase(),
            message: message,
            type: type
        });
    },
    cl: function(param) {
        return console.log(param)
    },
    getErrorMessage: function(key, data) {
        setTimeout(function() {
            Smp.notify('error', data[key][0])
        })
    },
    scrollTop: function() {
        $('.next,.previous').on('click', function() {
            $('body').scrollTop(0);
        });
    }
})