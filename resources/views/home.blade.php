@extends('layouts.dashboard')

@section('content')

        
    <div class="be-wrapper be-fixed-sidebar">
        <div id="app">
            <router-view name="default"></router-view>
        </div> 
    </div>        
               
 
@push('scripts')
  <script>
    window.home = @php 
            echo json_encode([
               'app_url' => env('APP_URL'),
               'user' => auth()->user()->id,
               'token' => csrf_token(),
               
               'area_show' => route('area.all'),
               'area_find' => route('area.find', ['id' => '#id#']),
               'area_delete' => route('area.remove', ['id' => '#id#']),
               'area_update' => route('area.update', ['id' => '#id#']),
               'area_export' => route('area.export'),
               'area_create' => route('area.create'),

               'van_show' => route('van.all'),
               'van_create_update' => route('van.create-update'),
               'van_remove' => route('van.remove' , ['id' => '#id#']),
               'logout' => route('logout'),
               

               'local_area_show' => route('localarea.all'),
               'local_area_find' => route('localarea.find', ['id' => '#id#']),
               'local_area_create' => route('localarea.create'),
               'local_area_update' => route('localarea.update', ['id' => '#id#']),
               'local_area_remove' => route('localarea.remove', ['id' => '#id#']),

               'tea_show' => route('tea.all'),
               'tea_create_update' => route('tea.create-update'),
               'tea_remove' => route('tea.remove', ['id' => '#id#']),

               'quantity_show' => route('quantity.all'),
               'quantity_create_update' => route('quantity.create'),
               'quantity_remove' => route('quantity.remove',['id' => '#id#']),

               'salesreport_create' => route('sales-report.create'),
               'salesman_area_show' => route('sales-report-find-salesman-area'),
               'sales_test' => route('sales.test'),

               /*--stock--*/
               'stocks_index' => route('stocks.index'),
               'stock_find_data' => route('stocks.find.data', ['stock_index_id' => '#id#']),
               'stock_list_add' => route('stock.list.add'),
               'stocks_category_all' => route('stocks.all.category'),
               'stock_category_add' => route('stock.cat.create.update'),
               'stock_remove' => route('stocks.cat.remove',['cat_id' => '#id#']),

               /*--customer--*/
               'customer_show' => route('customer.all'),
               'customer_create_update' => route('customer.create-update'),
               'customer_find' => route('customer.find', ['id' => '#id#']),
               'customer_remove' => route('customer.remove', ['id' => '#id#']),
               'customer_export' => route('customer.export'),

               /*--Salesman--*/
               'salesman_show' => route('salesman.all'),
               'salesman_all_user' => route('salesman.all.user'),
               'salesman_create' => route('salesman.create.update'),

               /*--roles--*/
               'roles_show' => route('roles.all'),
               'roles_find' => route('roles.find', ['id' => '#id#']),
               'roles_create' => route('roles.create'),

               /*--user--*/
               'user_role_all' => route('user.roles.all'),
               'user_roles_find' => route('user.roles.find', ['user_id' => '#id#']),
               'user_roles_create' => route('user.roles.create'),
               'user_authorize' => route('roles.authorize.user'),

               /*--broker Routes--*/
               'broker_all' => route('broker.all'),
               'broker_find_id' => route('broker.find.id', ['id' => '#id#']),
               'broker_create_update' => route('broker.create.update'),
               'broker_remove' => route('broker.remove', ['id' => '#id#']),
               /**
                * Tea Grade
                */
               'tea_grade_all' => route('tea.grade.all'),
               'tea_grade_find_id' => route('tea.grade.find.id', ['id' => '#id#']),
               'tea_grade_create_update' => route('tea.grade.create.update'),
               'tea_grade_remove' => route('tea.grade.remove', ['id' => '#id#']),
               /**
                * garden
                * @type {Object}
                */
               'garden_all' => route('garden.all'),
               'garden_create' => route('garden.create'),
               'garden_show' => route('garden.show', ['id' => '#id#']),
               'garden_edit' => route('garden.edit', ['id' => '#id#']),
               'garden_remove' => route('garden.remove', ['id' => '#id#']),
               'garden_update' => route('garden.update', ['id' => '#id#']),
               'garden_create_update' => route('garden.store'),
               /**
                * vat
                * @type {Object}
                */
               'vat_all' => route('vat.all'),
               'vat_create_update' => route('vat.create-update'),
               'vat_remove' => route('vat.remove', ['id' => '#id#']),

               /*--dashboard--*/

               'dashboard_all' => route('dashboard.view'),
               'purchase_store' => route('purchase.store'),
               'purchase_history' => route('purchase.history', ['type' => '#type#']),

               'blend_store' => route('blend.store'),
               'blend_by_date' => route('blend.by-date', ['date' => '#date#']),
               'blend_export_by_date' => route('blend.date.export', ['date' => '#date#']),

               'home_rent_store' => route('home_rent_store'),
               'home_rent_history' => route('home.rent.history', ['type' => '#type#']),
               'daily_spent_store' => route('daily_spent_store'),
               'daily_spent_history' => route('daily_spent_history', ['type' => '#type#']),
               'purchase_search' => route('purchase.search'),
               'home_rent_search'=> route('home_rent_search'),
               'daily_spent_search' => route('daily_spent_search'),
               'all_assets_type' => route('assets.all'),
               'store_account_record' => route('store.account.record'),
               'store_account_record_show' => route('show.account.record.show', ['type' => '#type#']),
               'account_search' => route('search.account'),
               'store_account_record_show_by_ref' => route('show.account.record.show.by.ref', ['ref' => '#ref#', 'type'=>'#type#']),
               'account_search_by_ref' => route('search.account.ref'),
               'stock_index_create_or_update' => route('stock.list.index'),
               'stock_index_remove' => route('stock.index.remove', ['id' => '#id#']),

               'sales_report_get_by_salesman_id'  => route('salesreport-by-salesman'),
               'sales_report_get_by_salesman_id_adv_1' => route('salesreport-by-salesman-adv-1'),
               'sales_report_get_by_salesman_id_adv_2' => route('salesreport-by-salesman-adv-2'),
               'sales_report_get_by_salesman_id_adv_3' => route('salesreport-by-salesman-adv-3'),
               'salesman_new_create' => route('salesman.create.new'),
               'salesman_remove'  => route('salesman.remove', ['user_id' => '#id#']),
               'purchase_list_all' => route('purchase.list'),
               'blend_history' => route('blend.history', ['type' => '#type#']),
               'blend_search'  => route('blend.search'),
               'production_index' => route('production.index'),
               'production_store' => route('production.store'),
               'production_history' => route('production.history', ['type' => '#type#']),
               'production_search'  => route('production_search'),
               'stock_daily_data'   => route('stock.daily.data'),
               'stock_advance_search' => route('stock.advance.search'),
               'sales_report_daily_view' => route('salesreport-daily'),
               'sales_report_by_date_range' => route('sales-report-by-date-range'),
               'sales_report_single_id' => route('sales-report-single-by-id', ['sales_report_id' => '#id#']),
               'sales_report_by_salesman_id_range' => route('salesreport-with-salesman-id-range'),
               'sales_report_product_list_range' => route('salesreport-product-list-range'),


               'asset_type_get_asset' => route('get-asset-type'),
               'store_asset_type'     => route('store-asset-type'),
               'update_asset_type'    => route('update-asset-type'),

               'get_general_ledger'   => route('get-general-ledger'),
               'store_general_ledger' => route('store-general-ledger'),

               'get_subsidiary_ledger' => route('get-subsidiary-ledger'),
               'store_subsidiary_ledger' => route('store-subsidiary-ledger'),
               'get_all_ledger' => route('get-all-ledger'),

               'store_all_transaction'  => route('post-all-transaction'),
               'get_transaction_by_date_range' => route('get-transaction-by-date-range'),
               'get_acccount_details' => route('get_acccount_details'),
               'get_net_income' => route('get-net-income'),
               'get_balance_sheet' => route('get-balance-sheet'),
                /*--employee--*/
                'get_employee'  => route('employee.all'),
                'image_upload'  => route('image.upload'),
                'file_upload'   => route('file.upload'),
                'create_employee' => route('employee.create'),
                'show_single_employee' => route('employee.show', ['employee_id' => '#employee_id#']),
                'set_get_current_attendence' => route('employee.attendence'),
                'get_sheet_by_date'         => route('employee.get_sheet_date', ['date' => '#date#']),
                'set_attendence'            => route('employee.set_attendence')
            ]);
    @endphp
</script>
 <script src={{mix('js/dashboard.js')}} ></script>
@endpush
</div>
@endsection
