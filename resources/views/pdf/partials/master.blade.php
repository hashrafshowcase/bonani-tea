<!DOCTYPE html>
<html>
  <head>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" type="text/css" href="../../../../node_modules/bootstrap/dist/css/bootstrap.min.css">
  </head>
  <style>
	.page-break {
	    page-break-after: always;
	}
  </style>
  <body>
      <div class="container">
          @yield('content')
      </div>
  </body>
</html>