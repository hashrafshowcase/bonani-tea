
<!DOCTYPE html>
<html>
  <head>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
  </head>
  <style>
	.page-break {
	    page-break-after: always;
	}
  </style>
  <body>
      	<div>
          	<div class="customer">
          		<h1 class="text-center">Bonani Customer Details</h1>
			</div>

			<br>

			<div>
				<table class="table">
				  <thead class="thead-inverse">
				    <tr>
				      <th>#</th>
				      <th>Customer Name</th>
				      <th>Shop Name</th>
				      <th>Phone Number</th>
				      <th>Area</th>
				      <th>Local Area</th>
				      <th>Total Amount</th>
				    </tr>
				  </thead>
				  <tbody>
				  	@foreach($customers as $key => $item)
				    <tr>
				      <th scope="row">{{$key}}</th>
				      <td>{{ $item->customer_name }}</td>
				      <td>{{ $item->customer_shop_name }}</td>
				      <td>{{ $item->customer_phone_number }}</td>
				      <td>{{ $item->area->area_name }}</td>
				      <td>{{ $item->localarea->local_area_name }}</td>
				      <td>{{ $item->total_amount }}</td>
				    </tr>
				    @endforeach
				  </tbody>
				</table>
			</div>
      	</div>
  </body>
</html>