
<!DOCTYPE html>
<html>
  <head>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
  </head>
  <style>
	.page-break {
	    page-break-after: always;
	}
  </style>
  <body>
      	<div>
          	<div class="customer">
          		<p class="text-right">Date: {{$data['date']}}</p>
          		<h1 class="text-center">Bonani All Area Details</h1>
			</div>

			<br>

			<div>
				@foreach($data['data'] as $a)
					<h1>{{$a->area_name}}</h1>
					@foreach($a->localareas as $item)
						<li>{{$item->local_area_name}}</li>
					@endforeach
				@endforeach
			</div>
      	</div>
  </body>
</html>