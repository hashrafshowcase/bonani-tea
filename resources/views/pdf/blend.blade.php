
<!DOCTYPE html>
<html>
  <head>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
  </head>
  <style>
	.page-break {
	    page-break-after: always;
	}
  </style>
  <body>
      	<div>
          	<div class="customer">
          		<p class="text-right">Date: {{$blends['date']}}</p>
          		<h1 class="text-center">Bonani Blend Details</h1>
			</div>

			<br>

			<div>
				<table class="table">
				  <thead class="thead-inverse">
				    <tr>
				      <th>#</th>
				      <th>Tea</th>
				      <th>Tea Grade</th>
				      <th>Packs</th>
				    </tr>
				  </thead>
				  <tbody>
				  	@foreach($blends['data'] as $key => $item)
				    <tr>
				      <th scope="row">{{$key + 1}}</th>
				      <td>{{ $item->tea->name }}</td>
				      <td>{{ $item->tea_grade->grade_name }}</td>
				      <td>{{ $item->packs }}</td>
				    </tr>
				    @endforeach
				  </tbody>
				</table>
			</div>
      	</div>
  </body>
</html>